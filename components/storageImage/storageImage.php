<?php

namespace app\components\storageImage;

use himiklab\thumbnail\EasyThumbnailImage;

class storageImage
{
    // В данную директорию по умолчанию сохраняет виджет ELfinder.
    const DIRECTORY_IMAGE_GLOBAL = 'upload/global/';

    // Путь, по которому будут распологаться фото в оригинале.
    const DIRECTORY_IMAGE_FULL = 'upload/image_full/';

    // Путь, куда сохраняются миниатюры.
    const DIRECTORY_IMAGE_MINI = 'upload/image_mini/';

    // Пути до папок, в которых лежат изображения
    public static $pathGlobal;
    public static $pathFull;
    public static $pathMini;
    
    // Полные пути до изображений.
    public static $imgGlobal;
    public static $imgFull;
    public static $imgMini;


    /**
     * Функция вызывается при обновлении или удалении изображений.
     */
    public static function imageDelete ()
    {
        if (file_exists(self::$imgFull))
        {
            // Переносим изображение из папки image_full в папку global и удаляем миниатюру.
            rename(self::$imgFull, self::$imgGlobal);
            unlink(self::$imgMini);
        }
    }

    /**
     * Создание папок image_mini и image_full, а так же перемещение изображений из папки global в папку image_full.
     */
    public static function imageFull()
    {
        // Если нет папки, то создаем
        if(!is_dir(self::$pathFull))
            mkdir(self::$pathFull, 0777);// создаём папку

        if(!is_dir(self::$pathMini))
            mkdir(self::$pathMini, 0777);// создаём папку

        // Переносим изображение из папки global в папку image_full.
        rename(self::$imgGlobal, self::$imgFull);
    }

    /**
     * Создание миниатюры из полного изображения.
     *
     * Миниаютра создается исходя из полученных данных width и height в style.
     * При отстутствии данных миниатюра будет создана по реальным размерам.
     *
     * @param string $image
     * @param string $key
     */
    public static function imageMini($image, $key)
    {
        // Получаем высоту и ширину фото.
        preg_match('/height:([0-9]*).*width:([0-9]*)/', $image[0][$key], $property);

        // Если добавляем изображение в текст, то добавляются пробелы и меняются местами ширина с высотой.
        if (empty($property))
        {
            preg_match('/width: ([0-9]*).*height: ([0-9]*)/', $image[0][$key], $alternative);

            $property[1] = $alternative[2];
            $property[2] = $alternative[1];
        }

        // Если в стиле не заданы ширина и высота, делаем миниатюру в соответствии с оригиналом.
        if(empty($property))
        {
            $originalSize = getimagesize(self::$imgFull);

            $property[2] = $originalSize[0];
            $property[1] = $originalSize[1];
        }

        $createdMini =  EasyThumbnailImage::thumbnailFileUrl(
            self::$imgFull,
            $property[2],
            $property[1],
            EasyThumbnailImage::THUMBNAIL_OUTBOUND

        );

        // Переносим созданное изображение в нужную нам папку.
        rename(substr($createdMini, 1), self::$imgMini);
    }

    /**
     * Создание имен и путей для изображений, а так же создание путей для папок, в которых они находятся.
     * Передаем измененные данные обратно в виджет, для записи в бд.
     *
     * @param $content
     * @param null $delete
     * @return mixed
     */
    public static function body ($content, $delete = null)
    {
        preg_match_all('#<img.*src="(.*)".*>#isU', $content, $image);

        foreach ($image[1]  as $key => $value)
        {
            $pathInfo = pathinfo($value); // Получаем имя файла.

            // Если удаляем, необходим путь к старому имени.
            if (!empty($delete))
                $nameImg = $pathInfo['basename'];

            // Если создаем, то генерируем новое, уникальное имя.
            else{
                $time = time() + $key;
                $uniqueName = md5($time.$pathInfo['basename']);
                $nameImg = $uniqueName.'.'.$pathInfo['extension'];
            }


            // Папка имеющая название в 2 символа.
            $folder = substr($nameImg, 0, 2);

            // Пути до папок, в которых лежат изображения.
            self::$pathGlobal = self::DIRECTORY_IMAGE_GLOBAL;
            self::$pathFull   = self::DIRECTORY_IMAGE_FULL.$folder;
            self::$pathMini   = self::DIRECTORY_IMAGE_MINI.$folder;
            

            // Полные пути изображений.
            self::$imgGlobal  = self::$pathGlobal.$pathInfo['basename'];
            self::$imgFull    = self::$pathFull.'/'.$nameImg;
            self::$imgMini    = self::$pathMini.'/'.$nameImg;

            
            // Логика компонента.
            if (!empty($delete)) {
               self::imageDelete();
            }

            else {
                self::imageFull();
                self::imageMini($image, $key);
            }

            // Заменяем в полученной строке старый путь изображения на новый, для записи в БД.
            $content = str_replace($value,  self::$pathMini.'/'.$nameImg, $content);
        }
        return $content;
    }
}