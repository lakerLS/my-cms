<?php

namespace app\components\storageImage;

use app\models\Content;

class eventOfImage
{
    // Сохранение и редактирование изображений.
    public static function beforeSave($insert, $model)
    {
        // Если запись создается. Ищем и обрабатываем изображения в полях text и image
        if ($insert)
        {
            //Yii::$app->session->setFlash('success', 'Запись добавлена!');

            if(isset($model->text))
                $model->text = storageImage::body($model->text);

            if(isset($model->image))
                $model->image = storageImage::body($model->image);
        }

        // Если запись редактируется. Удаляем старые изображения. Ищем и обрабатываем изображения в полях text и image
        else
        {
            //Yii::$app->session->setFlash('success', 'Запись обновлена!');

            $content = Content::findOne(['id' => $model->id]);

            if(isset($model->text))
            {
                storageImage::body($content->text, 'delete');

                $model->text = storageImage::body($model->text);
            }

            if(isset($model->image))
            {
                storageImage::body($content->image, 'delete');

                $model->image = storageImage::body($model->image);
            }
        }
    }

    // Если запись удаляется, то удаляем принадлежащией ей изображения.
    public static function afterDelete($model)
    {
        if(isset($model->text))
            storageImage::body($model->text, 'delete');

        if(isset($model->image))
            storageImage::body($model->image, 'delete');
    }
}