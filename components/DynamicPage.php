<?php

namespace app\components;

use app\models\Category;
use app\models\Content;
use app\models\Redirect;
use Yii;
use yii\helpers\Url;
use yii\web\UrlRuleInterface;
use yii\base\BaseObject;

class DynamicPage extends BaseObject implements UrlRuleInterface
{
    public $parents;

    public $meta;

    /**
     * Проверяем, является ли страница динамической, если да, то обрабатываем соответствующе для отображения
     * необходимой страницы.
     *
     * @param \yii\web\UrlManager $manager
     * @param \yii\web\Request $request
     * @return array|bool
     */
    public function parseRequest($manager, $request)
    {
        // Делим на массивы и берем последнее значение.
        $url = explode('/', $request->pathInfo);
        $url = array_pop($url);

        $category = Category::findAll(['url' => $url]);
        $content = Content::findAll(['url' => $url]);

        // Если в базе данных есть данный адрес, то обрабатываем как динамическую страницу
        if (!empty($category) || !empty($content))
        {
            /**
             * Получаем родителей текущей категории или категории в которой находится текущая запись.
             *
             * Сверяем полный путь текущий с полученным из базы данных, нужен для определения где мы находимся,
             * так как у записи в БД может быть одинаковый url, но разное расположение,
             * благодаря данной проверки если в обоих таблицах будет одинаковый урл, это не вызовет конфликт.
             *
             * Если путь был найден, то переводим на необходимый контроллер
             */
            foreach ($category as $value)
            {
                $this->parents = $value->parents()->andWhere(['<>', 'lvl', '0'])->all(); ;
                $this->meta = $value;


                // Если найдена текущая страница
                if('/'.Yii::$app->request->pathInfo == Url::to(['/content/many']))
                {
                    return ['content/many', ['current' => $value]];
                }

                // Если страница не найдена, но найдена в истории. Передаем новый адрес параметром, для переадресации.
                elseif(!empty($oldUrl = Redirect::findOne(['old_url' => Url::to(['/content/many'])])))
                {
                    $redirect = Redirect::find()
                        ->where(['category_id' => $oldUrl->category_id])->orderBy(['date' => SORT_DESC])->one();

                    return ['content/many', ['current' => $value, 'redirect' => $redirect->new_url]];
                }
            }

            foreach ($content as $value)
            {
                $this->parents = $value->category->parents()->andWhere(['<>', 'lvl', '0'])->all();
                $this->meta = $value->category;

                $explode = explode('/', Yii::$app->request->pathInfo);
                $cut = array_pop($explode);
                $categoryUrl = implode('/', $explode);


                // Если найдена текущая страница
                if('/'.Yii::$app->request->pathInfo == Url::to(['/content/one', 'model' => $value]))
                {
                    return ['content/one', ['current' => $value]];
                }

                // Если страница не найдена, но найден адрес родителя в истории.
                // Передаем актуальный параметр, для переадресации.
                elseif(!empty($oldUrl = Redirect::findOne(['old_url' => Url::to(['/content/many'])])))
                {
                    $redirect = Redirect::find()
                        ->where(['category_id' => $oldUrl->category_id])->orderBy(['date' => SORT_DESC])->one();

                    return ['content/one', ['current' => $value, 'redirect' => $redirect->new_url.'/'.$value->url]];
                }
            }
        }

        return false;
    }


    /**
     * При создании адреса на пункт меню, необходимо указать контроллер 'content/many'.
     *
     * При создании адреса на конкретную запись, необходимо указывать контроллер 'content/one',
     * а так же передавать массив $model.
     *
     * При формировании адреса на пункт меню, находясь на не динамической странице, к примеру в админке.
     * Необходимо указывать контроллер 'content/many' и передавать следующие параметры:
     * url - строкой и parents - массивом.
     *
     *
     * @param \yii\web\UrlManager $manager
     * @param string $route
     * @param array $params
     * @return bool|string
     */
    public function createUrl($manager, $route, $params)
    {
        if ($route == 'content/many' || $route == 'content/one')
        {
            // Полный путь.
            $url = null;

            if (!empty($params['url']))
            {
                $lastUrl = $params['url'];
                $parents = $params['parents'];
            }

            else
            {
                $lastUrl = $this->meta->url;
                $parents = $this->parents;
            }

            // Склеиваем по кусочкам всех родителей записи.
            foreach ($parents as $value)
            {
                if (!empty($value->url))
                    $url = $url.'/'.$value->url;
            }

            // Если ссылка на конкретную запись.
            if ($route == 'content/one')
                return $url.'/'.$lastUrl.'/'.$params['model']->url;

            // Если ссылка на пункт меню.
            elseif ($route == 'content/many')
                return $url.'/'.$lastUrl;
        }

        return false; // this rule does not apply
    }
}