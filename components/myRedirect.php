<?php
namespace app\components;

use app\models\Category;
use app\models\Redirect;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;


/**
 * Данный класс используется в событии после записи/редактирования категории.
 *
 * Class myRedirect
 * @package app\components
 */
class myRedirect
{
    public static $category;

    public static $fullUrl;

    public static $oldUrl;

    /**
     * Делаем запись в базу данных при изменении уровня вложенности или основного адреса.
     * @param $main
     */
    public static function urlMain ($main)
    {
        // Ищем, есть ли старая запись. Совпадений по данному критерию может быть много, но нам необходима последняя.
        self::$oldUrl = Redirect::find()->where(['category_id' => $main->id])->orderBy(['date' => SORT_DESC])->one();

        // При изменении вложенности через виджет treeManager ломаются путь, поэтому пришлось сделать доп. запрос.
        self::$category = Category::find()->where(['id' => $main->id])->one();
        $parents = self::$category->parents()->andWhere(['<>', 'lvl', '0'])->all();

        // Получаем полный адрес.
        self::$fullUrl = Url::to(['/content/many', 'url' => $main->url, 'parents' => $parents]);

        // Старой записи нет или полный адрес изменился.
        if (empty(self::$oldUrl) || self::$oldUrl->new_url != self::$fullUrl)
        {
            $redirect = new Redirect();

            $redirect->category_id = self::$category->id;

            $redirect->new_url = self::$fullUrl;

            if (!empty(self::$oldUrl))
                $redirect->old_url = self::$oldUrl->new_url;

            $redirect->save();
        }
    }


    /**
     * Если основная категория изменяет свой адрес, при этом содержит детей, то необходимо сделать новые записи в БД
     * всем детям с актуальными адресами.
     *
     * @throws ServerErrorHttpException
     */
    public static function urlChildren()
    {
        // Получаем детей если есть у категории, которая изменила свой адрес.
        $children = self::$category->children()->all();

        if (!empty($children))
        {
            // Работаем с каждым ребенком отдельно.
            foreach ($children as $value)
            {
                // Получаем данные на текущего ребенка. Совпадений может быть много, но нам необходима последняя.
                $urlChildren = Redirect::find()
                    ->where(['category_id' => $value->id])->orderBy(['date' => SORT_DESC])->one();

                // Если ребенок существует и старый путь отца существует. При грубом редактировании таблицы могут
                // возникнуть проблемы, если удалить у существующего сына все записи или родителя.
                if (!empty($urlChildren) && !empty(self::$oldUrl->new_url))
                {
                    // Производим подсчет, с помощью которого определяем сколько частей адреса необходимо удалить.
                    $count = substr_count(self::$oldUrl->new_url.'/', '/');

                    $explode = explode('/', $urlChildren->new_url);

                    // Удаляем $count количество частей адреса, для дальнейшего склеивания.
                    for ($x = 0; $x < $count; $x++)
                    {
                        unset($explode[$x]);
                    }

                    $implode = implode('/', $explode);

                    // Если адрес ребенка изменился, тогда производим запись
                    if ($urlChildren->new_url != self::$fullUrl.'/'.$implode )
                    {
                        $redirect = new Redirect();

                        $redirect->category_id = $urlChildren->category_id;

                        $redirect->new_url = self::$fullUrl.'/'.$implode;

                        if (!empty($urlChildren->new_url))
                            $redirect->old_url = $urlChildren->new_url;

                        $redirect->save();
                    }
                }

                else
                {
                    throw new ServerErrorHttpException('Вы удалили необходимые записи в таблице "redirect". 
                        Пожалуйста создайте изменяемым категориям и их детям запись в таблице "redirect".');
                }
            }
        }
    }
}