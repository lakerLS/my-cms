<?php

use yii\db\Migration;

/**
 * Class m181204_171358_redirect
 */
class m181204_171358_redirect extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('redirect', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'old_url' => $this->string(60),
            'new_url' => $this->string(60),
            'date' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->addForeignKey(
            'FK_redirect_category',
            'redirect',
            'category_id',
            'category',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181204_171358_redirect cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181204_171358_redirect cannot be reverted.\n";

        return false;
    }
    */
}
