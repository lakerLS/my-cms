<?php

use yii\db\Migration;

/**
 * Class m181126_083218_sections
 */
class m181126_083218_sections extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sections',
            [
                'id' => $this->primaryKey(),
                'name' => $this->string()->notNull(),
                'type' => $this->string()->notNull(),
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181126_083218_sections cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181126_083218_sections cannot be reverted.\n";

        return false;
    }
    */
}
