<?php

use yii\db\Migration;

/**
 * Class m181126_082011_order
 */
class m181126_082011_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order',
            [
                'id' => $this->primaryKey(),
                'name' => $this->string(60),
                'email' => $this->string(60)->notNull(),
                'phone' => $this->string(60)->notNull(),
                'adress' => $this->string(),
                'price_name' => $this->string(60),
                'price_id' => $this->string(60),
                'closed' => $this->tinyInteger(1)->notNull()->defaultValue(false),
                'date' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181126_082011_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181126_082011_order cannot be reverted.\n";

        return false;
    }
    */
}
