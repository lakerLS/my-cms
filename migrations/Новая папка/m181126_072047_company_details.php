<?php

use yii\db\Migration;

/**
 * Class m181126_072047_company_details
 */
class m181126_072047_company_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('company_details', [
           'id' => $this->primaryKey(),
            'image' => $this->string(),
            'indification' => $this->string(),
            'fio' => $this->string(),
            'phone_one' => $this->string(),
            'phone_two' => $this->string(),
            'phone_three' => $this->string(),
            'email_one' => $this->string(),
            'email_two' => $this->string(),
            'coordinate' => $this->string(),
            'address' => $this->string(500),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181126_072047_company_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181126_072047_company_details cannot be reverted.\n";

        return false;
    }
    */
}
