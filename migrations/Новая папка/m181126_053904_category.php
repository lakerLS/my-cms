<?php

use yii\db\Migration;

/**
 * Class m181126_053904_category
 */
class m181126_053904_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'root' => $this->integer(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'lvl' => $this->smallInteger(5)->notNull(),

            'name' => $this->string(60)->notNull(),
            'url' => $this->string(60)->notNull(),
            'type' => $this->string()->notNull(),
            'image' => $this->string(),
            'title' => $this->string(),
            'description' => $this->string(),
            'tag' => $this->string(),

            'icon' => $this->string(),
            'icon_type' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'active' => $this->tinyInteger(1)->notNull()->defaultValue(true),
            'selected' => $this->tinyInteger(1)->notNull()->defaultValue(false),
            'disabled' => $this->tinyInteger(1)->notNull()->defaultValue(false),
            'readonly' => $this->tinyInteger(1)->notNull()->defaultValue(false),
            'visible' => $this->tinyInteger(1)->notNull()->defaultValue(true),
            'collapsed' => $this->tinyInteger(1)->notNull()->defaultValue(true),
            'movable_u' => $this->tinyInteger(1)->notNull()->defaultValue(true),
            'movable_d' => $this->tinyInteger(1)->notNull()->defaultValue(true),
            'movable_l' => $this->tinyInteger(1)->notNull()->defaultValue(true),
            'movable_r' => $this->tinyInteger(1)->notNull()->defaultValue(true),
            'removable' => $this->tinyInteger(1)->notNull()->defaultValue(true),
            'removable_all' => $this->tinyInteger(1)->notNull()->defaultValue(true),
            'child_allowed' => $this->tinyInteger(1)->notNull()->defaultValue(true),
        ]);

        $this->createIndex('tree_NK1', 'category', 'root');
        $this->createIndex('tree_NK2', 'category', 'lft');
        $this->createIndex('tree_NK3', 'category', 'rgt');
        $this->createIndex('tree_NK4', 'category', 'lvl');
        $this->createIndex('tree_NK5', 'category', 'active');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181126_053904_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181126_053904_category cannot be reverted.\n";

        return false;
    }
    */
}
