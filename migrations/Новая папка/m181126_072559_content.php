<?php

use yii\db\Migration;

/**
 * Class m181126_072559_content
 */
class m181126_072559_content extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('content', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'type' => $this->string(60)->notNull(),
            'url' => $this->string(60)->notNull(),
            'name' => $this->string()->notNull(),
            'image' => $this->text(),
            'text' => $this->text(),
            'price' => $this->integer(),
            'price_old' => $this->integer(),
            'title' => $this->string(),
            'description' => $this->string(),
            'keywords' => $this->string(),
            'section' => $this->string(60),
            'author_id' => $this->integer()->notNull(),
            'source' => $this->string(),
            'hits' => $this->integer(),
            'date' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'position' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'FK_content_category',
            'content',
            'category_id',
            'category',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181126_072559_content cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181126_072559_content cannot be reverted.\n";

        return false;
    }
    */
}
