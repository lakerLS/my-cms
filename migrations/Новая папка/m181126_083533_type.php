<?php

use yii\db\Migration;

/**
 * Class m181126_083533_type
 */
class m181126_083533_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('type',
            [
               'id' => $this->primaryKey(),
               'type' => $this->string(60)->notNull(),
               'name' => $this->string(60)->notNull(),
               'select' => $this->string()->notNull(),
               'many' => $this->integer()->notNull()->defaultValue(0),
               'one' => $this->integer()->notNull()->defaultValue(0),
               'description' => $this->string(60),
               'tpl_v1' => $this->text(),
               'tpl_v2' => $this->text(),
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181126_083533_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181126_083533_type cannot be reverted.\n";

        return false;
    }
    */
}
