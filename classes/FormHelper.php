<?php

namespace app\classes;

use app\models\Category;
use app\models\Content;
use app\widgets\body;
use app\widgets\myModal;
use Yii;
use yii2mod\comments\models\CommentModel;


class FormHelper
{
    // Добавляем красный символ "*", если поле считается обязательным
    public static function required ()
    {
        return ['template' => '{label}<span style="color: red">*</span> {input} {error}'];
    }
}