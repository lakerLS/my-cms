<?php

namespace app\classes;

use app\models\CompanyDetails;
use himiklab\thumbnail\EasyThumbnailImage;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;


class WidHelper
{
    public static $text;
    public static $length;

    /**
     * Вывод фото с заданным соотношением сторон.
     *
     * $ratio - множитель, которым задаются нужные пропорции изображения.
     * Примеры: 1.5 - соотношение 3:2 (альбомная). 2 - 2:4(широкоформатная). 1 - квадрат. 0.667 - 2:3 (портретная).
     *
     * $number - в строке имеется несколько изображений и нужно выбрать конкретное.
     *
     * @param $model
     * @param float $ratio
     * @param integer $number
     * @return bool|string
     */
    public static function image ($model, $ratio = 1.5, $number = 0)
    {
        // Если изображение существует, то удаляем все теги и получаем чистый адрес изображения.
        if (!empty($model->image))
            preg_match_all('#<img.*src="(.*)".*>#isU', $model->image, $image);

        //Если изображение отсутствует и оно первое, то заменяется или на картинку "not found".
        elseif ($number == 0)
            $image[1][0] = '/default-image_207_200.jpg';

        /**
         * Если изображение отсутствует и оно не первое, то очищаем массив.
         * Используется когда X изображений выводится циклом, позволяет остановить цикл, когда изображений больше нет.
         */
        else
            $image = null;


        // Если изображения нет, то операции не производятся и возвращается false.
        if (!empty ($image[1][$number])) {

            // Получаем полный путь к фото 'full'.
            $urlPhoto = Yii::$app->request->hostInfo.'/'.str_replace('mini', 'full', $image[1][$number]);

            // Получаем размеры изображения.
            $size = getimagesize("$urlPhoto");

            // Используется для проверки ниже. Проверяем какая ориентация у изображения (портретная или альбомная).
            $check = $size[0] <= $size[1];

            // Если множитель больше или равен единице.
            if ($ratio >= 1)
            {
                /**
                 * Если ширина меньше высоты (портретная ориентация), то конечная ширина равна текущей ширине,
                 * а конечная высота равна текущей ширине деленной на множитель.
                 *
                 * Если ширина больше высоты (альбомная ориентация), то конечная ширина равна текущей высоте,
                 * а конечная высота равна текущей высоте деленной на множитель.
                 */
                $width = $check ? $size[0] : $size[1];
                $height = $check ? $size[0]/$ratio : $size[1]/$ratio;
            }

            // Если множитель меньше единицы.
            else
            {
                /**
                 * Если ширина меньше высоты (портретная ориентация), то конечная ширина равна текущей ширине умноженной
                 * на множитель, а конечная высота равна текущей ширине.
                 *
                 * Если ширина больше высоты (альбомная ориентация), то конечная ширина равна высоте умноженной на
                 * множитель, а конечная высота равна текущей высоте.
                 */
                $width = $check ? $size[0]*$ratio : $size[1]*$ratio;
                $height = $check ? $size[0] : $size[1];
            }

            return EasyThumbnailImage::thumbnailImg(
                $urlPhoto,
                $width,
                $height,
                EasyThumbnailImage::THUMBNAIL_OUTBOUND,
                [
                    'width' => '100%',
                    'alt' => $model->name,
                ]
            );
        }

        else {
            return false;
        }
    }

    // Краткое содержимое.
    public static function text ($model, $length = '61')
    {

        $strip = $model->text;
        $explode = explode(' ', $strip);
        $substr = array_slice($explode, 0, $length);
        $implode = implode(' ', $substr);

        self::$text = count($explode);
        self::$length = $length;

        echo $implode;
    }

    // Читать полность (отображение или скрытие).
    public static function readMore($model = null, $style = '0')
    {
        if (self::$text >= self::$length && $style == '0')
        {
            echo Html::beginTag('div', ['class' => 'link pull-right icon-link']);
                echo Html::a('Читать полностью', ['content/one', 'model' => $model]);
            echo Html::endTag('div');
        }

        elseif (WidHelper::$text >= WidHelper::$length && $style == '1')
        {
            echo Html::a('Подробнее', ['content/one', 'model' => $model], ['class' => 'read-more btn btn-sm btn-primary']);
        }
    }

    // Редактировать или удалить (для администратора).
    public static function updateDelete($model)
    {
        // если страница содержит много записей (hasMany) и админ
        if (Yii::$app->controller->action->id == 'many' && Yii::$app->user->can('admin'))
        {
            // поднять запись на одну позицию
            echo Html::tag('a', '',
                [
                    'class' => 'glyphicon glyphicon-triangle-top',
                    'href' => '/content/move-up?id='.$model->id.'&redirect='.Yii::$app->request->pathInfo,
                    'data-method' => 'post'
                ]);

            // опустить запись на одну позицию
            echo Html::tag('a', '',
                [
                    'class' => 'glyphicon glyphicon-triangle-bottom',
                    'href' => '/content/move-down?id='.$model->id.'&redirect='.Yii::$app->request->pathInfo,
                    'data-method' => 'post'
                ]);
        }

        if (Yii::$app->user->can('admin'))
        {
            // редактировать
            echo Html::tag('a', '', [
                'class' => 'glyphicon glyphicon-pencil',
                'href' => '#',
                'title' => 'Редактировать',
                'data-toggle' => 'modal',
                'data-target' => '#update-'.$model->id,
            ]);

            // Если запись будет просматриваться полностью (hasOne), то редирект переведет на список записей в которой она находилась (hasMany)
            $redirect = '/'.Yii::$app->controller->action->id == 'many' ? Yii::$app->request->pathInfo : Url::to(['content/many']);

            // удалить
            echo Html::tag('a', '',
                [
                    'class' => 'glyphicon glyphicon-trash',
                    'href' => '/content/delete?id='.$model->id.'&redirect='.$redirect,
                    'title' => 'Удалить',
                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                    'data-method' => 'post'
                ]);
        }
    }

    // Теги для бокового меню.
    public static function tagSidebar ($meta, $model = null)
    {
        if (!empty ($model->keywords))
        {
            $keywords = explode(",", $model->keywords);

            foreach ($keywords as $key)
            {
                echo Html::a(trim($key), ['content/one', 'model' => $model]), ['class' => 'tag'];
            }
        }

        else {
            $keywords = explode(",", $meta->tags);

            foreach ($keywords as $key)
            {
                echo Html::a(trim($key), ['content/many']), ['class' => 'tag'];
            }
        }
    }

    // Теги для блога.
    public static function tagBlog ($model)
    {
        if (!empty ($model->keywords))
        {
            $keywords = explode(",", $model->keywords);

            $count = 0;
            foreach ($keywords as $key)
            {
                $count++;

                echo Html::a(trim($key), ['content/one', 'model' => $model]).
                $count == 3 ? '/ ' : null;

                if ($count == '3')
                    break;
            }
        }
    }

    // Счетчик комментариев.
    public static function commentCount ($model)
    {
        $count = 0;
        foreach ($model->comment as $value) {
            if (!empty($value->entityId && $value->status == '1')){
              $count++;
            };
        }
        //$count = CommentModel::find()->where(['status' => '1'])->andWhere(['entityId' => $model->id])->count();

        echo 'Комментариев: '.$count;
    }

    // Проверка на автора для редактирования.
    public static function validateAuthor ($model)
    {
        if (Yii::$app->user->identity->id == $model->customer_id || Yii::$app->user->can('admin')) {
            echo 'ok';
        }
    }

    // Координаты для карты гугл.
    public static function coordinate ($count)
    {
        $details = CompanyDetails::find()->one();

        $details = str_replace(' ', '', $details->coordinate);
        $details = explode(',', $details);

        return $details[$count];
    }

    // Вывод оповещения, если форма отправлена успешно.
    public static function setFlash($key, $text)
    {
        if (Yii::$app->session->hasFlash($key)) { ?>

            <!-- NOT LOGGED IN -->
            <div class="card card-default col-lg-12" style="min-height: 40px; background-color: #469ef5; color: white; margin-bottom: 50px">
                <div class="card-block" style="text-align: center; margin-top: 7px">
                    <?= $text ?>
                </div>
            </div>
            <!-- /NOT LOGGED IN -->
        <? }
    }

    // Вывод ошибки, если адрес уже существует.
    public static function alertError($key, $text)
    {
        if (Yii::$app->session->hasFlash($key)) { ?>

            <div class="alert  alert-icon alert-danger" role="alert">
                <i class="fa fa-times" style="top: 10px"></i>
                <?= $text ?>
            </div>
        <? }
    }


    // Пример карандашей: modalBody::pencil('phone')
}