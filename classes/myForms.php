<?php
namespace app\classes;

use app\models\form\ContactForm;
use app\widgets\modalButton;
use dektrium\user\models\LoginForm;
use Yii;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use himiklab\yii2\recaptcha\ReCaptcha;

/* @var $form \yii\widgets\ActiveForm */
class myForms
{
    // Форма для быстрой отправки email сообщения с сайта
    public static function Contacts()
    {
        $contacts = new ContactForm();
        $form = ActiveForm::begin(['action' => '/site/mail?redirect='.Yii::$app->request->pathInfo]);

            // Для обязательных полей добавляем красную * с помощью 'template'
            echo $field = $form->field($contacts, 'name', FormHelper::required())->textInput(['placeholder' => 'Имя']);

            echo $field = $form->field($contacts, 'email', FormHelper::required())->textInput(['placeholder' => 'Email']);

            echo $field = $form->field($contacts, 'subject', FormHelper::required())->textInput(['placeholder' => 'Тема']);

            echo $field = $form->field($contacts, 'message', FormHelper::required())->textarea(['placeholder' => 'Сообщение']);

            //echo  $form->field($faq, 'reCaptcha')->widget(ReCaptcha::className(), [
            //  'name' => 'NewUserRegisterForm[reCaptcha]',
            //])->label(false);

            echo '<div>';
                echo Html::submitButton('Отправить', ['class' => 'col-md-3 btn btn-primary contact-submit', 'name' => 'contact-button']);
            echo '</div>';

        ActiveForm::end();
    }

    // Форма для быстрой отправки email сообщения с сайта. Распологается в подвале сайта
    public static function FooterContacts ()
    {
        $model = new ContactForm();

        $form = ActiveForm::begin(['action' => '/site/mail?redirect='.Yii::$app->request->pathInfo]);

            echo '<div class="form-group has-feedback mb-10" style="display:flex">';
                echo $field = $form->field($model, 'name',
                    [
                        'template' => "{label}\n{input}\n<i class=\"fa fa-user form-control-feedback\"></i>",
                        'options' => ['class' => 'col-lg-6', 'style' => 'padding-right: 2px; padding-left: 0px'],
                        'inputOptions' => ['class' => 'form-control']
                    ])->textInput(['placeholder' => 'Имя'])->label(false);

                echo $field = $form->field($model, 'email',
                    [
                        'template' => "{label}\n{input}\n<i class=\"fa fa-envelope form-control-feedback\"></i>",
                        'options' => ['class' => 'col-lg-6', 'style' => 'padding-right: 0px; padding-left: 2px'],
                        'inputOptions' => ['class' => 'form-control']
                    ])->textInput(['placeholder' => 'Email'])->label(false);
            echo '</div>';

            echo $field = $form->field($model, 'subject',
            [
                'template' => "{label}\n{input}\n<i class=\"fa fa-paper-plane form-control-feedback\"></i>",
                'options' => ['class' => 'form-group has-feedback mb-10'],
            ])->textInput(['placeholder' => 'Тема'])->label(false);

            echo $field = $form->field($model, 'message',
            [
                'template' => "{label}\n{input}\n<i class=\"fa fa-pencil form-control-feedback\"></i>",
                'options' => ['class' => 'form-group has-feedback mb-10'],
            ])->textarea(['rows' => '4', 'placeholder' => 'Сообщение'])->label(false);

            //echo  $form->field($faq, 'reCaptcha')->widget(ReCaptcha::className(), [
            //  'name' => 'NewUserRegisterForm[reCaptcha]',
            //])->label(false);

            echo '<div>';
                echo Html::submitButton('Отправить', ['class' => 'margin-clear submit-button btn btn-default', 'name' => 'contact-button']);
            echo '</div>';
        ActiveForm::end();
    }

    // форма для входа и авторизации в админку из подвала.
    public static function adminka ()
    {
        if (Yii::$app->user->isGuest) {
            echo 'Copyright © 2017 The Project by '.modalButton::login().'. All Rights Reserved';
        }
        else {

            $form = ActiveForm::begin(['options' => ['class' => 'credit-texts', 'style' => 'text-align: center']]);

                echo 'Copyright © 2017 The Project by ';

                echo Html::a('админка ', ['/category'], ['data-method' => 'post']);
                echo Html::a(' | выход', ['/user/security/logout'], ['data-method' => 'post']);

                echo '. All Rights Reserved';

                echo Html::hiddenInput('redirect', Yii::$app->request->pathInfo);

            ActiveForm::end();
        }
    }

    // Форма для модального окна авторизации
    public static function modalLogin ()
    {
        $form = ActiveForm::begin(['action' => '/user/login']);

            $login = \Yii::createObject(LoginForm::className());

            echo $form->field($login, 'login')->textInput(['id' => 'login_modal']);

            echo $form->field($login, 'password')->passwordInput(['id' => 'password_modal'])->label('Пароль (<a href="/user/forgot" tabindex="5">Забыли пароль?</a>)');

            echo Html::hiddenInput('redirect', Yii::$app->request->pathInfo);

            echo Html::submitButton('Авторизоваться', ['class' => 'btn btn-primary btn-block']);

        ActiveForm::end();
    }
}