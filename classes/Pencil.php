<?php

namespace app\classes;


use app\models\Content;
use Yii;
use app\widgets\extended\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


class Pencil
{
    // Кнопка редактирования
    static public function editDelete($model, $editDelete = '')
    {
        if(Yii::$app->user->can('admin'))
        {
            ob_start();
            ?>



            <?php
            if($editDelete == 'edit')
            {
                ?>
                <a  class="admin"  title="Редактировать" href="<?=\yii\helpers\Url::to(['content/update'])?>?id=<?=$model->id?>&type=pencil&redirect=<?= Yii::$app->request->pathInfo ?>">&#9998;</a>
                <?
            }
            ?>


            <?php
            if($editDelete == 'delete')
            {
                ?>
                <?php  ActiveForm::begin(); ?>

                <?= Html::a('&#10008;', ['content/del', 'id' => $model->id], ['data-method' => 'post','title'=>'удалить', 'class'=>'admin','data-confirm'=>'Вы уверены, что хотите удалить этот элемент?']) ?>
                <?php ActiveForm::end(); ?>

                <?php
            }

            return  $object = ob_get_clean();


        }
    }

    /*
     * Карандаш, который позволяет вывести необходимый вам текст или изображение в любом месте страницы.
     * Первая переменная обязательна, указываем "имя" карандаша.
     * Вторая переменная может иметь любое содержание, указывается, если необходимо удалить теги <p> от CKEditor.
     */
    static public function pencil($url, $del = '')//отображает текст если его нет то добавит
    {
        $model = Content::findOne(['url' => $url]);
        if(!empty($model->text))
        {
            $str = trim($model->text);

            if (!empty($del))
            $str = substr($str,3,-4);

            echo $str;
            echo Pencil::editDelete($model, 'edit');
        }

        elseif(Yii::$app->user->can('admin')) {
            ?>
                <a style="color: #4d9ae1"
                 href="/content/create?type=pencil&url=<?=$url?>&category_id=1&redirect=<?= Yii::$app->request->pathInfo ?>">
                Добавить запись</a>
            <?
        }
    }
}