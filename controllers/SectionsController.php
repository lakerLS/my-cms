<?php

namespace app\controllers;

use Yii;
use app\models\Sections;
use app\models\search\SectionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SectionsController implements the CRUD actions for Sections model.
 */
class SectionsController extends Controller
{

    public $layout = 'admin';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

            'as AccessBehavior' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'rules' =>
                    ['sections' =>
                        [
                            [
                                'allow' => true,
                                'roles' => ['admin'],
                            ],
                        ]
                    ]
            ],
        ];
    }

    /**
     * Lists all Sections models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SectionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sections model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sections model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sections();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            // Если создание записи из админки
            if(!empty(Yii::$app->request->get('category_id') && !empty(Yii::$app->request->get('type'))))
                return $this->redirect('/content/create?category_id='.Yii::$app->request->get('category_id').'&type='.Yii::$app->request->get('type'));

            // Если редактирование из админки
            elseif (!empty(Yii::$app->request->get('id')))
                return $this->redirect('/content/update?id='.Yii::$app->request->get('id'));

            // Если создание или редактирование записи из модального окна
            elseif(!empty (Yii::$app->request->get('redirect')))
                return $this->redirect(Yii::$app->request->get('redirect'));

            // Создание раздела из админки
            else
                return $this->redirect('/sections?active=3');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Sections model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('/sections?active=3');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sections model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/sections?active=3']);
    }

    /**
     * Finds the Sections model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sections the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sections::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
