<?php

namespace app\controllers;

use app\models\Category;
use app\models\Faq;
use app\models\Order;
use app\models\Redirect;
use app\models\search\ContentSearch;
use Yii;
use app\models\Content;
use app\widgets\extended\pagination\Pagination;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContentController implements the CRUD actions for Content model.
 */
class ContentController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'as AccessBehavior' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'rules' =>
                    ['content' =>
                        [
                            [
                                'actions' => ['main', 'many', 'one'],
                                'allow' => true,
                            ],
                            [
                                'allow' => true,
                                'roles' => ['admin'],
                            ],
                        ]
                    ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Отображения категории hasMany
     * Lists all Content models.
     * @return mixed
     */
    public function actionMany(Category $current, $redirect = null)
    {

        if(!empty($redirect))
        {
            return $this->redirect($redirect, 301);
        }

        $order = new Order();
        $faq = new Faq();
        $model = new Content;

        $searchModel = new ContentSearch();
        $dataProvider = $searchModel->search($current);

        // Подключаем свою пагинацию для динамических адресов
        $pages = new Pagination([
            'defaultPageSize' => $dataProvider->pagination->defaultPageSize,
            'totalCount' => $dataProvider->query->count(),
            'route' => Yii::$app->request->pathInfo,
        ]);

        if(!empty(Yii::$app->session['model']))
        {
            $model->load(Yii::$app->session['model']);
            unset(Yii::$app->session['model']);
        }


        return $this->render($current->type.'/'.$current->type.'Many', [
            'dataProvider' => $dataProvider,
            'meta' => $current,
            'model' => $model,
            'order' => $order,
            'faq' => $faq,
            'pages' => $pages,
        ]);

    }

    /**
     * Отображение конкретной записи.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionOne(Content $current, $redirect = null)
    {
        if(!empty($redirect))
        {
            return $this->redirect($redirect.'/'.$current->url, 301);
        }

        if(!empty(Yii::$app->session['model']))
        {
            $current->load(Yii::$app->session['model']);
            unset(Yii::$app->session['model']);
        }

        return $this->render($current->type.'/'.$current->type.'One', [
            'model' => $current,
            'meta' => $current->category,
        ]);
    }

    /**
     * Creates a new Content model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout='admin';

        $model = new Content();

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            unset(Yii::$app->session['model']);

            if (Yii::$app->request->get('redirect') != '/content/create')
                return $this->redirect([Yii::$app->request->get('redirect')]);

            else
                return $this->redirect('../category');
        }

        elseif ($model->load(Yii::$app->request->post()) && !$model->save())
        {

            Yii::$app->session['model'] = Yii::$app->request->post();

            //echo '<pre>'; print_r(Yii::$app->session['model']); echo '</pre>';
            
            Yii::$app->session->setFlash('notUnique');
            return $this->redirect([Yii::$app->request->get('redirect')]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Content model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout='admin';

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            // Если редактируется не из админки
            if (Yii::$app->request->get('redirect') != 'content') {

                if (Yii::$app->request->get('update') == 'many')
                    return $this->redirect(Yii::$app->request->get('redirect'));

                else
                    return $this->redirect(Yii::$app->request->get('redirect') . '/' . $model->url);
            }

            // Если редактируется в админке
            else
                return $this->redirect('../category');
        }

        elseif ($model->load(Yii::$app->request->post()) && !$model->save())
        {

            Yii::$app->session['model'] = Yii::$app->request->post();

            //echo '<pre>'; print_r(Yii::$app->session['model']); echo '</pre>';

            Yii::$app->session->setFlash('notUnique');
            return $this->redirect([Yii::$app->request->get('redirect')]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Content model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if (!empty(Yii::$app->request->get('redirect')))
            return $this->redirect([Yii::$app->request->get('redirect')]);

        else
            return $this->redirect('../category');
    }

    // Для виджета position
    public function actionMoveUp($id)
    {
        $this->findModel($id)->moveNext();

        if (!empty(Yii::$app->request->get('redirect')))
            return $this->redirect([Yii::$app->request->get('redirect')]);

        else
            return $this->redirect('../category');
    }

    // Для виджета position
    public function actionMoveDown($id)
    {
        $this->findModel($id)->movePrev();

        if (!empty(Yii::$app->request->get('redirect')))
            return $this->redirect([Yii::$app->request->get('redirect')]);

        else
            return $this->redirect('../category');
    }

    protected function findModel($id)
    {
        if (($model = Content::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
