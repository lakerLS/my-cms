<?php

namespace app\controllers;

use app\models\Category;
use app\models\search\ContentSearch;
use Yii;
use app\models\search\CategorySearch;
use yii\web\Controller;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    public $layout = 'admin';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'as AccessBehavior' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'rules' =>
                    ['category' =>
                        [
                            [
                                'allow' => true,
                                'roles' => ['admin'],
                            ],
                        ]
                    ]
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        // получаем данные категорий
        $treeViewModel = new CategorySearch();
        $treeView = $treeViewModel->treeView();

        // получаем данные контента
        $searchModel = new ContentSearch();
        $dataProvider = $searchModel->modify(Yii::$app->request->queryParams);

        return $this->render('index', [
            'treeView' => $treeView,
            'treeCheck' => Category::find()->one(),

            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
