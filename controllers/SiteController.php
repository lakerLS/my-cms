<?php

namespace app\controllers;

use app\models\Category;
use app\models\Content;
use app\models\form\ContactForm;
use app\models\search\ContentSearch;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionMail()
    {
        // Отправка сообщений на почту админу
        $contacts = new ContactForm();
        if ($contacts->load(Yii::$app->request->post()) && $contacts->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('formSubmitted');
        }

        if (empty(Yii::$app->request->get('redirect')))
            return $this->redirect('/');

        else
            return $this->redirect(Yii::$app->request->get('redirect'));
    }

    public function actionAdd()
    {
        $request = Yii::$app->request;

        // получаем товар из базы
        $product = Content::findOne(['id' => $request->post('Content')['id']]);


        if ($product) {
            //создаём сессию если её ещё нет
            $session = Yii::$app->session;
            if (!isset($session['cart'])) $session['cart'] = [];


            $cart = $session['cart'];

            if(empty($request->post('quantity')))
            {
                $sum = 1;
            }
            else{
                $sum = $request->post('quantity');
            }



            if (isset($cart[$request->post('id')])) {
                $sum = $cart[$request->post('id')]+$sum;
            }

            //корзину в сессию
            if (!empty($cart[$request->post('id')])) {
                $cart[$request->post('id')] =  $sum;
            } else {
                $cart[$request->post('id')] = $sum;
            }
            $session['cart'] = $cart;
        }
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new Content();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

}
