<?php
namespace app\widgets\megaMenu;

use app\models\Category;
use yii\helpers\Html;


class megaMenu
{

    public static function body()
    {
        // получаем дерево пунктов меню.
        $categorys = Category::findOne(['lvl' => '0'])->children()->all();

        // в конце цикла присваивается значение объекта. Начинаем всегда с главной.
        $lvl = '1';
        $count = 'first';

        // Этот ul оборачивает всю конструкцию.
        echo Html::beginTag('ul', optionsTag::ul('main'))."\n";

            // Формируем пункты меню
            foreach($categorys as $category)
            {
                // если пункт меню не изменился по уровню вложенности и это не первая итерация цикла.
                if($lvl == $category->lvl && $count == 'some')
                    echo Html::endTag('li')."\n";

                // если пункт меню опустился по вложенности на "х" уровней.
                if ($lvl < $category->lvl)
                    echo Html::beginTag('ul', optionsTag::ul('nested', $category->lvl))."\n";

                // если пункт меню по вложенности поднялся на "х" уровней.
                if($lvl > $category->lvl) {

                    // Если разница во вложенности равна двум, то цикл сработает 2 раза, если разница равна одному, то 1 раз и т.д.
                    for($cycle = $lvl - $category->lvl; $cycle; $cycle--)
                    {
                        echo Html::endTag('li')."\n";
                        echo Html::endTag('ul')."\n";
                    }
                }

                echo Html::beginTag('li', optionsTag::li($categorys, $category))."\n";

                echo Html::tag('a', $category->name, optionsTag::a($categorys, $category))."\n";

                $lvl = $category->lvl;

                $count = 'some';
            }

        echo Html::endTag('ul');
    }
}