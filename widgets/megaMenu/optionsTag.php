<?php
namespace app\widgets\megaMenu;

use Yii;

class optionsTag
{
    // class для тегов <ul>, <li>, <a>.

    const UL_all = 'navbar-nav ml-xl-auto'; // <ul> оборачивает всю конструкцию
    const UL_nested_one = 'dropdown-menu'; // <ul> пункт меню вложенный на один уровень
    const UL_nested_more = 'dropdown-menu'; // <ul> пункт меню вложенный на два и более уровней


    // Активный пункт меню, корневой.
    const LI_active_main = 'nav-item dropdown active'; // <li>

    // Активный пункт меню.
    const LI_active = 'dropdown active'; // <li>


    // Пункт меню без вложеностей, корневой.
    const LI_lonely_main = 'nav-item'; // <li>
    const A_lonely_main = 'nav-link'; // <a>

    // Пункт меню без вложеностей.
    const LI_lonely = false; // <li>
    const A_lonely = false; // <a>


    // Пункт меню с вложенностями, корневой.
    const LI_has_nesting_main = 'nav-item dropdown'; // <li>
    const A_has_nesting_main = 'nav-link dropdown-toggle'; // <a>

    // Пункт меню с вложенностями.
    const LI_has_nesting = 'dropdown'; // <li>
    const A_has_nesting = 'dropdown-toggle'; // <a>





    public static function ul($value, $lvl = null)
    {
        // если пункт меню корневой
        if ($value == 'main')
            return ['class' => self::UL_all];

        // если пункт меню вложен минимум дважды (для выбора в какую сторону формировать список пунктов меню)
        elseif ($value == 'nested' && $lvl >= 3)
            return ['class' => self::UL_nested_more];

        // если пункт меню вложен один раз
        elseif ($value == 'nested')
            return ['class' => self::UL_nested_one];

        else
            return false;
    }

    public static function li($categorys, $category)
    {
        $ex = explode( '/', Yii::$app->request->pathInfo);
        $e = array_pop($ex);
        $i = implode('/', $ex);


        /**
         * Если пункт меню активный, выделить. Ниже перечислены все проверки сверху вниз.
         *
         * Сравниваем полный текущий адрес страницы со всеми пунктами меню, если адреса частично совпадают
         * проверка успешна. Адрес пункта меню для сравнения формируется абсолютный. Пример: http//example/item .
         *
         * Сравниваем текущий адрес с пунктом меню. Совпадение только одно, для текущего пункта меню.
         *
         * Сравниваем текущий адрес с главной сайта.
         */
        if (
            !empty(substr_count(Yii::$app->request->absoluteUrl,
                         Yii::$app->request->hostInfo.url::create($categorys, $category->id).'/')) ||
            '/'.Yii::$app->request->pathInfo == url::create($categorys, $category->id) ||
            '/'.Yii::$app->request->pathInfo == '/index'
        )
            // если пункт меню корневой, применяется первый class.
            return $category->lvl == '1' ? ['class' => self::LI_active_main] : ['class' => self::LI_active];

        // если пункт меню не имеет вложенностей
        elseif ($category->lft+1 == $category->rgt)
            // если пункт меню корневой, применяется первый class.
            return $category->lvl == '1' ? ['class' => self::LI_lonely_main] : ['class' => self::LI_lonely];

        // если пункт меню имеет вложенность
        else
            // если пункт меню корневой, применяется первый class.
            return $category->lvl == '1' ? ['class' => self::LI_has_nesting_main] : ['class' => self::LI_has_nesting];
    }

    public static function a($categorys, $category)
    {
        // если нет вложенности
        if ($category->lft+1 == $category->rgt) {
            // если пункт меню корневой, применяется первый class.
            return $category->lvl == '1' ?
                ['class' => self::A_lonely_main, 'href' => url::create($categorys, $category->id)] :
                ['class' => self::A_lonely, 'href' => url::create($categorys, $category->id)];
        }
        // если есть вложенность
        else
            // если пункт меню корневой, применяется первый class.
            return $category->lvl == '1' ?
                ['class' => self::A_has_nesting_main, 'href' => url::create($categorys, $category->id)] :
                ['class' => self::A_has_nesting, 'href' => url::create($categorys, $category->id)];

    }
}