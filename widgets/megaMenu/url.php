<?php
namespace app\widgets\megaMenu;


class url
{
    // составляет адреса для пунктов меню и хлебных крошек
    public static function create ($categorys, $id)
    {
        $previous = $categorys['0']; // сдесь хранятся данные из предыдущей записи, для сравнения с текущей записью
        $path = null; // сдесь будет лежать путь к категории

        $array = null; // объявляем переменную, в которой будет формироваться массив с адресами: $array[$id] => [$finish_url]

            foreach ($categorys as $current) // формируем пути ко всем дерикториям
            {

                // $current - текущая запись, $previous - предыдущая запись
                if ($current->lft > $previous->lft && $current->rgt < $previous->rgt) {
                    $path = $path . '/' . $previous->url; // формируем путь к категории
                } // если запись не имеет такой же путь как и предыдущая и она лежит в корне, то обнуляем переменную и составляем путь заново
                elseif ($current->lvl != $previous->lvl && $current->lvl == '1')
                    $path = null;

                // если запись имеет другую вложенность, то удаляем последний элемент.
                elseif ($current->lvl != $previous->lvl) {

                    $path = explode('/', $path);

                    // узнаем разницу в уровне и производим соответствующее количество циклов для удаления не нужной части пути.
                    for ($cycle = $previous->lvl - $current->lvl; $cycle; $cycle--) {
                        array_pop($path);
                    }

                    $path = implode('/', $path);
                }


                // выводим путь категории склееный с адресом записи
                if ($current->url != '/') // если не главная
                    $finish_url = $path.'/'.$current->url;

                else // если главная
                    $finish_url = '/';

                $previous = $current; // пресваиваем значение в конце цикла, тем самым получаем предыдущую запись на следующей проходке

                $array[$current->id] = $finish_url; // собираем массив, т.к. 2 параллельных массива, один для адресов, другой для формирования меню
            }

        return $array[$id];
    }
}