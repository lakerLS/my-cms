<?php
namespace app\widgets\megaMenu;

use Yii;
use yii\helpers\Html;


class breadcrumbs
{
    public static function body($meta, $model = null)
    {
        // Получаем родителей у текущего пункта меню.
        $category = $meta->parents()->andWhere(['<>', 'lvl', '0'])->all();;

        $url = null; // формируются урлы родителей.
        $name = null; // урлы родителей оборачиваются в теги.

        // формируем хлебные крошки склеивая предыдущую и текущую запись.
        foreach ($category as $value) {

            $url = url::create($category, $value->id);

            $name = $name.
            Html::beginTag('li', ['class' => 'breadcrumb-item']).
                Html::tag('a', $value->name, ['href' => $url]).
            Html::endTag('li');
        }

        // если крошки распологаются в списке записей viewMany, то к крошкам добавляем неактивный, текущий пункт меню
        if (empty($model->name))
        {
            return $name.Html::tag('li', $meta->name, ['class' => 'breadcrumb-item active']);
        }

        // если крошки в отдельной записи viewOne, то прибавляем к родителям текущий пункт меню, а так же неактивное наименование записи
        else
        {
            $currentMenu = $url.'/'.$meta->url;

            return $name.
                    Html::beginTag('li', ['class' => 'breadcrumb-item']).
                        Html::tag('a', $meta->name, ['href' => $currentMenu]).
                    Html::endTag('li').
                    Html::tag('li', $model->name, ['class' => 'breadcrumb-item active']);
        }
    }
}