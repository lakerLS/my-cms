<?php

namespace app\widgets;

use Yii;

class Counter
{
    /**
     * Подсчет количества записей в базе данных
     * Две переменные, $nameTable - имя таблицы, $condition - указываем массивом условие поиска
     */
    public static function post ($nameTable, $condition)
    {
        // Указываем расположение модели.
        $table = '\\app\\models\\'.$nameTable;

        return $table::find()->where($condition)->count();
    }
}