<?php

namespace app\widgets;

use app\classes\myForms;
use app\models\Category;
use app\models\Content;
use app\models\Order;
use app\widgets\extended\Modal;
use Yii;
use yii\helpers\Html;

class modalBody
{
    // Создание записи. Используется в layout.
    public static function create ($model, $meta)
    {
        if (Yii::$app->user->can('admin')) {

            ob_start()  //Все, что заключено в ob_start() и ob_get_clean будет заключенно в переменную ?>

                <div class="button-fixed" style="margin-bottom: 20px">
                    <button type="submit" class="btn btn-sm btn-default-transparent" style="cursor: pointer" title="Создать">Добавить запись &nbsp; <i class="fa fa-paper-plane"></i></button>
                </div>

            <? $object = ob_get_clean() ?>

            <?php // начало всплывающего окна
            Modal::begin([
                'toggleButton' => [
                    'label' => $object,
                    'tag' => 'div',
                    'class' => 'price-grid-title',
                ],

                'size' => 'modal-lg',
                'header' => '<h4 class="modal-title">Создать запись</h4>',
            ]);


            echo Yii::$app->view->render('//content/create', [
                'model' => $model,
                'meta' => $meta
            ]);

            Modal::end();
        }
    }

    // Редактирование записи (отображение кнопки отдельно). Используется в layout.
    public static function update ($model, $meta = null)
    {
        if (Yii::$app->user->can('admin'))
        {
            // начало всплывающего окна
            Modal::begin([
                'id' => 'update-'.$model->id,

                'toggleButton' => false,

                'size' => 'modal-lg',
                'header' => '<h4 class="modal-title">Редактировать: '.$model->name.'</h4>',
            ]);

            echo Yii::$app->view->render('//content/update', [
                'model' => $model,
                'meta' => $meta,
            ]);

            Modal::end();
        }
    }

    // Заказ услуг
    public static function services ($model)
    {
        // начало всплывающего окна
        Modal::begin([

            'id' => 'services-'.$model->id,
            'toggleButton' => false,

            'header' => '<h4 class="modal-title">Оформление заявки</h4>',
        ]);

        $order = new Order();

        echo Yii::$app->view->render('/order/create', [
            'order' => $order,
            'model' => $model
        ]);

        Modal::end();
    }

    // Карандаш.
    public static function pencil($name, $meta = null, $model = null)
    {
        // Если карандаш размещен в списке (hasMany), то добавляем к имени адрес категории.
        if (!empty($meta) && empty($model))
            $url = $meta->url.'-'.$name;

        // Если карандаш размещен в записи (hasOne), то добавляем к имени адрес записи.
        elseif (!empty($model))
            $url = $model->url.'-'.$name;

        // Если запись распологается в layout, то привязываем ее к корневой категории.
        else
        {
            $meta = Category::findOne(['lvl' => '0']);
            $url = $name;
        }

        // Ищем карандаш. url всегда уникальный.
        $content = Content::find()->where(['category_id' => $meta->id])->andWhere(['url' => $url])->one();

        // Если карандаш не существует, создаем экземпляр класса для создания записи.
        if(empty($content->type))
            $content = new Content();

        /**
         * Если текст существует и его смотрит не админ, то отобразить
         * (для админа отображение идет ниже, с возможностью вывода модального окна).
         */
        if(!empty($content->text) && Yii::$app->user->can('admin') != true)
            echo $content->text;

        // Если админ, то выводим возможность редактирования и создания карандаша.
        if(Yii::$app->user->can('admin'))
        {
            // начало всплывающего окна
            Modal::begin([
                'toggleButton' => [
                    'label' => !empty($content->text) ? $content->text : '<b style="color: #00a3d9">Добавить текст</b>',
                    'tag' => 'span',
                    'class' => 'admin',
                    'style' => 'cursor: pointer; border-bottom: dashed 1px #0088cc;',
                    'title' => 'Редактировать'
                ],

                'size' => 'modal-lg',
                'header' => empty($content->text) ?
                    '<h4 class="modal-title">Создание записи</h4>' :
                    '<h4 class="modal-title">Редактирование записи</h4>',
                'options' => ['style' => 'margin-top: 0px']
            ]);

            // Собираем массив для создания и редактирования карандаша.
            $pencil['category_id'] = $meta->id;
            $pencil['type'] = 'pencil';
            $pencil['url'] = $url;
            $pencil['name'] = $name;

            $pencil['id'] = strstr($url, '-'); // используем как id для поля text CKEDITOR

            echo Yii::$app->view->render(empty($content->type) ? '//content/create' : '//content/update' ,
                [
                'model' => $content,
                'pencil' => $pencil,
                ]
            );

            Modal::end();
        }
    }

    // Для редактирования слайдера

    /**
     * @param integer $count
     * @param $model
     */
    public static function sliderUpdate($count, $model)
    {
        if (Yii::$app->user->can('admin')) {

            ob_start()  //Все, что заключено в ob_start() и ob_get_clean будет заключенно в переменную
            ?>

            <div class="button-fixed" style="margin-bottom: <?= $count ?>px; margin-left: 38px">
                <button type="submit" class="btn btn-sm btn-default-transparent" style="cursor: pointer" title="Редактировать">Редактировать: <?= $model->name ?> &nbsp;<i class="fa fa-pencil"></i></button>
            </div>

            <?
            $object = ob_get_clean();

            // начало всплывающего окна
            Modal::begin([
                'toggleButton' => [
                    'label' => $object,
                    'tag' => 'div',
                    'class' => 'price-grid-title',
                ],

                'size' => 'modal-lg',
                'header' => 'Редактирование: '.$model->name,
            ]);

            echo Yii::$app->view->render('//content/update', [
                'model' => $model,
                'slider' => 'slider',
            ]);

            Modal::end();

            echo Html::beginTag('div', ['class' => 'button-fixed', 'style' => 'margin-bottom:'. $count+1 .'px']);
                echo Html::tag('a', null,
                    [
                        'class' => 'btn btn-sm btn-default-transparent glyphicon glyphicon-trash slider-trash',
                        'href' => '/content/delete?id='.$model->id.'&redirect=/',
                        'title' => 'Удалить',
                        'data-confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                        'data-method' => 'post',
                    ]
                );
            echo Html::endTag('div');
        }
    }

    // Для создания слайдера
    public static function sliderCreate($model)
    {
        if (Yii::$app->user->can('admin')) {

            ob_start()  //Все, что заключено в ob_start() и ob_get_clean будет заключенно в переменную
            ?>

            <div class="button-fixed" style="margin-bottom: 20px">
                <button type="submit" class="btn btn-sm btn-default-transparent" style="cursor: pointer"  title="Создать">Добавить слайдер <i class="fa fa-paper-plane"></i></button>
            </div>

            <?
            $object = ob_get_clean();

            // начало всплывающего окна
            Modal::begin([
                'toggleButton' => [
                    'label' => $object,
                    'tag' => 'div',
                    'class' => 'price-grid-title',
                ],

                'size' => 'modal-lg',
                'header' => '<h4 class="modal-title">Создать запись</h4>',
            ]);

            echo Yii::$app->view->render('//content/create', [
                'model' => $model,
                'slider' => 'slider',
            ]);

            Modal::end();
        }
    }

    // Авторизация
    public static function login ()
    {
        if (Yii::$app->user->isGuest) {
            Modal::begin([
                //'size' => 'modal-lg',
                'header' => '<h4 class="modal-title">Авторизация</h4>',
                'id' => 'modal-login',

                'toggleButton' => [

                    'label' => 'вход',
                    'tag' => 'span',
                    // Скрываем кнопку и выводим отдельно где нужно, если проблемы с наследованием стилей
                    'style' => 'cursor: pointer; color: #0088cc; visibility: hidden',
                ],
                'options' => ['style' => 'margin-top: 100px']
            ]);

            myForms::modalLogin();

            Modal::end();
        }
    }
}