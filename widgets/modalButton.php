<?php

namespace app\widgets;

use yii\helpers\Html;

class modalButton
{
    // Заказ услуг
    public static function services ($model, $name)
    {
        echo Html::tag('span', $name, [
            'data-toggle' => 'modal',
            'data-target' => '#services-'.$model->id,
            'class' => 'badge badge-success',
            'style' => 'cursor: pointer',
            'title' => 'Сделать быстрый заказ'
        ]);
    }

    // Авторизация
    public static function login ()
    {
        return Html::tag('span', 'Naigel', [
            'data-toggle' => 'modal',
            'data-target' => '#modal-login',
            //'style' => 'cursor: pointer;'
        ]);
    }
}