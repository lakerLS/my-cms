<?php

namespace app\widgets;

use app\models\Faq;
use Yii;
use yii\helpers\ArrayHelper;

class myMenu
{

    /**
     * Боковое меню, одна вложенность.
     * Передаем 2 обязательных параметра: label - заголовок, url - адрес.
     *
     * Необязательные параметры:
     *
     * active - необходим для вычисления активного пунка меню. Два обязательных параметра: контроллер и экшен.
     * Пример: 'active' =>'site/view'.
     * Поддерживается возможность GET адресов. Для этого необходимо изменить в виджете параметр после "?".
     *
     * count - выводит количество записей после заголовка. Два обязательных параметра: имя таблицы, откуда вести счет и условие.
     * Пример: 'count' => [Order', 'closed' => '1'],
     *
     * notice или noticeTwo - выводят до двух иконок с оповещениями одновременно, при определенных условиях.
     * Три обязательных параметра: имя таблицы, запрос, равно (условие вывода).
     * Необязательные параметры: title
     * Пример: 'notice' => ['OrderInfo', ['status' => '1'], '1', 'title']
     */
    public static function sidebar ($items)
    {
        // Задаем необходимый класс для <ul>
        echo '<ul>';

        foreach ($items as $value) {

            // Если active не задан, присваиваем значение url убирая первый слеш.
            if (empty ($value['active']) && empty(Yii::$app->request->get('order')))
                $active = strstr(substr($value['url'], '1', -1), '?', true);

            // Если GET указан.
            elseif (!empty (Yii::$app->request->get()))
                $active = substr($value['url'], '1');

            // Если active указан
            else
                $active = $value['active'];

            /**
             * Если пункт активен, значит добавляем "active" в нужное место.
             * Сдесь active равен имени контроллера и экшена или GET запроса.
             * Если пункты меню привязаны к GET запросу, необходимо изменить значение после "?".
             */
             if (empty (Yii::$app->request->get('order')) && $active == Yii::$app->request->pathInfo ||
                 empty (Yii::$app->request->get('order')) && $active == Yii::$app->controller->route ||
                 $active == Yii::$app->controller->id.'?order='.Yii::$app->request->get('order'))
             {?>

                    <!-- Указываем класс для <li> и для <a>. Метка "active" может содержаться в разных тегах, в зависимости от шаблона.
                         Не забываем дублировать классы для <li> и <a>. -->
                    <li class="active">
                        <a href="<?= $value['url'] ?>">

             <? } else { ?>

                    <li>
                        <a href="<?= $value['url'] ?>">

             <? } ?>

                        <?= $value['label'] ?>

                        <!-- Счетчик начало -->
                        <?
                        // Указываем расположение модели.
                        if (isset ($value['count'])){
                            $table = '\\app\\models\\'.$value['count']['0'];

                            $count = $table::find()->where($value['count']['1'])->count();
                            // Если счет не равен нулю, то отобразить
                            if(!empty($count))
                                echo '('.$count.')';
                        }?>
                        <!-- Счетчик конец-->

                        <!-- Иконки уведомления начало -->
                        <? if (isset ($value['notice'])) {

                            $table = '\\app\\models\\'.$value['notice']['0'];

                            if ($table::find()->where([key($value['notice']['1']) => implode($value['notice']['1'])])->one() == $value['notice']['2']) {

                                if (empty($value['notice']['3'])){ ?>
                                    <i class="et-chat" style="color: #87ff87"></i>

                                <? } else { ?>
                                <i class="et-chat" style="color: #87ff87" title="<?= $value['notice']['3'] ?>"></i>

                        <? }}} ?>

                        <? if (isset ($value['noticeTwo'])) {

                            $table = '\\app\\models\\'.$value['noticeTwo']['0'];

                            if ($table::find()->where($value['noticeTwo']['1'])->count() == $value['noticeTwo']['2']) {

                                if (empty($value['noticeTwo']['3'])){ ?>

                                    <font color="#dc3545"><i class="icon-warning-empty"></i></font>

                                <? } else { ?>
                                    <font color="#dc3545"><i class="icon-warning-empty" title="<?= $value['noticeTwo']['3'] ?>"></i></font>

                        <? }}} ?>
                        <!-- Иконки уведомления конец -->

                    </a>
                </li>
        <? }

        echo '</ul>';
    }
}