<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\modules\user\controllers;

use dektrium\user\events\AuthEvent;
use dektrium\user\Module;
use Yii;
use yii\base\Event;

use dektrium\user\controllers\SecurityController as BasicSecurityController;

/**
 * Controller that manages user authentication process.
 *
 * @property Module $module
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class SecurityController extends BasicSecurityController
{

    public function init()
    {
        parent::init();

        Event::on(SecurityController::class, SecurityController::EVENT_AFTER_LOGOUT, function ( $e)
        {
            return $this->redirect('/articles');

        });

        Event::on(SecurityController::class, SecurityController::EVENT_AFTER_AUTHENTICATE, function (AuthEvent $e) {
            // if user account was not created we should not continue


            if ($e->account->id === null) {
                return;
            }

            // we are using switch here, because all networks provide different sets of data
            switch ($e->client->getName()) {
                case 'google':
                    //echo '<pre>'; print_r($e->client->getUserAttributes()['emails']['0']['value']); echo '</pre>'; die;

                    $e->account->updateAttributes([
                        'email' => $e->client->getUserAttributes()['emails']['0']['value'],
                        'username' => strstr($e->client->getUserAttributes()['emails']['0']['value'], '@', true),
                    ]);
            }

            // after saving all user attributes will be stored under account model
            // Yii::$app->identity->user->accounts['facebook']->decodedData
        });

//        $this->on(parent::EVENT_AFTER_ACTION, function ($event) {
//            $this->redirect('/'.\Yii::$app->getRequest()->post('redirect'));
//        });
    }
}
