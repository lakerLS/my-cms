<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\Nav;

/**
 * @var \yii\web\View $this
 * @var \dektrium\user\models\User $user
 * @var string $content
 */

$this->title = 'Редактировать';
?>


    <!-- page title -->
    <header id="page-header" style="margin-bottom: 30px">
        <h1>Добавить запись</h1>
        <ol class="breadcrumb">
            <li><a href="#">Управление доступом</a></li>
            <li><a href="/user/admin/index">Пользователи</a></li>
            <li class="active">Редактировать</li>
        </ol>
    </header>
    <!-- /page title -->

<div class="panel panel-default">
    <div class="panel-body form-input ">

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>


<div class="breadcrumb-container">

<?= Nav::widget([
    'options' => [
        '',
    ],
    'items' => [
        [
            'label' => Yii::t('user', 'Account details'),
            'url' => ['/user/admin/update', 'id' => $user->id]
        ],
        [
            'label' => Yii::t('user', 'Profile details'),
            'url' => ['/user/admin/update-profile', 'id' => $user->id]
        ],
        ['label' => Yii::t('user', 'Information'), 'url' => ['/user/admin/info', 'id' => $user->id]],
        [
            'label' => Yii::t('user', 'Assignments'),
            'url' => ['/user/admin/assignments', 'id' => $user->id],
            'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']),
        ],
        '<hr>',
        [
            'label' => Yii::t('user', 'Confirm'),
            'url' => ['/user/admin/confirm', 'id' => $user->id],
            'visible' => !$user->isConfirmed,
            'linkOptions' => [
                'class' => 'text-success',
                'data-method' => 'post',
                'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
            ],
        ],
        [
            'label' => Yii::t('user', 'Block'),
            'url' => ['/user/admin/block', 'id' => $user->id],
            'visible' => !$user->isBlocked,
            'linkOptions' => [
                'class' => 'text-danger',
                'data-method' => 'post',
                'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
            ],
        ],
        [
            'label' => Yii::t('user', 'Unblock'),
            'url' => ['/user/admin/block', 'id' => $user->id],
            'visible' => $user->isBlocked,
            'linkOptions' => [
                'class' => 'text-success',
                'data-method' => 'post',
                'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
            ],
        ],
        [
            'label' => Yii::t('user', 'Delete'),
            'url' => ['/user/admin/delete', 'id' => $user->id],
            'linkOptions' => [
                'class' => 'text-danger',
                'data-method' => 'post',
                'data-confirm' => Yii::t('user', 'Are you sure you want to delete this user?'),
            ],
        ],
    ],
]) ?>
</div>

<?= $content ?>

    </div>
</div>