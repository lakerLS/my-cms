<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\User $model
 * @var dektrium\user\Module $module
 */

$this->title = Yii::t('user', 'Sign up');
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- main-container start -->
<!-- ================ -->
<div class="main-container dark-translucent-bg" style="background-image:url('images/background-img-6.jpg');">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-auto">
                <!-- main start -->
                <!-- ================ -->
                <div class="main object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="100">
                    <div class="form-block p-30 light-gray-bg border-clear">
                        <h2 class="title">Регистрация</h2>


                        <?php $form = ActiveForm::begin([
                            'id' => 'registration-form',

                            'options' => [
                                'class' => 'form-horizontal',
                            ],


                            'enableAjaxValidation' => true,
                            'enableClientValidation' => false,
                        ]); ?>

                        <form class="form-horizontal">

                            <div class="form-group has-feedback row">
                                <label for="inputUsername" class="col-md-3 control-label text-md-right col-form-label">Логин <span class="text-danger small">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" id="register-form-username" class="form-control" name="register-form[username]" aria-required="true" placeholder="Логин" required>
                                    <i class="fa fa-user form-control-feedback pr-4"></i>
                                </div>
                            </div>

                            <div class="form-group has-feedback row">
                                <label for="inputName" class="col-md-3 control-label text-md-right col-form-label">Email <span class="text-danger small">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" id="register-form-email" class="form-control" name="register-form[email]" aria-required="true" placeholder="Email" required>
                                    <i class="fa fa-pencil form-control-feedback pr-4"></i>
                                </div>
                            </div>

                            <div class="form-group has-feedback row">
                                <label for="inputPassword" class="col-md-3 control-label text-md-right col-form-label">Пароль <span class="text-danger small">*</span></label>
                                <div class="col-md-8">
                                    <input type="password" id="register-form-password" class="form-control" name="register-form[password]" aria-required="true" aria-invalid="true" placeholder="Пароль" required>
                                    <i class="fa fa-lock form-control-feedback pr-4"></i>
                                </div>
                            </div>

<!--                            <div class="form-group row">-->
<!--                                <div class="ml-md-auto col-md-9">-->
<!--                                    <div class="checkbox">-->
<!--                                        <label>-->
<!--                                            <input type="checkbox" required> Accept our <a href="#">privacy policy</a> and <a href="#">customer agreement</a>-->
<!--                                        </label>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->

                            <div class="form-group row">
                                <div class="ml-md-auto col-md-9">
                                    <button type="submit" class="btn btn-group btn-default btn-animated">Зарегистрироваться <i class="fa fa-check"></i></button>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </form>
                    </div>
                </div>
                <!-- main end -->
            </div>
        </div>
    </div>
</div>
<!-- main-container end -->




