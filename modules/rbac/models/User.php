<?php

namespace app\modules\rbac\models;

use developeruz\db_rbac\interfaces\UserRbacInterface;

class User extends \dektrium\user\models\User implements UserRbacInterface
{
    public function getUserName()
    {
        return $this->username;
    }
}