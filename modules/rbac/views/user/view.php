<?php
namespace developeruz\db_rbac\views\user;

use Yii;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Управление ролями пользователя';
?>

<!-- page title -->
<header id="page-header" style="margin-bottom: 30px">
    <h1><?= $this->title ?></h1>
    <ol class="breadcrumb">
        <li><a href="#">Управление доступом</a></li>
        <li><a href="/user/admin/index">Пользователи</a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</header>
<!-- /page title -->

<div class="panel panel-default">
    <div class="panel-body form-input ">

    <?php $form = ActiveForm::begin(['action' => ["update", 'id' => $user->getId()]]); ?>

    <?= Html::checkboxList('roles', $user_permit, $roles, ['separator' => '<br>']); ?>

    <div class="form-group" style="margin-top: 20px">
        <?= Html::submitButton(Yii::t('db_rbac', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    </div>
</div>


