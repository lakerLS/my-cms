<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Site extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => \yii\web\View::POS_END];
    public $css = [
        'template/bootstrap/css/bootstrap.css',
        'template/fonts/font-awesome/css/font-awesome.css',
        'template/fonts/fontello/css/fontello.css',
        'template/plugins/magnific-popup/magnific-popup.css',
        'template/plugins/rs-plugin-5/css/settings.css',
        'template/plugins/rs-plugin-5/css/layers.css',
        'template/plugins/rs-plugin-5/css/navigation.css',
        'template/css/animations.css',
        'template/plugins/owlcarousel2/assets/owl.carousel.min.css',
        'template/plugins/owlcarousel2/assets/owl.theme.default.min.css',
        'template/plugins/hover/hover-min.css',
        'template/css/style.css',
        'template/css/typography-default.css',
        'template/css/skins/light_blue.css',
        'template/css/custom.css',
        'template/bootstrap/css/icons_bootstrap.css',

        'myCssJs/css/myCss.css',
        'myCssJs/css/my.cms.css',

        //'css/site.css',
        //'custom_theme/css/dashicons.min.css',
    ];

    public $js = [
        //'template/plugins/jquery.min.js',
        'template/plugins/popper.min.js',
        'template/bootstrap/js/bootstrap.min.js',
        'template/plugins/modernizr.js',
        'template/plugins/rs-plugin-5/js/jquery.themepunch.tools.min.js?rev=5.0',
        'template/plugins/rs-plugin-5/js/jquery.themepunch.revolution.min.js?rev=5.0',
        'template/plugins/isotope/imagesloaded.pkgd.min.js',
        'template/plugins/isotope/isotope.pkgd.min.js',
        'template/plugins/magnific-popup/jquery.magnific-popup.min.js',
        'template/plugins/waypoints/jquery.waypoints.min.js',
        'template/plugins/waypoints/sticky.min.js',
        'template/plugins/jquery.countTo.js',
        'template/plugins/jquery.parallax-1.1.3.js',
        'template/plugins/jquery.validate.js',
        'template/plugins/morphext/morphext.min.js',
        'template/plugins/owlcarousel2/owl.carousel.min.js',
        'template/js/template.js',
        'template/js/custom.js',

        'myCssJs/js/myJs.js'
    ];

    public $depends = [
//       'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset'
    ];

}
