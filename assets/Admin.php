<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Admin extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => \yii\web\View::POS_END];
    public $css = [
        'admin/assets/plugins/bootstrap/css/bootstrap.min.css',
        'admin/assets/css/essentials.css',
        'admin/assets/css/layout.css',
        'admin/assets/css/color_scheme/green.css',

        'myCssJs/css/myCss.css'
    ];

    public $js = [
        //'admin/assets/plugins/',
        //'admin/assets/plugins/jquery/jquery-2.2.3.min.js',
        'admin/assets/js/app.js',

        'myCssJs/js/myJs.js'
    ];

    public $depends = [

        //'yii\web\JqueryAsset',
    ];

}
