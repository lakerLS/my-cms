<?php

$this->title = 'Заказ услуги'
?>


<!-- CART -->
<section>
    <div class="container">
        <div align="center">

            Укажите ваши данные и мы свяжемся с Вами.
            <hr />

            <?= $this->render('context/_form',[
                'order' => $order,
                'model' => $model,
            ]) ?>

        </div>
    </div>
</section>
<!-- /CART -->