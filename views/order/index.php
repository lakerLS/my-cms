<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

    $this->title = 'Обработка заказов';
?>

<!-- page title -->
<header id="page-header" style="margin-bottom: 30px">
    <h1><?= $this->title ?></h1>
    <ol class="breadcrumb">
        <li><a href="#">Наполнение сайта</a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</header>
<!-- /page title -->

<div class="panel panel-default">
    <div class="panel-heading">
        <span class="title elipsis">
            <strong style="font-size: 20px">Актуальные заказы</strong>
        </span>
    </div>

    <div class="panel-body">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'layout' => '{items}{pager}',
        'columns' => [

            ['attribute' => 'price_name',
                'headerOptions' => ['style' => 'text-align: center', 'width' => '160'],
                'contentOptions' => ['style' => 'text-align: center'],
            ],
            ['attribute' => 'name',
                'headerOptions' => ['style' => 'text-align: center', 'width' => '260'],
                'contentOptions' => ['style' => 'text-align: center'],
            ],
            ['attribute' => 'email',
                'headerOptions' => ['style' => 'text-align: center', 'width' => '160'],
                'contentOptions' => ['style' => 'text-align: center'],
            ],
            ['attribute' => 'phone',
                'headerOptions' => ['style' => 'text-align: center', 'width' => '160'],
                'contentOptions' => ['style' => 'text-align: center'],
            ],
            ['attribute' => 'adress',
                'headerOptions' => ['style' => 'text-align: center'],
                'contentOptions' => ['style' => 'text-align: center', 'width' => '400'],
            ],

            ['attribute' => 'date',
                'headerOptions' => ['style' => 'text-align:center;', 'width' => '140'],
                'value' => function($model){
                    return Yii::$app->formatter->asDate($model->date);
                }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{complete} {delete}',
                'contentOptions' => ['width' => '60'],

                'buttons' => [
                    'complete' => function($url, $model, $key) {     // render your custom button

                        $form = ActiveForm::begin(['action' => '/order/update?id='.$model->id,]);

                        echo $form->field($model, 'closed')->hiddenInput(['value' => '1'])->label(false);

                        ActiveForm::end();

                        return Html::submitButton("<i class='fa fa-check-square-o' style='color: #337ab7' title='Закрыть заявку'></i>", ['form' => $form->id]);
                    },
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <span class="title elipsis">
            <strong style="font-size: 20px">История заказов</strong>
        </span>
    </div>

    <div class="panel-body">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataHistory,
        //'filterModel' => $searchModel,
        'layout' => '{items}{pager}',
        'columns' => [

            ['attribute' => 'price_name',
                'headerOptions' => ['style' => 'text-align: center', 'width' => '160'],
                'contentOptions' => ['style' => 'text-align: center'],
            ],
            ['attribute' => 'name',
                'headerOptions' => ['style' => 'text-align: center', 'width' => '260'],
                'contentOptions' => ['style' => 'text-align: center'],
            ],
            ['attribute' => 'email',
                'headerOptions' => ['style' => 'text-align: center', 'width' => '160'],
                'contentOptions' => ['style' => 'text-align: center'],
            ],
            ['attribute' => 'phone',
                'headerOptions' => ['style' => 'text-align: center', 'width' => '160'],
                'contentOptions' => ['style' => 'text-align: center'],
            ],
            ['attribute' => 'adress',
                'headerOptions' => ['style' => 'text-align: center'],
                'contentOptions' => ['style' => 'text-align: center', 'width' => '400'],
            ],

            ['attribute' => 'date',
                'headerOptions' => ['style' => 'text-align:center;', 'width' => '140'],
                'value' => function($model){
                    return Yii::$app->formatter->asDate($model->date);
                }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'contentOptions' => ['width' => '60']
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>
    </div>
</div>
