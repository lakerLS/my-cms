<?php

use app\classes\FormHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use himiklab\yii2\recaptcha\ReCaptcha;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

    <!-- Необходимо указывать конкретный путь к экшену, т.к. может исполняться из разных мест -->
    <? $form = ActiveForm::begin(['action' => '/order/create?redirect='.Yii::$app->request->pathInfo]); ?>

    <?= $form->field($order, 'name') ?>

    <!-- Для обязательных полей добавляем красную * с помощью 'template' -->
    <?= $form->field($order, 'phone', FormHelper::required())->textInput()?>

    <?= $form->field($order, 'email', FormHelper::required())->textInput()?>

    <?= $form->field($order, 'adress') ?>

    <?= $form->field($order, 'price_name')->hiddenInput(['value' => $model->name])->label(false) ?>

    <?= $form->field($order, 'price_id')->hiddenInput(['value' => $model->id])->label(false) ?>

    <?//=  $form->field($faq, 'reCaptcha')->widget(ReCaptcha::className(), [
    //  'name' => 'NewUserRegisterForm[reCaptcha]',
    //])->label(false) ?>

    <div class="form-group" align="center">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-success', 'name' => 'contact-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>
