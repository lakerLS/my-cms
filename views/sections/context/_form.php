<?php

use app\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sections */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel panel-default">
    <div class="panel-body form-input ">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?
    if (!empty(Yii::$app->request->get('category_id')))
        echo $form->field($model, 'type')->hiddenInput(['value' => Yii::$app->request->get('category_id')])->label(false);

    elseif (!empty(Yii::$app->request->get('type')))
        echo $form->field($model, 'type')->hiddenInput(['value' => Yii::$app->request->get('type')])->label(false);

    else
        echo $form->field($model, 'type')->dropDownList(ArrayHelper::map(Category::find()->where(['<>', 'type', 'static'])->all(), 'id', 'name')) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    </div>
</div>
