<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sections */

$this->title = 'Изменить раздел: '.$model->name;
?>

<!-- page title -->
<header id="page-header" style="margin-bottom: 30px">
    <h1><?= $this->title ?></h1>
    <ol class="breadcrumb">
        <li><a href="#">Наполнение сайта</a></li>
        <li><a href="/sections">Категории</a></li>
        <li class="active"><?= $model->name ?></li>
    </ol>
</header>
<!-- /page title -->

    <?= $this->render('context/_form', [
        'model' => $model,
    ]) ?>

