<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SectionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Разделы';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- page title -->
<header id="page-header" style="margin-bottom: 30px">
    <h1><?= $this->title ?></h1>
    <ol class="breadcrumb">
        <li><a href="#">Наполнение сайта</a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</header>
<!-- /page title -->

<div class="panel panel-default">
    <div class="panel-body">

        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout'=>"\n{items}",

            'columns' => [

                'name',
                // Второй вариант вывода из связанной таблицы
                ['attribute' => 'type',
                    'value' => 'category.name',
                    'headerOptions' => ['style' => 'text-align: center', 'width' => '400'],
                    'contentOptions' => ['style' => 'text-align: center'],
                ],

                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'headerOptions' => ['style' => 'text-align: center', 'width' => '50'],
                ],
            ],
        ]); ?>

        <p>
            <?= Html::a('добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?php Pjax::end(); ?>

    </div>
</div>
