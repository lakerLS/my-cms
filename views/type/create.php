<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Type */

$this->title = 'Создать тип содержимого';
?>

<!-- page title -->
<header id="page-header" style="margin-bottom: 30px">
    <h1>Создание типа содержимого</h1>
    <ol class="breadcrumb">
        <li><a href="#">Разработчику</a></li>
        <li><a href="/type">Типы содержимого</a></li>
        <li class="active">Создание</li>
    </ol>
</header>
<!-- /page title -->

    <?= $this->render('context\_form', [
        'model' => $model,
    ]) ?>
