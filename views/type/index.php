<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Типы содержимого';
?>

<!-- page title -->
<header id="page-header" style="margin-bottom: 30px">
    <h1><?= $this->title ?></h1>
    <ol class="breadcrumb">
        <li><a href="#">Разработчику</a></li>
        <li class="active">Типы содержимого</li>
    </ol>
</header>
<!-- /page title -->

<div class="panel panel-default">
    <div class="panel-body">

        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout'=>"{items}",

            'columns' => [
                'name:ntext',
                'type',
                ['attribute' => 'many',
                    'label' => 'список',
                    'contentOptions' => ['width' => '1', 'style' => 'text-align:center'],
                ],

                ['attribute' => 'one',
                    'label' => 'запись',
                    'contentOptions' => ['width' => '1', 'style' => 'text-align:center'],
                ],

                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}'
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>

        <div>
            <?= Html::a('Добавить тип содержимого', ['type/create'], ['class' => 'btn btn-success']) ?>
        </div>

    </div>
</div>
