<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Type */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel panel-default">
    <div class="panel-body form-input ">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput() ?>

    <? foreach (explode(',', $model->select) as $value)
        $array[$value] = $value;

    $model->select = $array;

    echo $form->field($model, 'select')->widget(Select2::classname(), [
        'data' => [
            'section' => 'Раздел',
            'name' => 'Заголовок',
            'url' => 'Адрес',
            'image' => 'Изображение',
            'text' => 'Текст',
            'price' => 'Цена',
            'price_old' => 'Старая цена',
            'source' => 'Источник',
            'title' => 'Title',
            'description' => 'Дискрипция',
            'keywords' => 'Теги',
        ],
        'options' => ['multiple' => true, 'placeholder' => 'Выбрать ...'],
        'pluginOptions' => [
            'allowClear' => true,

        ],
    ]); ?>

    <?= $form->field($model, 'many')->checkbox() ?>
    <?= $form->field($model, 'one')->checkbox() ?>

    <?= $form->field($model, 'tpl_v1')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tpl_v2')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    </div>
</div>
