<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Type */

$this->title = $model->name ;
?>

<!-- page title -->
<header id="page-header" style="margin-bottom: 30px">
    <h1>Редактировать: <?= $model->name ?></h1>
    <ol class="breadcrumb">
        <li><a href="#">Разработчику</a></li>
        <li><a href="/type">Типы содержимого</a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</header>
<!-- /page title -->

    <?= $this->render('context\_form', [
        'model' => $model,
    ]) ?>

