<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyDetails */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="panel panel-default">
    <div class="panel-body form-input ">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'indification')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_one')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_two')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_three')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email_one')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email_two')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'coordinate')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton( 'Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    </div>
</div>