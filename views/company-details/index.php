<?php

use app\models\CompanyDetails;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CompanyDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Информация о компании';
?>

<!-- page title -->
<header id="page-header" style="margin-bottom: 30px">
    <h1><?= $this->title ?></h1>
    <ol class="breadcrumb">
        <li><a href="#">Наполнение сайта</a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</header>
<!-- /page title -->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout'=>"{items}",
        'columns' => [

            'indification',
            'fio',
            'phone_one',
            'phone_two',
            'phone_three',
            'email_one:email',
            'email_two:email',
            'coordinate',
            'address',
        ],
    ]); ?>

    <p>
        <?
        $model = CompanyDetails::find()->one();
        echo Html::a( empty ($model) ? 'Добавить' : 'Редактировать', empty ($model) ? ['create'] : ['update?id='.$model->id], ['class' => 'btn btn-success'])
        ?>
    </p>


