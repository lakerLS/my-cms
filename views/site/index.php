<?php


?>

<!-- banner start -->
<!-- ================ -->
<div class="banner clearfix">

    <!-- slideshow start -->
    <!-- ================ -->
    <div class="slideshow">

        <!-- slider revolution start -->
        <!-- ================ -->
        <div class="slider-revolution-5-container">
            <div id="slider-banner-fullscreen" class="slider-banner-fullscreen rev_slider" data-version="5.0">
                <ul class="slides">
                    <!-- slide 1 start -->
                    <!-- ================ -->
                    <li class="text-center" data-transition="random" data-slotamount="default" data-masterspeed="default" data-title="Unlimited Options and Layouts">

                        <!-- main image -->
                        <img src="images/portfolio-slider-slide-1.jpg" alt="slidebg1" data-bgposition="center center" data-bgrepeat="no-repeat" data-bgfit="cover" class="rev-slidebg">

                        <!-- Transparent Background -->
                        <div class="tp-caption dark-translucent-bg"
                             data-x="center"
                             data-y="center"
                             data-start="0"
                             data-transform_idle="o:1;"
                             data-transform_in="o:0;s:600;e:Power2.easeInOut;"
                             data-transform_out="o:0;s:600;"
                             data-width="5000"
                             data-height="5000">
                        </div>

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption xlarge_white"
                             data-x="center"
                             data-y="110"
                             data-start="1000"
                             data-end="2500"
                             data-splitin="chars"
                             data-splitout="chars"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-transform_idle="o:1;"
                             data-transform_in="x:[50%];o:0;s:600;e:Power4.easeInOut;"
                             data-transform_out="x:[-50%];o:0;s:200;e:Power2.easeInOut;"
                             data-mask_in="x:0;y:0;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">Inspiration
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption xlarge_white"
                             data-x="center"
                             data-y="110"
                             data-start="3100"
                             data-end="4600"
                             data-splitin="chars"
                             data-splitout="chars"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-transform_idle="o:1;"
                             data-transform_in="x:[-50%];o:0;s:600;e:Power4.easeInOut;"
                             data-transform_out="x:[-50%];o:0;s:200;e:Power2.easeInOut;"
                             data-mask_in="x:0;y:0;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">Innovation
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption xlarge_white"
                             data-x="center"
                             data-y="110"
                             data-start="5200"
                             data-end="6700"
                             data-splitin="chars"
                             data-splitout="chars"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-transform_idle="o:1;"
                             data-transform_in="x:[50%];o:0;s:600;e:Power4.easeInOut;"
                             data-transform_out="x:0;y:[-50%];o:0;s:200;e:Power2.easeInOut;"
                             data-mask_in="x:0;y:0;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">Success
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption large_white"
                             data-x="center"
                             data-y="110"
                             data-start="7000"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[-100%];sX:1;sY:1;o:0;s:1100;e:Power4.easeInOut;"
                             data-transform_out="y:0;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[-100%];s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"><span class="logo-font">The <span class="text-default">Project</span></span> <br> Powerful Bootstrap Template
                        </div>


                        <!-- LAYER NR. 5 -->
                        <div class="tp-caption"
                             data-x="center"
                             data-y="250"
                             data-hoffset="-232"
                             data-start="1000"
                             data-end="6200"
                             data-transform_idle="o:1;"
                             data-transform_in="x:[50%];y:0;o:0;s:700;e:Power4.easeInOut;"
                             data-transform_out="x:0;y:0;o:0;s:700;e:Power2.easeInOut;"><span class="icon large circle"><i class="circle icon-lightbulb"></i></span>
                        </div>

                        <!-- LAYER NR. 6 -->
                        <div class="tp-caption"
                             data-x="center"
                             data-y="250"
                             data-start="3100"
                             data-end="6200"
                             data-transform_idle="o:1;"
                             data-transform_in="x:[-50%];y:0;o:0;s:700;e:Power4.easeInOut;"
                             data-transform_out="x:0;y:0;o:0;s:700;e:Power2.easeInOut;"><span class="icon large circle"><i class="circle icon-arrows-ccw"></i></span>
                        </div>

                        <!-- LAYER NR. 7 -->
                        <div class="tp-caption"
                             data-x="center"
                             data-y="250"
                             data-hoffset="232"
                             data-start="5200"
                             data-end="6200"
                             data-transform_idle="o:1;"
                             data-transform_in="x:[50%];y:0;o:0;s:700;e:Power4.easeInOut;"
                             data-transform_out="x:0;y:0;o:0;s:700;e:Power2.easeInOut;"><span class="icon large circle"><i class="circle icon-chart-line"></i></span>
                        </div>

                        <!-- LAYER NR. 8 -->
                        <div class="tp-caption tp-resizeme large_white"
                             data-x="center"
                             data-y="210"
                             data-start="6400"
                             data-end="10000"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[-100%];sX:1;sY:1;o:0;s:1100;e:Power4.easeInOut;"
                             data-transform_out="y:0;s:1000;e:Power2.easeInOut;"><div class="separator light"></div>
                        </div>

                        <!-- LAYER NR. 9 -->
                        <div class="tp-caption medium_white"
                             data-x="center"
                             data-y="250"
                             data-start="6800"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[-50%];sX:1;sY:1;o:0;s:1100;e:Power4.easeInOut;"
                             data-transform_out="y:0;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[-50%];s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br> Nesciunt, maiores, aliquid. Repellat eum numquam aliquid culpa offici, <br> tenetur fugiat dolorum sapiente eligendi...
                        </div>

                        <!-- LAYER NR. 10 -->
                        <div class="tp-caption"
                             data-x="center"
                             data-y="bottom"
                             data-voffset="100"
                             data-start="1250"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[100%];sX:1;sY:1;o:0;s:2000;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">
                            <a href="#page-start" class="btn btn-lg moving smooth-scroll"><i class="icon-down-open-big"></i><i class="icon-down-open-big"></i><i class="icon-down-open-big"></i></a>
                        </div>

                    </li>
                    <!-- slide 1 end -->

                    <!-- slide 2 start -->
                    <!-- ================ -->
                    <li data-transition="random" data-slotamount="7" data-masterspeed="default" data-title="Premium HTML5 Bootstrap Theme">

                        <!-- main image -->
                        <img src="images/portfolio-slider-slide-2.jpg" alt="slidebg2" data-bgposition="center center" data-bgrepeat="no-repeat" data-bgfit="cover" class="rev-slidebg">

                        <!-- Transparent Background -->
                        <div class="tp-caption dark-translucent-bg"
                             data-x="center"
                             data-y="center"
                             data-start="0"
                             data-transform_idle="o:1;"
                             data-transform_in="o:0;s:600;e:Power2.easeInOut;"
                             data-transform_out="o:0;s:600;"
                             data-width="5000"
                             data-height="5000">
                        </div>

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption large_white"
                             data-x="left"
                             data-y="110"
                             data-start="1000"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[100%];sX:1;sY:1;o:0;s:1100;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"><span class="logo-font">The Project</span> <br> Premium HTML5 Bootstrap Theme
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption medium_white"
                             data-x="left"
                             data-y="240"
                             data-speed="500"
                             data-start="1200"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[100%];sX:1;sY:1;o:0;s:900;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"><span class="icon default-bg circle small hidden-xs"><i class="fa fa-laptop"></i></span> 100% Responsive
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption medium_white"
                             data-x="left"
                             data-y="300"
                             data-speed="500"
                             data-start="1400"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[100%];sX:1;sY:1;o:0;s:900;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"><span class="icon default-bg circle small hidden-xs"><i class="icon-check"></i></span> Bootstrap Based
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption medium_white"
                             data-x="left"
                             data-y="360"
                             data-speed="500"
                             data-start="1600"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[100%];sX:1;sY:1;o:0;s:900;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"><span class="icon default-bg circle small hidden-xs"><i class="icon-gift"></i></span> Packed Full of Features
                        </div>

                        <!-- LAYER NR. 5 -->
                        <div class="tp-caption medium_white"
                             data-x="left"
                             data-y="420"
                             data-speed="500"
                             data-start="1800"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[100%];sX:1;sY:1;o:0;s:900;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"><span class="icon default-bg circle small hidden-xs"><i class="icon-hourglass"></i></span> Very Easy to Customize
                        </div>

                        <!-- LAYER NR. 6 -->
                        <div class="tp-caption small_white"
                             data-x="left"
                             data-y="490"
                             data-speed="500"
                             data-start="2000"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[100%];sX:1;sY:1;o:0;s:900;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"><a href="#" class="btn btn-default-transparent btn-lg btn-animated">Purchase <i class="fa fa-cart-arrow-down"></i></a>
                        </div>
                    </li>
                    <!-- slide 2 end -->
                </ul>
                <div class="tp-bannertimer"></div>
            </div>
        </div>
        <!-- slider revolution end -->

    </div>
    <!-- slideshow end -->

</div>
<!-- banner end -->

<div id="page-start"></div>

<!-- section start -->
<!-- ================ -->
<section class="section default-bg clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="call-to-action text-center">
                    <div class="row">
                        <div class="col-md-8">
                            <h1 class="title">WE OFFER MORE POSSIBILITIES TO MEET YOUR EVERY NEED</h1></div>
                        <div class="col-md-4 text-md-right">
                            <br>
                            <p><a href="#" class="btn btn-lg btn-gray-transparent btn-animated">Learn More<i class="fa fa-arrow-right pl-20"></i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section end -->

<!-- section start -->
<!-- ================ -->
<section class="light-gray-bg pv-30 clearfix">
    <div class="container">
        <div class="row justify-content-lg-center">
            <div class="col-lg-8">
                <h2 class="text-center mt-4">We <i class="fa fa-heart text-default"></i> What We Do</h2>
                <div class="separator"></div>
                <p class="large text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam voluptas facere vero ex tempora saepe perspiciatis ducimus sequi animi.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="pv-30 feature-box margin-clear text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                    <span class="icon default-bg circle"><i class="fa fa-diamond"></i></span>
                    <h3>We Love Details</h3>
                    <div class="separator clearfix"></div>
                    <p>Voluptatem ad provident non repudiandae beatae cupiditate amet reiciendis lorem ipsum dolor sit amet, consectetur.</p>
                    <a href="page-services.html">Read More <i class="pl-1 fa fa-angle-double-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="pv-30 feature-box margin-clear text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="150">
                    <span class="icon default-bg circle"><i class="fa fa-connectdevelop"></i></span>
                    <h3>We Are Extremely Flexible</h3>
                    <div class="separator clearfix"></div>
                    <p>Iure sequi unde hic. Sapiente quaerat sequi inventore veritatis cumque lorem ipsum dolor sit amet, consectetur.</p>
                    <a href="page-services.html">Read More <i class="pl-1 fa fa-angle-double-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="pv-30 feature-box margin-clear text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="200">
                    <span class="icon default-bg circle"><i class="fa icon-snow"></i></span>
                    <h3>We Use Latest Technologies</h3>
                    <div class="separator clearfix"></div>
                    <p>Inventore dolores aut laboriosam cum consequuntur delectus sequi lorem ipsum dolor sit amet, consectetur.</p>
                    <a href="page-services.html">Read More <i class="pl-1 fa fa-angle-double-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section end -->

<!-- section start -->
<!-- ================ -->
<section class="section pv-30">
    <div class="container">
        <div class="row justify-content-lg-center">
            <div class="col-lg-8 text-center">
                <h2 class="title"><span class="text-muted">Our</span> <span class="text-default">Work</span></h2>
                <div class="separator"></div>
            </div>
        </div>
        <!-- isotope filters start -->
        <div class="filters d-flex justify-content-center">
            <ul class="nav nav-pills style-2">
                <li class="nav-item"><a class="nav-link active" href="#" data-filter="*">All</a></li>
                <li class="nav-item"><a class="nav-link" href="#" data-filter=".web-design">Web design</a></li>
                <li class="nav-item"><a class="nav-link" href="#" data-filter=".app-development">App development</a></li>
                <li class="nav-item"><a class="nav-link" href="#" data-filter=".site-building">Site building</a></li>
            </ul>
        </div>
        <!-- isotope filters end -->
        <div class="isotope-container row grid-space-0">
            <div class="col-md-6 col-lg-3 isotope-item web-design">
                <div id="carousel-portfolio" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators bottom margin-clear">
                        <li data-target="#carousel-portfolio" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-portfolio" data-slide-to="1"></li>
                        <li data-target="#carousel-portfolio" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div class="image-box text-center">
                                <div class="overlay-container">
                                    <img src="images/portfolio-1.jpg" alt="">
                                    <div class="overlay-top">
                                        <div class="text">
                                            <h3><a href="portfolio-item.html">Project Title</a></h3>
                                            <p class="small">Web Design</p>
                                        </div>
                                    </div>
                                    <div class="overlay-bottom">
                                        <div class="links">
                                            <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="image-box text-center">
                                <div class="overlay-container">
                                    <img src="images/portfolio-1-2.jpg" alt="">
                                    <div class="overlay-top">
                                        <div class="text">
                                            <h3><a href="portfolio-item.html">Project Title</a></h3>
                                            <p class="small">Web Design</p>
                                        </div>
                                    </div>
                                    <div class="overlay-bottom">
                                        <div class="links">
                                            <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="image-box text-center">
                                <div class="overlay-container">
                                    <img src="images/portfolio-1-3.jpg" alt="">
                                    <div class="overlay-top">
                                        <div class="text">
                                            <h3><a href="portfolio-item.html">Project Title</a></h3>
                                            <p class="small">Web Design</p>
                                        </div>
                                    </div>
                                    <div class="overlay-bottom">
                                        <div class="links">
                                            <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 isotope-item app-development">
                <div class="image-box text-center">
                    <div class="overlay-container">
                        <img src="images/portfolio-2.jpg" alt="">
                        <div class="overlay-top">
                            <div class="text">
                                <h3><a href="portfolio-item.html">Project Title</a></h3>
                                <p class="small">App Development</p>
                            </div>
                        </div>
                        <div class="overlay-bottom">
                            <div class="links">
                                <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 isotope-item app-development">
                <div class="image-box text-center">
                    <div class="overlay-container">
                        <img src="images/portfolio-3.jpg" alt="">
                        <div class="overlay-top">
                            <div class="text">
                                <h3><a href="portfolio-item.html">Project Title</a></h3>
                                <p class="small">App Development</p>
                            </div>
                        </div>
                        <div class="overlay-bottom">
                            <div class="links">
                                <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 isotope-item web-design">
                <div class="image-box text-center">
                    <div class="overlay-container">
                        <img src="images/portfolio-4.jpg" alt="">
                        <div class="overlay-top">
                            <div class="text">
                                <h3><a href="portfolio-item.html">Project Title</a></h3>
                                <p class="small">Web Design</p>
                            </div>
                        </div>
                        <div class="overlay-bottom">
                            <div class="links">
                                <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 isotope-item site-building">
                <div class="image-box text-center">
                    <div class="overlay-container">
                        <img src="images/portfolio-5.jpg" alt="">
                        <div class="overlay-top">
                            <div class="text">
                                <h3><a href="portfolio-item.html">Project Title</a></h3>
                                <p class="small">Site Building</p>
                            </div>
                        </div>
                        <div class="overlay-bottom">
                            <div class="links">
                                <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 isotope-item app-development">
                <div class="image-box text-center">
                    <div class="overlay-container">
                        <img src="images/portfolio-6.jpg" alt="">
                        <div class="overlay-top">
                            <div class="text">
                                <h3><a href="portfolio-item.html">Project Title</a></h3>
                                <p class="small">App Development</p>
                            </div>
                        </div>
                        <div class="overlay-bottom">
                            <div class="links">
                                <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 isotope-item site-building">
                <div class="image-box text-center">
                    <div class="overlay-container">
                        <img src="images/portfolio-7.jpg" alt="">
                        <div class="overlay-top">
                            <div class="text">
                                <h3><a href="portfolio-item.html">Project Title</a></h3>
                                <p class="small">Site Building</p>
                            </div>
                        </div>
                        <div class="overlay-bottom">
                            <div class="links">
                                <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 isotope-item app-development">
                <div class="image-box text-center">
                    <div class="overlay-container">
                        <img src="images/portfolio-8.jpg" alt="">
                        <div class="overlay-top">
                            <div class="text">
                                <h3><a href="portfolio-item.html">Project Title</a></h3>
                                <p class="small">App Development</p>
                            </div>
                        </div>
                        <div class="overlay-bottom">
                            <div class="links">
                                <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 isotope-item web-design">
                <div id="carousel-portfolio-2" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators bottom margin-clear">
                        <li data-target="#carousel-portfolio-2" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-portfolio-2" data-slide-to="1"></li>
                        <li data-target="#carousel-portfolio-2" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div class="image-box text-center">
                                <div class="overlay-container">
                                    <img src="images/portfolio-9.jpg" alt="">
                                    <div class="overlay-top">
                                        <div class="text">
                                            <h3><a href="portfolio-item.html">Project Title</a></h3>
                                            <p class="small">Web Design</p>
                                        </div>
                                    </div>
                                    <div class="overlay-bottom">
                                        <div class="links">
                                            <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="image-box text-center">
                                <div class="overlay-container">
                                    <img src="images/portfolio-1-4.jpg" alt="">
                                    <div class="overlay-top">
                                        <div class="text">
                                            <h3><a href="portfolio-item.html">Project Title</a></h3>
                                            <p class="small">Web Design</p>
                                        </div>
                                    </div>
                                    <div class="overlay-bottom">
                                        <div class="links">
                                            <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="image-box text-center">
                                <div class="overlay-container">
                                    <img src="images/portfolio-1-5.jpg" alt="">
                                    <div class="overlay-top">
                                        <div class="text">
                                            <h3><a href="portfolio-item.html">Project Title</a></h3>
                                            <p class="small">Web Design</p>
                                        </div>
                                    </div>
                                    <div class="overlay-bottom">
                                        <div class="links">
                                            <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 isotope-item app-development">
                <div class="image-box text-center">
                    <div class="overlay-container">
                        <img src="images/portfolio-10.jpg" alt="">
                        <div class="overlay-top">
                            <div class="text">
                                <h3><a href="portfolio-item.html">Project Title</a></h3>
                                <p class="small">App Development</p>
                            </div>
                        </div>
                        <div class="overlay-bottom">
                            <div class="links">
                                <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 isotope-item app-development">
                <div class="image-box text-center">
                    <div class="overlay-container">
                        <img src="images/portfolio-11.jpg" alt="">
                        <div class="overlay-top">
                            <div class="text">
                                <h3><a href="portfolio-item.html">Project Title</a></h3>
                                <p class="small">App Development</p>
                            </div>
                        </div>
                        <div class="overlay-bottom">
                            <div class="links">
                                <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 isotope-item web-design">
                <div class="image-box text-center">
                    <div class="overlay-container">
                        <img src="images/portfolio-12.jpg" alt="">
                        <div class="overlay-top">
                            <div class="text">
                                <h3><a href="portfolio-item.html">Project Title</a></h3>
                                <p class="small">Web Design</p>
                            </div>
                        </div>
                        <div class="overlay-bottom">
                            <div class="links">
                                <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 isotope-item site-building">
                <div class="image-box text-center">
                    <div class="overlay-container">
                        <img src="images/portfolio-13.jpg" alt="">
                        <div class="overlay-top">
                            <div class="text">
                                <h3><a href="portfolio-item.html">Project Title</a></h3>
                                <p class="small">Site Building</p>
                            </div>
                        </div>
                        <div class="overlay-bottom">
                            <div class="links">
                                <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 isotope-item app-development">
                <div class="image-box text-center">
                    <div class="overlay-container">
                        <img src="images/portfolio-14.jpg" alt="">
                        <div class="overlay-top">
                            <div class="text">
                                <h3><a href="portfolio-item.html">Project Title</a></h3>
                                <p class="small">App Development</p>
                            </div>
                        </div>
                        <div class="overlay-bottom">
                            <div class="links">
                                <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 isotope-item site-building">
                <div class="image-box text-center">
                    <div class="overlay-container">
                        <img src="images/portfolio-15.jpg" alt="">
                        <div class="overlay-top">
                            <div class="text">
                                <h3><a href="portfolio-item.html">Project Title</a></h3>
                                <p class="small">Site Building</p>
                            </div>
                        </div>
                        <div class="overlay-bottom">
                            <div class="links">
                                <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 isotope-item app-development">
                <div class="image-box text-center">
                    <div class="overlay-container">
                        <img src="images/portfolio-16.jpg" alt="">
                        <div class="overlay-top">
                            <div class="text">
                                <h3><a href="portfolio-item.html">Project Title</a></h3>
                                <p class="small">App Development</p>
                            </div>
                        </div>
                        <div class="overlay-bottom">
                            <div class="links">
                                <a href="portfolio-item.html" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="space-bottom"></div>
        <hr>
    </div>
    <div class="owl-carousel content-slider">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-8">
                    <div class="testimonial text-center">
                        <div class="testimonial-image">
                            <img src="images/testimonial-1.jpg" alt="Jane Doe" title="Jane Doe" class="rounded-circle">
                        </div>
                        <h3 class="mt-3">Just Perfect!</h3>
                        <div class="separator"></div>
                        <div class="testimonial-body">
                            <blockquote>
                                <p>Sed ut perspiciatis unde omnis iste natu error sit voluptatem accusan tium dolore laud antium, totam rem dolor sit amet tristique pulvinar, turpis arcu rutrum nunc, ac laoreet turpis augue a justo.</p>
                            </blockquote>
                            <div class="testimonial-info-1">- Jane Doe</div>
                            <div class="testimonial-info-2">By Company</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-8">
                    <div class="testimonial text-center">
                        <div class="testimonial-image">
                            <img src="images/testimonial-2.jpg" alt="Jane Doe" title="Jane Doe" class="rounded-circle">
                        </div>
                        <h3 class="mt-3">Amazing!</h3>
                        <div class="separator"></div>
                        <div class="testimonial-body">
                            <blockquote>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et cupiditate deleniti ratione in. Expedita nemo, quisquam, fuga adipisci omnis ad mollitia libero culpa nostrum est quia eos esse vel!</p>
                            </blockquote>
                            <div class="testimonial-info-1">- Jane Doe</div>
                            <div class="testimonial-info-2">By Company</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="clients-container">
            <div class="clients">
                <div class="client-image object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">
                    <a href="#"><img src="images/client-1.png" alt=""></a>
                </div>
                <div class="client-image object-non-visible" data-animation-effect="fadeIn" data-effect-delay="200">
                    <a href="#"><img src="images/client-2.png" alt=""></a>
                </div>
                <div class="client-image object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
                    <a href="#"><img src="images/client-3.png" alt=""></a>
                </div>
                <div class="client-image object-non-visible" data-animation-effect="fadeIn" data-effect-delay="400">
                    <a href="#"><img src="images/client-4.png" alt=""></a>
                </div>
                <div class="client-image object-non-visible" data-animation-effect="fadeIn" data-effect-delay="500">
                    <a href="#"><img src="images/client-5.png" alt=""></a>
                </div>
                <div class="client-image object-non-visible" data-animation-effect="fadeIn" data-effect-delay="600">
                    <a href="#"><img src="images/client-6.png" alt=""></a>
                </div>
                <div class="client-image object-non-visible" data-animation-effect="fadeIn" data-effect-delay="700">
                    <a href="#"><img src="images/client-7.png" alt=""></a>
                </div>
                <div class="client-image object-non-visible" data-animation-effect="fadeIn" data-effect-delay="800">
                    <a href="#"><img src="images/client-8.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section end -->

<!-- section start -->
<!-- ================ -->
<section class="pv-40 stats padding-bottom-clear dark-translucent-bg hovered background-img-6">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-6 text-center">
                <div class="feature-box object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
                    <span class="icon without-bg"><i class="fa fa-diamond"></i></span>
                    <h3><strong>Projects</strong></h3>
                    <span class="counter" data-to="1525" data-speed="5000">0</span>
                </div>
            </div>
            <div class="col-lg-3 col-6 text-center">
                <div class="feature-box object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
                    <span class="icon without-bg"><i class="fa fa-users"></i></span>
                    <h3><strong>Clients</strong></h3>
                    <span class="counter" data-to="1225" data-speed="5000">0</span>
                </div>
            </div>
            <div class="col-lg-3 col-6 text-center">
                <div class="feature-box object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
                    <span class="icon without-bg"><i class="fa fa-cloud-download"></i></span>
                    <h3><strong>Downloads</strong></h3>
                    <span class="counter" data-to="12235" data-speed="5000">0</span>
                </div>
            </div>
            <div class="col-lg-3 col-6 text-center">
                <div class="feature-box object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
                    <span class="icon without-bg"><i class="fa fa-share"></i></span>
                    <h3><strong>Shares</strong></h3>
                    <span class="counter" data-to="15002" data-speed="5000">0</span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section end -->