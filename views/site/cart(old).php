<style>
    .cart {background-color: #565656;}

</style>

<!-- breadcrumb start -->
<!-- ================ -->
<div class="breadcrumb-container dark" style="margin-top: -5px">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="/">Главная</a></li>
            <li class="breadcrumb-item active">Корзина</li>
        </ol>
    </div>
</div>
<!-- breadcrumb end -->

<!-- main-container start -->
<!-- ================ -->
<section class="main-container dark-bg">

    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12">

                <!-- page-title start -->
                <!-- ================ -->
                <h1 class="page-title">Shopping Cart</h1>
                <div class="separator-2"></div>
                <!-- page-title end -->

                <table class="table cart table-hover table-colored">
                    <thead>
                    <tr>
                        <th>Product </th>
                        <th>Price </th>
                        <th>Quantity</th>
                        <th>Remove </th>
                        <th class="amount">Total </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="remove-data cart">
                        <td class="product"><a href="shop-product.html">Product Title 1</a> <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas inventore modi.</small></td>
                        <td class="price">$99.50 </td>
                        <td class="quantity">
                            <div class="form-group">
                                <input type="text" class="form-control" value="2">
                            </div>
                        </td>
                        <td class="remove"><a href="#" class="btn btn-remove btn-sm btn-default">Remove</a></td>
                        <td class="amount">$199.00 </td>
                    </tr>

                    <tr class="cart">
                        <td colspan="3">Discount Coupon</td>
                        <td colspan="2">
                            <div class="form-group">
                                <input type="text" class="form-control">
                            </div>
                        </td>
                    </tr>
                    <tr class="cart">
                        <td class="total-quantity" colspan="4">Total 8 Items</td>
                        <td class="total-amount">$1997.00</td>
                    </tr>
                    </tbody>
                </table>
                <div class="text-right">
                    <a href="shop-cart.html" class="btn btn-group btn-default">Update Cart</a>
                    <a href="shop-checkout.html" class="btn btn-group btn-default">Checkout</a>
                </div>

            </div>
            <!-- main end -->

        </div>
    </div>
</section>
<!-- main-container end -->