<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>

<!-- main-container start -->
<!-- ================ -->
<div class="main-container parallax dark-bg border-clear text-center margin-clear" style="background-image:url('');">
    <div class="container">
        <div class="row justify-content-lg-center">
            <!-- main start -->
            <!-- ================ -->
            <div class="main col-lg-6 pv-40">
                <h1 class="page-title extra-large"><span class="text-default"><?= $exception->statusCode ?></span></h1>
                <h2 class="mt-4"><?= $message ?></h2>
<!--                <p class="lead">The requested URL was not found on this server. Make sure that the Web site address displayed in the address bar of your browser is spelled and formatted correctly.</p>-->
<!--                <form role="search">-->
<!--                    <div class="form-group has-feedback">-->
<!--                        <input type="text" class="form-control" placeholder="Search">-->
<!--                        <i class="fa fa-search form-control-feedback"></i>-->
<!--                    </div>-->
<!--                </form>-->
                <a href="/" class="btn btn-default btn-animated btn-lg">Вернуться на главную<i class="fa fa-home"></i></a>
            </div>
            <!-- main end -->
        </div>
    </div>
</div>
<!-- main-container end -->
