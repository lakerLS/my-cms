<?php

use app\widgets\myMenu;

?>

<!-- sidebar start -->
<!-- ================ -->
<aside class="col-lg-4 col-xl-3 order-lg-1">
    <div class="sidebar">

        <div class="block clearfix">
            <h3 class="title">Профиль</h3>
            <div class="separator-2"></div>
            <nav>
                <ul class="nav flex-column">

                    <?= myMenu::sidebar([
                        ['label' => 'Ваш аккаунт', 'url' => '/account', 'active' => 'user/settings/account'],
                        ['label' => 'Ваш профиль', 'url' => '/user/settings/profile'],
                    ]) ?>

                </ul>
            </nav>
        </div>

        <? if (Yii::$app->user->can('admin')) { ?>
        <div class="block clearfix">
            <h3 class="title">Управление доступом</h3>
            <div class="separator-2"></div>
            <nav>
                <ul class="nav flex-column">

                    <?= myMenu::sidebar([
                        ['label' => 'Пользователи', 'url' => '/user/admin/index'],
                        ['label' => 'Роли', 'url' => '/permit/access/role'],
                        ['label' => 'Правила доступа', 'url' => '/permit/access/permission'],
                    ]) ?>

                </ul>
            </nav>
        </div>

        <div class="block clearfix">
            <h3 class="title">Администратору</h3>
            <div class="separator-2"></div>
            <nav>
                <ul class="nav flex-column">

                    <?= myMenu::sidebar([
                        ['label' => 'Пункты меню и содержимое', 'url' => '/category'],
                        ['label' => 'Типы содержимого', 'url' => '/type'],
                    ]) ?>

                </ul>
            </nav>
        </div>
        <? } ?>

        <div class="block clearfix">
            <h3 class="title">Featured Project</h3>
            <div class="separator-2"></div>
            <div id="carousel-portfolio-sidebar" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-portfolio-sidebar" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-portfolio-sidebar" data-slide-to="1"></li>
                    <li data-target="#carousel-portfolio-sidebar" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="image-box shadow text-center mb-20">
                            <div class="overlay-container">
                                <img src="/images/portfolio-11.jpg" alt="">
                                <a href="/portfolio-item.html" class="overlay-link">
                                    <i class="fa fa-link"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="image-box shadow text-center mb-20">
                            <div class="overlay-container">
                                <img src="/images/portfolio-1-2.jpg" alt="">
                                <a href="/portfolio-item.html" class="overlay-link">
                                    <i class="fa fa-link"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="image-box shadow text-center mb-20">
                            <div class="overlay-container">
                                <img src="/images/portfolio-1-3.jpg" alt="">
                                <a href="/portfolio-item.html" class="overlay-link">
                                    <i class="fa fa-link"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</aside>
<!-- sidebar end -->