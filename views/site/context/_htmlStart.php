<!-- main-container start -->
<!-- ================ -->
<section class="main-container dark-bg">

    <div class="container">
        <div class="row">

            <!-- sidebar start -->
            <!-- ================ -->
            <?= $this->render ('//site/context/_sidebar') ?>
            <!-- sidebar end -->

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-lg-8 order-lg-2 ml-xl-auto">

                <!-- page-title start -->
                <!-- ================ -->
                <h1 class="page-title"><?= $this->title ?></h1>
                <div class="separator-2"></div>
                <!-- page-title end -->