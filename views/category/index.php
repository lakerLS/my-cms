<?php

use kartik\tree\TreeView;
use kartik\tree\Module;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Меню и содержимое';
$this->params['breadcrumbs'][] = $this->title;


?>

<? $this->registerJsFile("myCssJs/js/bootstrap/bootstrap.js",['depends' => [\yii\web\JqueryAsset::className()]]) ?>

<!-- page title -->
<header id="page-header" style="margin-bottom: 30px">
    <h1><?= $this->title ?></h1>
    <ol class="breadcrumb">
        <li><a href="#">Наполнение сайта</a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</header>
<!-- /page title -->

<div class="panel panel-default">
    <div class="panel-heading">
        <span class="title elipsis">
            <strong style="font-size: 20px">Категории</strong>
        </span>
    </div>
    <div class="panel-body">

        <?
        $s = null;
        /** @var array $treeView */
        /** @var array $treeCheck */
        echo TreeView::widget([
            'query' => $treeView, // передаем данные из таблицы category

            'options' => ['id' => 'myTree'],

            'topRootAsHeading' => true, // this will override the headingOptions
            'detailOptions' => ['style' => 'background-color:#f6f8f8;'],
            'treeOptions' => ['style' => 'height:395px;'],
            'mainTemplate' =>
                '<div class="row darkColor">
                                <div class="col-sm-5">
                                    {wrapper}
                                </div>
                                <div class="col-sm-7">
                                    {detail}
                                </div>
                            </div>',

            'headerTemplate' =>
                '<div class="row">
                                <div class="col-sm-12">
                                    {search}
                                </div>
                            </div>',


            // single query fetch to render the tree
            // use the Product model you have in the previous step
            'headingOptions' => ['label' => false],
            'fontAwesome' => true,     // optional
            'allowNewRoots' => !empty($treeCheck) ? false : true, // выводить кнопку создания дерева
            'isAdmin' => !empty($treeCheck) ? false : true,         // optional (toggle to enable admin mode)
            'iconEditSettings'=> ['show' => 'none'],
            'displayValue' => $treeCheck->root,        // initial display value
            'softDelete' => false,       // defaults to true
            'cacheSettings' => [
                'enableCache' => false   // defaults to true
            ],

            'showIDAttribute' => false,
            'showNameAttribute' => false,

            'nodeAddlViews' => [
                Module::VIEW_PART_2 => '@app/views/category/context/_form_2',
            ]
        ]);

        ?>

    </div>
</div>



<div class="panel panel-default">

    <?php Pjax::begin(); ?>

    <!-- Pjax портит отображение, поэтому прописываем стиль -->
    <div class="panel-heading" style="background-color: white; text-transform: uppercase; border-color: #ddd;">
        <span class="title elipsis">
            <strong style="font-size: 20px">Содержимое</strong>
        </span>
        <ul class="options pull-right list-inline">

            <li><?= Html::a('<i class="fa fa-paper-plane"></i>Добавить запись',
                    ['content/create'],
                    ['class' => 'btn btn-3d btn-sm btn-green',
                        'title' => 'Создание новой записи',
                    ])
                ?></li>

            <li><?= Html::a('<i class="fa fa-filter"></i>Фильтр по категории',
                    ['/category?active=3'],
                    ['class' => 'btn btn-3d btn-sm btn-blue',
                        'onmouseenter' => 'category_filter()',
                        'title' => 'Выберите необходимую категорию выше',
                        'id' => 'button_filter',
                    ])
                ?></li>
            <li><?= Html::a('<i class="fa fa-refresh"></i>Сбросить',
                    ['/category?active=3'],
                    ['class' => 'btn btn-3d btn-sm btn-purple',
                        'title' => 'Отобразить все записи',
                    ])
                ?></li>
        </ul>
    </div>


    <div class="panel-body">
        <?
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout'=>"\n{items}\n{pager}",

            'tableOptions' => [
                'class' => 'table table-striped table-hover table-bordered dataTable no-footer',
            ],

            'columns' => [

                ['attribute' => 'name',
                    'filterInputOptions' => ['class' => 'form-control', 'placeholder' => 'Поиск по заголовку...'],
                ],

                ['attribute' => 'category_id',
                    'value' => 'category.name',

                    'headerOptions' => [
                        'style' => 'text-align: center',
                        'width' => '360',
                        'hidden'
                    ],

                    'filterInputOptions' => ['type' => 'hidden'],

                    'contentOptions' => [
                        'style' => 'text-align: center',
                    ]
                ],

                ['attribute' => 'type',
                    'value' => 'oneType.name',

                    'header' => 'Дизайн',
                    'headerOptions' => ['width' => '60'],
                    'contentOptions' => ['style' => 'text-align: center'],
                ],

                ['class' => 'yii\grid\ActionColumn',
                    'header' => 'Действия',
                    'headerOptions' => ['style' => 'text-align: center', 'width' => '105'] ,
                    'contentOptions' => ['style' => 'text-align: center;'],
                    'urlCreator'=>function($action, $model, $key, $index){
                        return ['content/'. $action, 'id' => $model->id];

                    },
                    'template' => '{moveUp} {moveDown} {update} {delete} ',
                    'buttons' => [
                        'moveUp' => function($url, $model, $key) {     // render your custom button
                            return Html::a("<i class='glyphicon glyphicon-triangle-top'></i>", ["content/move-up", "id" => $model->id], ['data-method' => 'post']);
                        },
                        'moveDown' => function($url, $model, $key) {     // render your custom button
                            return Html::a("<i class='glyphicon glyphicon-triangle-bottom'></i>", ["content/move-down", "id" => $model->id], ['data-method' => 'post']);
                        },
                    ]
                ],

            ],
        ]);
        ?>
    </div>

    <?php Pjax::end(); ?>

</div>
