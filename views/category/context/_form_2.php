<?php

use app\classes\FormHelper;
use app\models\Type;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="row">

    <?= $form->field($node, 'id')->textInput()->hiddenInput()->label(false)?>

    <div class="col-sm-6">
        <?= $form->field($node, 'name', FormHelper::required())->textInput(
                [
                    'onchange' => "countCharCategory()",
                    'onkeyup' => "countCharCategory()",
                ]
        )?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($node, 'url', FormHelper::required())->textInput()?>
    </div>

    <div class="col-sm-6">
        <?
        echo $form->field($node, 'type', FormHelper::required())->dropDownList(ArrayHelper::merge(
            ArrayHelper::map(Type::find()->where(['many' => '1'])->andWhere(['<>', 'type', 'pencil'])->all(), 'type', 'name'),
            ['static' => 'Статический']
        ))
        ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($node, 'image')->widget(CKEditor::className(), [
            'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                'customConfig' => '/myCssJs/js/configCKEDITORphoto.js',
                'height' => '40px',
            ]),
            'containerOptions' => ['style' => 'height: 10px']

        ]); ?>
    </div>

</div>

<div class="row">
    <div class="col-sm-12">
        <?= $form->field($node, 'description')->textarea();?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?= $form->field($node, 'tags')->textInput()?>
    </div>
</div>
