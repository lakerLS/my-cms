<?php

use app\classes\FormHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use himiklab\yii2\recaptcha\ReCaptcha;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel panel-default">
    <div class="panel-body form-input ">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'text')->textarea(['rows' => '5']) ?>

        <?//=  $form->field($faq, 'reCaptcha')->widget(ReCaptcha::className(), [
        //  'name' => 'NewUserRegisterForm[reCaptcha]',
        //])->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton('Ответить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
