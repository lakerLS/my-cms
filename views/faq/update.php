<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Faq */

$this->title = 'Ответить на: '.$model->name;
?>

<!-- page title -->
<header id="page-header" style="margin-bottom: 30px">
    <h1><?= $this->title ?></h1>
    <ol class="breadcrumb">
        <li><a href="#">Наполнение сайта</a></li>
        <li><a href="/faq?active=3">Ответы на вопросы</a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</header>
<!-- /page title -->

<div class="faq-update">

    <div class="faq-form">

    <?= $this->render('context/_form', [
            'model' => $model
    ]) ?>

    </div>

</div>
