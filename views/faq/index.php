<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\FaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ответы на вопросы';
?>

<!-- page title -->
<header id="page-header" style="margin-bottom: 30px">
    <h1><?= $this->title ?></h1>
    <ol class="breadcrumb">
        <li><a href="#">Наполнение сайта</a></li>
        <li class="active"><?= $this->title ?></li>
    </ol>
</header>
<!-- /page title -->


<div class="panel panel-default">
    <div class="panel-body">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => '{items}',
            'columns' => [
                ['class' => 'yii\grid\SerialColumn',
                    'options' => ['width' => '1'],
                ],

                'name',
                'text',

                ['class' => 'yii\grid\ActionColumn',
                    'header' => 'Действия',
                    'contentOptions' => ['width' => '1', 'align' => 'center']
                ],
            ],
        ]); ?>
    </div>
</div>
