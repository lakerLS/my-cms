<?php


use app\classes\FormHelper;
use app\models\Category;
use app\models\Content;
use app\models\Sections;
use app\models\Type;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>

<?
// Если используем модальный карандаш
if ($mode == 'create' && !empty($pencil)) {
    $type = Type::find()->where(['type' => $pencil['type']])->one();
    $explodeType = explode(",", $type->select); //делим на массивы
}

// Если создаем слайдер
elseif ($mode == 'create' && !empty($slider)) {
    $type = Type::find()->where(['type' => 'slider'])->one();
    $explodeType = explode(",", $type->select); //делим на массивы
}

elseif ($mode == 'create') {
    $type = Type::find()->where(['type' => $meta ? $meta->type : Yii::$app->request->get('type')])->one();
    $explodeType = explode(",", $type->select); //делим на массивы
}

elseif ($mode == 'update') {
    $type = Type::find()->where(['type' => !empty($model->type) ? $model->type : $pencil])->one();
    $explodeType = explode(',', $type->select);
}

elseif ($mode == 'modalCreate') {
    $type = Type::find()->where(['type' => $info->type])->one();
    $explodeType = explode(",", $type->select); //делим на массивы
}

foreach ($explodeType as $margin)
{
    // Через elseif выводим уникальные формы для нужных нам полей.
    if ($margin == 'image')
    {
        echo $type->description;

        echo $form->field($model, $margin)->widget(CKEditor::className(), [
            'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                'preset' => 'basic',
                'customConfig' => '/myCssJs/js/configCKEDITOR.js',
            ]),
            'options' => [
                'id' => 'image-'.$model->id ,
            ]
        ]);
    }

    elseif ($margin == 'text')
    {

        echo $form->field($model, $margin)->widget(CKEditor::className(), [
            'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                'allowedContent' => true,
                'height' => '400',
                'customConfig' => '/myCssJs/js/configCKEDITOR.js',
            ]),
            'options' => [
                'id' => !empty($model->id) ? 'text-'.$model->id : 'text-'.$pencil['id'] ,
            ]
        ]);
    }

    // Выводим заголовок, если карандаш, то поле выводится скрытым, ниже (после цикла)
    elseif ($margin == 'name')
        echo $form->field($model, $margin, FormHelper::required())->textInput(['id' => 'content-name-'.$model->id, 'maxlength' => true, 'onchange' => "countCharContent('$model->id')", 'onkeyup' => "countCharContent('$model->id')"]);

    elseif ($margin == 'url')
    {
        echo $form->field($model, $margin, FormHelper::required())
            ->textInput(['id' => !empty($model->id) ? 'content-url-'.$model->id : 'content-url-'.$pencil['id']]);
    }

    elseif ($margin == 'description')
        echo $form->field($model, $margin)->textarea(['maxlength' => true]);


    elseif ($margin == 'section') {

        // Необходимо для корректного отображения доступных разделов в админке
        if (!empty(Yii::$app->request->get('category_id')))
            $category = Category::find()->where(['id' => Yii::$app->request->get('category_id') ])->one();

        elseif (empty($model->category->id))
            $category = $meta;



        // Если создается запись из админки
        if (!empty(Yii::$app->request->get('category_id')))
            $redirect = '/sections/create?category_id='.Yii::$app->request->get('category_id').'&type='.Yii::$app->request->get('type');

        // Если редактируется запись из админки
        elseif (!empty(Yii::$app->request->get('id')))
            $redirect = '/sections/create?id='.$model->id.'&type='.$model->category_id;

        // Редирект из модального окна (Many)
        elseif (!empty($meta->id))
            $redirect = '/sections/create?type='.$meta->id.'&redirect=/'.Yii::$app->request->pathInfo;

        // Редирект из модального окна (One)
        else
            $redirect = '/sections/create?type='.$model->category->id.'&redirect=/'.Yii::$app->request->pathInfo;


        echo $form->field($model, $margin)
            ->dropDownList(ArrayHelper::map(
                Sections::find()
                    ->where(['type' => !empty($model->category->id) ? $model->category->id : $category->id])->all(),
                'name', 'name'),
                ['prompt' => '---------']
            )->label('Раздел <a class="badge" href="'.$redirect.'">Добавить раздел</a>');


    }

    // Стандартная форма для полей не исключений.
    else
        echo $form->field($model, $margin)->textInput();
}

if (!empty($pencil))
    echo $form->field($model, 'name')->hiddenInput(['id' => 'content-name-pencil'.$pencil['id'], 'value' => $pencil['name']])->label(false);

if (!empty($slider))
    echo $form->field($model, 'url')->hiddenInput(['value' => 'slider'])->label(false);

// При создании заполняем автора
if ($mode == 'create') {
    echo $form->field($model, 'author_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false);
}
?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
    </div>
