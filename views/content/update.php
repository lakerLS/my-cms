<?php

use app\classes\FormHelper;
use app\models\Category;
use app\models\Type;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\models\Content */

$this->title = $model->name;
?>

<!-- page title -->
<? if (Yii::$app->controller->route == 'content/update') { ?>
<header id="page-header">
    <h1>Редактировать запись: <?= $model->name ?></h1>
    <ol class="breadcrumb">
        <li><a href="#">Контент</a></li>
        <li><a href="/category">Меню и содержимое</a></li>
        <li class="active"><?= $model->name ?></li>
    </ol>
</header>
<? } ?>
<!-- /page title -->

<div class="news-form" style="margin-top: 30px">

    <?

    // не помню для чего эти строки, возможно еще где-то используются (но это не точно)
//    $str = strstr(strstr(Yii::$app->request->pathInfo, '/'), '.', true);
//    $category = Category::find()->where(['url' => empty($str) ? strstr(Yii::$app->request->pathInfo, '.', true) : str_replace('/', '', $str)])->one();

    // Если редактируется (карандаш) в списке записей
    if (!empty($meta) || !empty($pencil) || !empty($slider))
        $form = ActiveForm::begin(['action' => '/content/update?id='.$model->id.'&redirect=/'.Yii::$app->request->pathInfo.'&update=many']);

    // Если редактируется в конкретной записи или админке
    elseif (!empty(Yii::$app->request->get('redirect')))
        $form = ActiveForm::begin();

    else
        $form = ActiveForm::begin(['action' => '/content/update?id='.$model->id.'&redirect='.strstr(Yii::$app->request->pathInfo, '/', true).'&update=one']);


    // Выводить поля "Дизайн" и "Категория" только из админки.
    if(Yii::$app->request->pathInfo == 'content/update' && Yii::$app->request->get('type') != 'pencil'){

        echo $form->field($model, 'category_id', FormHelper::required())->dropDownList(
            ArrayHelper::map(Category::find()->where(['<>', 'lvl', '0'])->andWhere(['<>', 'type', 'list'])->all(), 'id', 'name')
        );

        echo $form->field($model, 'type', FormHelper::required())->dropDownList(
            ArrayHelper::map(Type::find()->where(['<>', 'type', 'pencil'])->andWhere(['one' => '1'])->all(), 'type', 'name')
        );
    }

    // Выводим остальные поля
    echo $this->render('_form', [
        'form' => $form,
        'model' => $model,
        'mode' => 'update',
        'meta' => isset($meta) ? $meta : null,
        'pencil' => isset($pencil) ? $pencil : null,
    ]);


    ActiveForm::end(); ?>
</div>
