<?php


use app\classes\WidHelper;
use app\models\Category;
use app\widgets\modalBody;
use yii\bootstrap\Html;

/** @var $meta */
Yii::$app->view->params['meta'] = $meta;
?>

<!-- main-container start -->
<!-- ================ -->
<section class="main-container dark-bg">

    <div class="container ">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12">

                <? modalBody::pencil('header') ?>

                <!-- page-title start -->
                <!-- ================ -->
                <h1 class="page-title"><?= $meta->description ?></h1>
                <div class="separator-2"></div>
                <!-- page-title end -->

                <!-- masonry grid start -->
                <!-- ================ -->
                <div class="row">

                    <div class="row">

                        <? $content = Category::find()
                            ->andWhere(['>', 'lft', $meta->lft])
                            ->andWhere(['<', 'rgt', $meta->rgt])
                            ->orderBy(['root' => SORT_ASC, 'lft' => SORT_ASC])
                            ->all(); ?>

                        <?foreach ($content as $value) { ?>

                            <!-- masonry grid item start -->
                            <div class="masonry-grid-item col-md-6 col-lg-4">
                                <!-- blogpost start -->
                                <article class="blogpost shadow-2 light-gray-bg bordered">
                                    <div class="overlay-container">
                                        <?= WidHelper::image($value) ?>
                                        <?= Html::a('<i class="fa fa-link"></i>', ['content/one', 'model' => $value], ['class' => 'overlay-link']) ?>
                                    </div>
                                    <header style="background-color: #4c4c4c;">
                                        <h2 style="text-align: center">
                                            <?= Html::a($value->name, ['content/one', 'model' => $value]) ?>
                                        </h2>
                                    </header>
                                </article>
                                <!-- blogpost end -->
                            </div>
                            <!-- masonry grid item end -->

                        <? } ?>

                    </div>

                    <? modalBody::pencil('footer') ?>

                </div>
                <!-- masonry grid end -->
            </div>
            <!-- main end -->

        </div>
    </div>
</section>
<!-- main-container end -->