<?php

/* @var $this yii\web\View */

use app\classes\WidHelper;
use yii\widgets\ListView;

$this->title = $meta->name;
Yii::$app->view->params['modelMany'] = $model;
Yii::$app->view->params['meta'] = $meta;

?>

<!-- scrollToTop -->
<!-- ================ -->
<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>

<!-- page wrapper start -->
<!-- ================ -->
<div class="page-wrapper">

    <!-- section start -->
    <!-- ================ -->
    <div class="dark-bg section">
        <div class="container-fluid">
            <!-- filters start -->
            <div class="sorting-filters text-center mb-20 d-flex justify-content-center">
                <form class="form-inline">
                    <div class="form-group">
                        <label>Sort by</label>
                        <select class="form-control">
                            <option selected="selected">Date</option>
                            <option>Price</option>
                            <option>Model</option>
                        </select>
                    </div>
                    <div class="form-group ml-1">
                        <label>Order</label>
                        <select class="form-control">
                            <option selected="selected">Acs</option>
                            <option>Desc</option>
                        </select>
                    </div>
                    <div class="form-group ml-1">
                        <label>Price $ (min/max)</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group ml-1">
                        <label class="invisible">Price $ (max)</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group ml-1">
                        <label>Category</label>
                        <select class="form-control">
                            <option selected="selected">Smartphones</option>
                            <option>Tablets</option>
                            <option>Smart Watches</option>
                            <option>Desktops</option>
                            <option>Software</option>
                            <option>Accessories</option>
                        </select>
                    </div>
                    <div class="form-group ml-1">
                        <a href="#" class="btn btn-default">Submit</a>
                    </div>
                </form>
            </div>
            <!-- filters end -->
        </div>
    </div>
    <!-- section end -->

    <!-- main-container start -->
    <!-- ================ -->
    <section class="main-container">

        <div class="container">
            <div class="row">

                <? WidHelper::setFlash('formSubmitted','Ваша заявка принята, мы свяжемся с Вами в ближайшее время, для уточнения деталей. Спасибо за внимание к проекту.') ?>

                <!-- main start -->
                <!-- ================ -->
                <div class="main col-md-12">
                    <!-- pills start -->
                    <!-- ================ -->
                    <div class="clear-style">
                            <div class="row">

                                <?php
                                // echo '<div class="read" style="text-align: right"><b>Сортировать по:</b> &nbsp&nbsp' . $sort->link('name') . ' &nbsp;&nbsp;&nbsp;&nbsp;' . $sort->link('id') . '</div></br>';

                                echo ListView::widget([
                                    'dataProvider' => $dataProvider,

                                    //'layout' => "{items}\n{pager}",

                                    'layout' => "{items}",

                                    'itemOptions' => [
                                        'tag' => false,
                                    ],
                                    'options' => [
                                        'tag' => 'article',
                                        'class' => 'post',
                                        // 'id' => 'list-wrapper',
                                    ],
                                    'itemView' => '_services',

                                    'viewParams' => [
                                        'meta' => $meta,
                                    ],
                                ]); ?>

                            </div>
                    </div>
                    <!-- pills end -->
                    <!-- pagination start -->
                    <nav aria-label="Page navigation">
                        <ul class="pagination justify-content-center">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <i aria-hidden="true" class="fa fa-angle-left"></i>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">4</a></li>
                            <li class="page-item"><a class="page-link" href="#">5</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <i aria-hidden="true" class="fa fa-angle-right"></i>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <!-- pagination end -->
                </div>
                <!-- main end -->

            </div>
        </div>
    </section>
    <!-- main-container end -->

    <!-- section start -->
    <!-- ================ -->
    <section class="section dark-translucent-bg background-img-2 pv-40" style="background-position: 50% 32%;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="call-to-action text-center">
                        <div class="row justify-content-lg-center">
                            <div class="col-lg-8">
                                <h2 class="title"><strong>Subscribe</strong> To Our Newsletter</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus error pariatur deserunt laudantium nam, mollitia quas nihil inventore, quibusdam?</p>
                                <div class="separator"></div>
                                <form class="form-inline margin-clear d-flex justify-content-center">
                                    <div class="form-group has-feedback">
                                        <label class="sr-only" for="subscribe3">Email address</label>
                                        <input type="email" class="form-control form-control-lg" id="subscribe3" placeholder="Enter email" name="subscribe3" required="">
                                        <i class="fa fa-envelope form-control-feedback"></i>
                                    </div>
                                    <button type="submit" class="btn btn-lg btn-gray-transparent btn-animated margin-clear ml-3">Submit <i class="fa fa-send"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section end -->

</div>
<!-- page-wrapper end -->





