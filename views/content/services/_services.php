<?php

use app\classes\WidHelper;
use app\widgets\modalBody;
use app\widgets\modalButton;
use yii\bootstrap\Html;


/** @var $meta
 */
?>

<? modalBody::update($model, $meta) ?>
<? modalBody::services($model) ?>

<div class="col-lg-6 masonry-grid-item">
    <div class="listing-item bordered light-gray-bg mb-20">
        <div class="row grid-space-0">
            <div class="col-md-6 col-lg-4 col-xl-3">
                <div class="overlay-container">
                    <?= WidHelper::image($model, 1) ?>
                    <?= Html::a(null, ['content/one', 'model' => $model], ['class' => 'overlay-link popup-img-single']) ?>
                    <span class="badge">30% OFF</span>
                </div>
            </div>
            <div class="col-md-6 col-lg-8 col-xl-9">
                <div class="body">
                    <h3 class="margin-clear">
                        <?= Html::a($model->name, ['content/one', 'model' => $model]) ?>
                        <? WidHelper::updateDelete($model) ?>
                    </h3>
                    <p>
                        <i class="fa fa-star text-default"></i>
                        <i class="fa fa-star text-default"></i>
                        <i class="fa fa-star text-default"></i>
                        <i class="fa fa-star text-default"></i>
                        <i class="fa fa-star"></i>
                    </p>
                    <p class="small"><? WidHelper::text($model, 20) ?></p>
                    <div class="elements-list clearfix">
                        <span class="price"><del><?= Yii::$app->formatter->asCurrency($model->price_old) ?></del> <?= Yii::$app->formatter->asCurrency($model->price) ?></span>
                        <? modalButton::services($model, '<a href="#" class="pull-right btn btn-sm btn-default-transparent btn-animated">Заказать<i class="fa fa-shopping-cart"></i></a>') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>