<?php

/* @var $this yii\web\View */

use app\models\Content;
use app\widgets\modalBody;
use yii\widgets\ListView;

$this->title = $meta->name;
Yii::$app->view->params['modelMany'] = $model;
Yii::$app->view->params['meta'] = $meta;
?>

<!-- main-container start -->
<!-- ================ -->
<section class="main-container">

    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12">

                <!-- page-title start -->
                <!-- ================ -->
                <h1 class="page-title"><? modalBody::pencil('1') ?></h1>
                <div class="separator-2"></div>
                <!-- page-title end -->
                <p class="lead"><? modalBody::pencil('2') ?></p>

                <!-- isotope filters start -->
                <div class="filters">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="#" data-filter="*">Все</a></li>
                        <?php

                        foreach (Content::find()->where(['category_id' => $meta->id])->distinct()->select(['section'])->all() as $value) {
                            ?>

                            <li class="nav-item"><a class="nav-link" href="#" data-filter=".<?= str_replace(' ', '-', $value->section) ?>"><?= $value->section ?></a></li>

                            <?php
                        }
                        ?>
                    </ul>
                </div>
                <!-- isotope filters end -->

                <div class="isotope-container-fitrows row grid-space-10">

                    <?php
                    // echo '<div class="read" style="text-align: right"><b>Сортировать по:</b> &nbsp&nbsp' . $sort->link('name') . ' &nbsp;&nbsp;&nbsp;&nbsp;' . $sort->link('id') . '</div></br>';

                    echo ListView::widget([
                        'dataProvider' => $dataProvider,

                        'layout' => "{items}",

                        'itemOptions' => [
                            'tag' => false,
                        ],
                        'options' => [
                            'tag' => 'article',
                            'class' => 'post',
                            // 'id' => 'list-wrapper',
                        ],
                        'itemView' => '_portfolio',

                        'viewParams' => [
                            'meta' => $meta,
                            'url' => $meta->parents()->all(), // формируются url'ы одним запросом ко всем адресам
                        ],
                    ]);

                    ?>

                    <?= \yii\widgets\LinkPager::widget([
                        'pagination' => $pages,
                    ]);
                    ?>

                </div>

            </div>
            <!-- main end -->

        </div>
    </div>
</section>
<!-- main-container end -->