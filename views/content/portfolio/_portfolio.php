<?php

use app\classes\WidHelper;
use app\widgets\modalBody;

?>

<? modalBody::update($model, $model->category) ?>

<div class="col-md-6 isotope-item <?= str_replace(' ', '-', $model->section) ?>">
    <div class="image-box style-2 mb-20 shadow-2 bordered text-center">
        <div id="carousel-portfolio" class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ol class="carousel-indicators top">

                <?
                $count = 0;

                while (!empty(WidHelper::image($model, 1.5, $count)))
                {
                    if (!empty (WidHelper::image($model, 1.5, 1))){
                ?>

                        <li data-target="#carousel-portfolio" data-slide-to="<?= $count ?>" class="<?= $count == 0 ? 'active' : null ?>"></li>

                <?
                    }
                    $count ++;
                }
                ?>

            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">

                <?
                $count = 0;
                while (!empty($value = WidHelper::image($model,1.5, $count)))
                {
                ?>

                <div class="carousel-item <?= $count == 0 ? 'active' : null ?>">
                    <div class="overlay-container">
                        <?= $value ?>
                    </div>
                </div>

                <? $count ++;} ?>

                <? if (!empty($model->description)) { ?>
                    <div class="overlay-to-top">
                        <p class="small margin-clear"><?= $model->description ?></p>
                    </div>
                <? } ?>


            </div>
        </div>
        <div class="body light-gray-bg ">
            <h3><?= $model->name ?><? WidHelper::updateDelete($model) ?></h3>
            <div class="separator"></div>
            <p><?= $model->text ?></p>
            <a href="<?= $model->source ?>" class="btn btn-default btn-hvr hvr-shutter-out-horizontal margin-clear">Перейти<i class="fa fa-arrow-right pl-10"></i></a>
        </div>
    </div>
</div>