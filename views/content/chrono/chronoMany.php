<?php

use yii\widgets\ListView;

$this->title = $meta->name;
Yii::$app->view->params['modelMany'] = $model;
Yii::$app->view->params['meta'] = $meta;
?>

<!-- main-container start -->
<!-- ================ -->
<section class="main-container">

    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12">

                <!-- page-title start -->
                <!-- ================ -->
                <h1 class="page-title"><?= $meta->name ?></h1>
                <div class="separator-2"></div>
                <!-- page-title end -->

                <!-- timeline grid start -->
                <!-- ================ -->
                <div class="timeline clearfix">

                <?php
                // echo '<div class="read" style="text-align: right"><b>Сортировать по:</b> &nbsp&nbsp' . $sort->link('name') . ' &nbsp;&nbsp;&nbsp;&nbsp;' . $sort->link('id') . '</div></br>';

                echo ListView::widget([
                    'dataProvider' => $dataProvider,

                    'layout' => "{items}",

                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'options' => [
                        'tag' => 'article',
                        'class' => 'post',
                    ],

                    'itemView' => '_chrono',

                    'viewParams' => [
                        'meta' => $meta,
                        'url' => $url,
                    ],
                ]);

                ?>
                <?= \yii\widgets\LinkPager::widget([
                    'pagination' => $pages,
                ]);
                ?>
                </div>
                <!-- timeline grid end -->


            </div>
            <!-- main end -->

        </div>
    </div>
</section>
<!-- main-container end -->