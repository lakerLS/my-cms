<?php

use app\classes\WidHelper;
use app\widgets\modalBody;
use yii\bootstrap\Html;

/** @var array $meta */
/** @var integer $index */
?>

    <? modalBody::update($model, $meta) ?>

    <!-- timeline grid item start -->

<?
    if (($index+1) % 2 == 0) { ?>
    <div class="timeline-item pull-right" style="margin-top: 230px">
    <? } else { ?>
    <div class="timeline-item">
    <? } ?>

        <?//= print_r($meta); die?>

        <!-- blogpost start -->
        <article class="blogpost shadow-2 bordered chrono">
            <div class="overlay-container">
                <?= WidHelper::image($model) ?>
                <?= Html::a('<i class="fa fa-link"></i>', ['content/one', 'model' => $model], ['class' => 'overlay-link']) ?>
            </div>
            <header>
                <h2>
                    <?= Html::a($model->name, ['content/one', 'model' => $model]) ?>
                    <? WidHelper::updateDelete($model) ?>
                </h2>
                <div class="post-info">
                        <span class="post-date">
                          <i class="icon-calendar"></i>
                          <?= Yii::$app->formatter->asDate($model->date) ?>
                        </span>
                    <span class="submitted"><i class="icon-user-1"></i><a href="#"><?= $model->user->username ?></a></span>
                    <span class="comments"><i class="icon-chat"></i> <a href="#"><? WidHelper::commentCount($model) ?></a></span>
                </div>
            </header>
            <div class="blogpost-content">
                <? WidHelper::text($model) ?>
            </div>
            <footer class="clearfix">
                <div class="tags pull-left">
                    <i class="icon-tags"></i> <? WidHelper::tagBlog($meta, $model) ?>
                </div>

            </footer>
        </article>
        <!-- blogpost end -->
    </div>
    <!-- timeline grid item end -->

