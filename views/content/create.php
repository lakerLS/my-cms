<?php

use app\classes\FormHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use app\models\Category;
use app\models\Type;

/* @var $this yii\web\View */
/* @var $model app\models\Content */

$this->title = 'Добавить запись';
?>

<!-- page title // Отображать только из админки -->
<? if (!isset($meta) && empty($pencil) && empty($slider)) { ?>
<header id="page-header">
    <h1>Добавить запись</h1>
    <ol class="breadcrumb">
        <li><a href="#">Контент</a></li>
        <li><a href="/category">Меню и содержимое</a></li>
        <li class="active">Добавить запись</li>
    </ol>
</header>
<? } ?>
<!-- /page title -->

<div class="news-form" style="margin-top: 30px">

        <?

        // Если редактируется в списке записей
            $form = ActiveForm::begin(['action' => '/content/create?redirect=/'.Yii::$app->request->pathInfo.'&category_id='.$model->id]);

        // Если создается карандаш, то поля категория и дизайн страницы делаем скрытыми. Значение берем из GET.
        if (Yii::$app->request->get('type') == 'pencil')
            echo $form->field($model, 'category_id')->hiddenInput(['maxlength' => true, 'value' => Yii::$app->request->get('category_id')])->label(false);

        // Если используем модальное окно
        elseif (!empty($meta))
            echo $form->field($model, 'category_id')->hiddenInput(['value' => $meta->id])->label(false);

        // Если используем модальный карандаш или создаем слайдер
        elseif (!empty($pencil) || !empty($slider))
            echo $form->field($model, 'category_id')->hiddenInput(['value' => $pencil['category_id']])->label(false);


        // Создание записи из админки
        else
            echo $form->field($model, 'category_id', FormHelper::required())->dropDownList(
                ArrayHelper::map(Category::find()->where(['<>', 'lvl', '0'])->andWhere(['<>', 'type', 'list'])->all(), 'id', 'name'),
                [
                    'prompt' => '--------------------',
                    'options' => !empty (Yii::$app->request->get('category_id')) ? [Yii::$app->request->get('category_id') => ['Selected' => true]] : null,
                    'onchange' => 'document.location = "' . Url::to(['content/create']) . '?category_id="+this.value',
                ]
            );

        // Если выбрали категорию, то выводим дальше
        if (!empty(Yii::$app->request->get('category_id')) || !empty($meta) || !empty($pencil) || !empty($slider)) {

            // Тип(Дизайн) страницы. Скрытые поля для карандаша
            if (Yii::$app->request->get('type') == 'pencil') {
                echo $form->field($model, 'url')->hiddenInput(['maxlength' => true, 'value' => Yii::$app->request->get('url')])->label(false);
                echo $form->field($model, 'type')->hiddenInput(['maxlength' => true, 'value' => Yii::$app->request->get('type')])->label(false);
            }

            // Скрытые поля для МОДАЛЬНОГО карандаша
            elseif (!empty($pencil)) {
                echo $form->field($model, 'url')->hiddenInput(['value' => $pencil['url']])->label(false);
                echo $form->field($model, 'type')->hiddenInput(['value' => 'pencil'])->label(false);
            }

            // Скрытые поля для слайдера
            elseif(!empty($slider))
                echo $form->field($model, 'type')->hiddenInput(['value' => 'slider'])->label(false);

            // Используем модальное окно
            elseif (!empty($meta)) {
                //Если запись имеет такой же тип дизайна как и категория, то автоматически это прописываем. Если нет, то выводим выпадающий список.
                $type = Type::find()->where(['type' => $meta->type])->andWhere(['one' => '1'])->one();
                if (!empty($type->id))
                    echo $form->field($model, 'type')->hiddenInput(['value' => $meta->type])->label(false);

                else
                    echo $form->field($model, 'type', FormHelper::required())->dropDownList(
                        ArrayHelper::map(Type::findAll(['one' => '1']), 'type', 'name'),
                        ['prompt' => '--------------------']
                    );
            }

            // Создание записи из админки
            elseif (Yii::$app->request->get('type') != 'pencil' || empty(Yii::$app->request->get('redirect'))) {

                $redirect = Yii::$app->request->get('redirect') ? '&redirect=' : null;

                echo $form->field($model, 'type', FormHelper::required())->dropDownList(
                    ArrayHelper::map(Type::findAll(['one' => '1']), 'type', 'name'),
                    [
                        'prompt' => '--------------------',
                        'options' => !empty (Yii::$app->request->get('type')) ? [Yii::$app->request->get('type') => ['Selected' => true]] : null,
                        'onchange' =>
                            'document.location = "' . Url::to(['content/create']) .
                            '?category_id=' . Yii::$app->request->get('category_id') .
                            $redirect.Yii::$app->request->get('redirect') .
                            '&type="+this.value' ,
                    ]
                );
            }


            // Выводим остальные поля циклом
            if (!empty(Yii::$app->request->get('type')) || !empty($meta) || !empty($pencil) || !empty($slider)) {

                echo $this->render('_form', [
                    'form' => $form,
                    'model' => $model,
                    'mode' => 'create',
                    'meta' => !empty($meta) ? $meta : null,
                    'pencil' => !empty($pencil) ? $pencil : null,
                    'slider' => !empty($slider) ? $slider : null,
                ]);

            }
        } ?>
        <?php ActiveForm::end(); ?>

    </div>