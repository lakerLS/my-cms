<?php

use app\classes\WidHelper;
use app\widgets\modalBody;
use yii\bootstrap\Html;

?>

<? modalBody::update($model, $model->category) ?>

<!-- blogpost start -->
                <article class="blogpost">
                    <div class="row grid-space-10">
                        <div class="col-lg-6">
                            <div class="overlay-container">
                                <?= WidHelper::image($model) ?>
                                <?= Html::a('<i class="fa fa-link"></i>', ['content/one', 'model' => $model], ['class' => 'overlay-link']) ?>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <header>
                                <h2>
                                    <?= Html::a($model->name, ['content/one', 'model' => $model]) ?>
                                    <? WidHelper::updateDelete($model) ?>
                                </h2>
                                <div class="post-info">
                        <span class="post-date">
                          <i class="icon-calendar"></i>
                            <?= Yii::$app->formatter->asDate($model->date) ?>
                        </span>
                                    <span class="submitted"><i class="icon-user-1"></i><a href="#"><?= $model->user->username ?></a></span>
                                    <span class="comments"><i class="icon-chat"></i> <a href="#"><? WidHelper::commentCount($model) ?></a></span>
                                </div>
                            </header>
                            <div class="blogpost-content">

                                <? WidHelper::text($model, 30) ?>
                            </div>
                        </div>
                    </div>
                    <footer class="clearfix">
                        <div class="tags pull-left"></div>
                    </footer>
                </article>
                <!-- blogpost end -->




