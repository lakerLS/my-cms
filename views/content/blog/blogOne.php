<?php

use app\classes\WidHelper;
use app\widgets\body;
use dektrium\user\widgets\Connect;
use yii2mod\comments\widgets\Comment;

$this->title = $model->name;
Yii::$app->view->params['modelOne'] = $model;
Yii::$app->view->params['meta'] = $meta;
?>

<!-- main-container start -->
<!-- ================ -->
<section class="main-container">

    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-lg-8">

                <!-- blogpost start -->
                <!-- ================ -->
                <article class="blogpost full">
                    <div class="blogpost-content" style="margin-bottom: 50px">
                        <div class="overlay-container">
                            <?= WidHelper::image($model) ?>
                        </div>

                        <? WidHelper::alertError('notUnique', 'Запись с указанным адресом в данной категории уже существует') ?>

                        <h3 class="my-4">
                            <?= $model->name ?>
                            <? WidHelper::updateDelete($model) ?>
                        </h3>

                        <header>
                            <div class="post-info mb-4">
                                <span class="post-date"><i class="icon-calendar"></i> <?= Yii::$app->formatter->asDate($model->date) ?></span>
                                <span class="submitted"><i class="icon-user-1"></i> <a href="#"><?= $model->user->username ?></a></span>
                                <span class="comments"><i class="icon-chat"></i> <a href="#"><? WidHelper::commentCount($model) ?></a></span>
                            </div>
                        </header>

                        <?= $model->text ?>
                    </div>

                    <?= Comment::widget(['model' => $model, 'commentView' => '@app/modules/comments/views/index']) ?>

                    <footer class="clearfix">
                        <div class="tags pull-left"><i class="icon-tags"></i> <a href="#">tag 1</a>, <a href="#">tag 2</a>, <a href="#">long tag 3</a></div>
                        <div class="link pull-right">
                            <?= Connect::widget(['baseAuthUrl' => ['/user/security/auth']]) ?>
                        </div>
                    </footer>
                </article>
                <!-- blogpost end -->

            </div>
            <!-- main end -->

            <!-- sidebar start -->
            <!-- ================ -->
            <aside class="col-lg-4 col-xl-3 ml-xl-auto">
                <div class="sidebar">
                    <div class="block clearfix">
                        <h3 class="title">Sidebar menu</h3>
                        <div class="separator-2"></div>
                        <nav>
                            <ul class="nav flex-column">
                                <li class="nav-item"><a class="nav-link" href="index.html">Home</a></li>
                                <li class="nav-item"><a class="nav-link active" href="blog-large-image-right-sidebar.html">Blog</a></li>
                                <li class="nav-item"><a class="nav-link" href="portfolio-grid-2-3-col.html">Portfolio</a></li>
                                <li class="nav-item"><a class="nav-link" href="page-about.html">About</a></li>
                                <li class="nav-item"><a class="nav-link" href="page-contact.html">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="block clearfix">
                        <h3 class="title">Featured Project</h3>
                        <div class="separator-2"></div>
                        <div id="carousel-portfolio-sidebar" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-portfolio-sidebar" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-portfolio-sidebar" data-slide-to="1"></li>
                                <li data-target="#carousel-portfolio-sidebar" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <div class="image-box shadow bordered text-center mb-20">
                                        <div class="overlay-container">
                                            <img src="/images/portfolio-4.jpg" alt="">
                                            <a href="portfolio-item.html" class="overlay-link">
                                                <i class="fa fa-link"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="image-box shadow bordered text-center mb-20">
                                        <div class="overlay-container">
                                            <img src="/images/portfolio-1-2.jpg" alt="">
                                            <a href="portfolio-item.html" class="overlay-link">
                                                <i class="fa fa-link"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="image-box shadow bordered text-center mb-20">
                                        <div class="overlay-container">
                                            <img src="/images/portfolio-1-3.jpg" alt="">
                                            <a href="portfolio-item.html" class="overlay-link">
                                                <i class="fa fa-link"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block clearfix">
                        <h3 class="title">Latest tweets</h3>
                        <div class="separator-2"></div>
                        <ul class="tweets">
                            <li>
                                <i class="fa fa-twitter"></i>
                                <p><a href="#">@lorem</a> ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, aliquid, et molestias nesciunt <a href="#">http://t.co/dzLEYGeEH9</a>.</p><span>16 hours ago</span>
                            </li>
                            <li>
                                <i class="fa fa-twitter"></i>
                                <p><a href="#">@lorem</a> ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, aliquid, et molestias nesciunt <a href="#">http://t.co/dzLEYGeEH9</a>.</p><span>16 hours ago</span>
                            </li>
                        </ul>
                    </div>
                    <div class="block clearfix">
                        <h3 class="title">Popular Tags</h3>
                        <div class="separator-2"></div>
                        <div class="tags-cloud">
                            <div class="tag">
                                <a href="#">energy</a>
                            </div>
                            <div class="tag">
                                <a href="#">business</a>
                            </div>
                            <div class="tag">
                                <a href="#">food</a>
                            </div>
                            <div class="tag">
                                <a href="#">fashion</a>
                            </div>
                            <div class="tag">
                                <a href="#">finance</a>
                            </div>
                            <div class="tag">
                                <a href="#">culture</a>
                            </div>
                            <div class="tag">
                                <a href="#">health</a>
                            </div>
                            <div class="tag">
                                <a href="#">sports</a>
                            </div>
                            <div class="tag">
                                <a href="#">life style</a>
                            </div>
                            <div class="tag">
                                <a href="#">books</a>
                            </div>
                            <div class="tag">
                                <a href="#">lorem</a>
                            </div>
                            <div class="tag">
                                <a href="#">ipsum</a>
                            </div>
                            <div class="tag">
                                <a href="#">responsive</a>
                            </div>
                            <div class="tag">
                                <a href="#">style</a>
                            </div>
                            <div class="tag">
                                <a href="#">finance</a>
                            </div>
                            <div class="tag">
                                <a href="#">sports</a>
                            </div>
                            <div class="tag">
                                <a href="#">technology</a>
                            </div>
                            <div class="tag">
                                <a href="#">support</a>
                            </div>
                            <div class="tag">
                                <a href="#">life style</a>
                            </div>
                            <div class="tag">
                                <a href="#">books</a>
                            </div>
                        </div>
                    </div>
                    <div class="block clearfix">
                        <h3 class="title">Testimonial</h3>
                        <div class="separator-2"></div>
                        <blockquote class="margin-clear">
                            <p>Design is not just what it looks like and feels like. Design is how it works.</p>
                            <footer><cite title="Source Title">Steve Jobs </cite></footer>
                        </blockquote>
                        <blockquote class="margin-clear">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos dolorem.</p>
                            <footer><cite title="Source Title">Steve Doe </cite></footer>
                        </blockquote>
                    </div>
                    <div class="block clearfix">
                        <h3 class="title">Latest News</h3>
                        <div class="separator-2"></div>
                        <div class="media margin-clear">
                            <div class="d-flex pr-2">
                                <div class="overlay-container">
                                    <img class="media-object" src="/images/blog-thumb-1.jpg" alt="blog-thumb">
                                    <a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                            <div class="media-body">
                                <h6 class="media-heading"><a href="blog-post.html">Lorem ipsum dolor sit amet...</a></h6>
                                <p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 23, 2017</p>
                            </div>
                        </div>
                        <hr>
                        <div class="media margin-clear">
                            <div class="d-flex pr-2">
                                <div class="overlay-container">
                                    <img class="media-object" src="/images/blog-thumb-2.jpg" alt="blog-thumb">
                                    <a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                            <div class="media-body">
                                <h6 class="media-heading"><a href="blog-post.html">Lorem ipsum dolor sit amet...</a></h6>
                                <p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 22, 2017</p>
                            </div>
                        </div>
                        <hr>
                        <div class="media margin-clear">
                            <div class="d-flex pr-2">
                                <div class="overlay-container">
                                    <img class="media-object" src="/images/blog-thumb-3.jpg" alt="blog-thumb">
                                    <a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                            <div class="media-body">
                                <h6 class="media-heading"><a href="blog-post.html">Lorem ipsum dolor sit amet...</a></h6>
                                <p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 21, 2017</p>
                            </div>
                        </div>
                        <hr>
                        <div class="media margin-clear">
                            <div class="d-flex pr-2">
                                <div class="overlay-container">
                                    <img class="media-object" src="/images/blog-thumb-4.jpg" alt="blog-thumb">
                                    <a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                            <div class="media-body">
                                <h6 class="media-heading"><a href="blog-post.html">Lorem ipsum dolor sit amet...</a></h6>
                                <p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 21, 2017</p>
                            </div>
                        </div>
                        <div class="text-right space-top">
                            <a href="blog-large-image-right-sidebar.html" class="link-dark"><i class="fa fa-plus-circle pl-1 pr-1"></i>More</a>
                        </div>
                    </div>
                    <div class="block clearfix">
                        <h3 class="title">Text Sample</h3>
                        <div class="separator-2"></div>
                        <p class="margin-clear">Debitis eaque officia illo impedit ipsa earum <a href="#">cupiditate repellendus</a> corrupti nisi nemo, perspiciatis optio harum, hic laudantium nulla maiores rem sit magni neque nihil sequi temporibus. Laboriosam ipsum reiciendis iste, nobis obcaecati, a autem voluptatum odio? Recusandae officiis dicta quod qui eligendi.</p>
                    </div>
                    <div class="block clearfix">
                        <form role="search">
                            <div class="form-group has-feedback">
                                <input type="text" class="form-control" placeholder="Search">
                                <i class="fa fa-search form-control-feedback"></i>
                            </div>
                        </form>
                    </div>
                </div>
            </aside>
            <!-- sidebar end -->

        </div>
    </div>
</section>
<!-- main-container end -->