<?php

use app\classes\WidHelper;
use app\widgets\modalBody;
use yii\widgets\ListView;

/**
 * @var $this yii\web\View
 * @var $meta
 * @var $pages
 */
$this->title = $meta->name;
Yii::$app->view->params['modelMany'] = $model;
Yii::$app->view->params['meta'] = $meta;
?>

<!-- main-container start -->
<!-- ================ -->
<section class="main-container">

    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-lg-8">
                <div style="margin-bottom: 30px">
                    <? modalBody::pencil('head', $meta) ?>
                </div>

                <!-- page-title start -->
                <!-- ================ -->
                <h1 class="page-title"><?= $meta->name ?></h1>

                <? WidHelper::alertError('notUnique', 'Запись с указанным адресом в данной категории уже существует') ?>
                <hr />
                <!-- page-title end -->

                <?php
                // echo '<div class="read" style="text-align: right"><b>Сортировать по:</b> &nbsp&nbsp' . $sort->link('name') . ' &nbsp;&nbsp;&nbsp;&nbsp;' . $sort->link('id') . '</div></br>';

                echo ListView::widget([
                    'dataProvider' => $dataProvider,

                    'layout' => "{items}",

                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'options' => [
                        'tag' => 'article',
                        'class' => 'post',
                        // 'id' => 'list-wrapper',
                    ],
                    'itemView' => '_blog',

                    'viewParams' => [
                        'meta' => $meta,
                    ],
                ]);

                ?>

                <?= \yii\widgets\LinkPager::widget([
                    'pagination' => $pages,
                ]);
                ?>



            </div>
            <!-- main end -->

            <!-- sidebar start -->
            <!-- ================ -->
            <aside class="col-lg-4 col-xl-3 ml-xl-auto">
                <div class="sidebar">
                    <div class="block clearfix">
                        <h3 class="title">Sidebar menu</h3>
                        <div class="separator-2"></div>
                        <nav>
                            <ul class="nav flex-column">
                                <li class="nav-item"><a class="nav-link" href="index.html">Home</a></li>
                                <li class="nav-item"><a class="nav-link active" href="blog-large-image-right-sidebar.html">Blog</a></li>
                                <li class="nav-item"><a class="nav-link" href="portfolio-grid-2-3-col.html">Portfolio</a></li>
                                <li class="nav-item"><a class="nav-link" href="page-about.html">About</a></li>
                                <li class="nav-item"><a class="nav-link" href="page-contact.html">Contact</a></li>
                            </ul>
                        </nav>
                    </div>

                    <div class="block clearfix">
                        <h3 class="title">Latest tweets</h3>
                        <div class="separator-2"></div>
                        <ul class="tweets">
                            <li>
                                <i class="fa fa-twitter"></i>
                                <p><a href="#">@lorem</a> ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, aliquid, et molestias nesciunt <a href="#">http://t.co/dzLEYGeEH9</a>.</p><span>16 hours ago</span>
                            </li>
                            <li>
                                <i class="fa fa-twitter"></i>
                                <p><a href="#">@lorem</a> ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, aliquid, et molestias nesciunt <a href="#">http://t.co/dzLEYGeEH9</a>.</p><span>16 hours ago</span>
                            </li>
                        </ul>
                    </div>
                    <div class="block clearfix">
                        <h3 class="title">Popular Tags</h3>
                        <div class="separator-2"></div>
                        <div class="tags-cloud">
                            <div class="tag">
                                <a href="#">energy</a>
                            </div>
                            <div class="tag">
                                <a href="#">business</a>
                            </div>
                            <div class="tag">
                                <a href="#">food</a>
                            </div>
                            <div class="tag">
                                <a href="#">fashion</a>
                            </div>
                            <div class="tag">
                                <a href="#">finance</a>
                            </div>
                            <div class="tag">
                                <a href="#">culture</a>
                            </div>
                            <div class="tag">
                                <a href="#">health</a>
                            </div>
                            <div class="tag">
                                <a href="#">sports</a>
                            </div>
                            <div class="tag">
                                <a href="#">life style</a>
                            </div>
                            <div class="tag">
                                <a href="#">books</a>
                            </div>
                            <div class="tag">
                                <a href="#">lorem</a>
                            </div>
                            <div class="tag">
                                <a href="#">ipsum</a>
                            </div>
                            <div class="tag">
                                <a href="#">responsive</a>
                            </div>
                            <div class="tag">
                                <a href="#">style</a>
                            </div>
                            <div class="tag">
                                <a href="#">finance</a>
                            </div>
                            <div class="tag">
                                <a href="#">sports</a>
                            </div>
                            <div class="tag">
                                <a href="#">technology</a>
                            </div>
                            <div class="tag">
                                <a href="#">support</a>
                            </div>
                            <div class="tag">
                                <a href="#">life style</a>
                            </div>
                            <div class="tag">
                                <a href="#">books</a>
                            </div>
                        </div>
                    </div>
                    <div class="block clearfix">
                        <h3 class="title">Testimonial</h3>
                        <div class="separator-2"></div>
                        <blockquote class="margin-clear">
                            <p>Design is not just what it looks like and feels like. Design is how it works.</p>
                            <footer><cite title="Source Title">Steve Jobs </cite></footer>
                        </blockquote>
                        <blockquote class="margin-clear">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos dolorem.</p>
                            <footer><cite title="Source Title">Steve Doe </cite></footer>
                        </blockquote>
                    </div>
                    <div class="block clearfix">
                        <h3 class="title">Latest News</h3>
                        <div class="separator-2"></div>
                        <div class="media margin-clear">
                            <div class="d-flex pr-2">
                                <div class="overlay-container">
                                    <img class="media-object" src="images/blog-thumb-1.jpg" alt="blog-thumb">
                                    <a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                            <div class="media-body">
                                <h6 class="media-heading"><a href="blog-post.html">Lorem ipsum dolor sit amet...</a></h6>
                                <p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 23, 2017</p>
                            </div>
                        </div>
                        <hr>
                        <div class="media margin-clear">
                            <div class="d-flex pr-2">
                                <div class="overlay-container">
                                    <img class="media-object" src="images/blog-thumb-2.jpg" alt="blog-thumb">
                                    <a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                            <div class="media-body">
                                <h6 class="media-heading"><a href="blog-post.html">Lorem ipsum dolor sit amet...</a></h6>
                                <p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 22, 2017</p>
                            </div>
                        </div>
                        <hr>
                        <div class="media margin-clear">
                            <div class="d-flex pr-2">
                                <div class="overlay-container">
                                    <img class="media-object" src="images/blog-thumb-3.jpg" alt="blog-thumb">
                                    <a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                            <div class="media-body">
                                <h6 class="media-heading"><a href="blog-post.html">Lorem ipsum dolor sit amet...</a></h6>
                                <p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 21, 2017</p>
                            </div>
                        </div>
                        <hr>
                        <div class="media margin-clear">
                            <div class="d-flex pr-2">
                                <div class="overlay-container">
                                    <img class="media-object" src="images/blog-thumb-4.jpg" alt="blog-thumb">
                                    <a href="blog-post.html" class="overlay-link small"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                            <div class="media-body">
                                <h6 class="media-heading"><a href="blog-post.html">Lorem ipsum dolor sit amet...</a></h6>
                                <p class="small margin-clear"><i class="fa fa-calendar pr-10"></i>Mar 21, 2017</p>
                            </div>
                        </div>
                        <div class="text-right space-top">
                            <a href="blog-large-image-right-sidebar.html" class="link-dark"><i class="fa fa-plus-circle pl-1 pr-1"></i>More</a>
                        </div>
                    </div>
                    <div class="block clearfix">
                        <h3 class="title">Text Sample</h3>
                        <div class="separator-2"></div>
                        <p class="margin-clear">Debitis eaque officia illo impedit ipsa earum <a href="#">cupiditate repellendus</a> corrupti nisi nemo, perspiciatis optio harum, hic laudantium nulla maiores rem sit magni neque nihil sequi temporibus. Laboriosam ipsum reiciendis iste, nobis obcaecati, a autem voluptatum odio? Recusandae officiis dicta quod qui eligendi.</p>
                    </div>
                    <div class="block clearfix">
                        <form role="search">
                            <div class="form-group has-feedback">
                                <input type="text" class="form-control" placeholder="Search">
                                <i class="fa fa-search form-control-feedback"></i>
                            </div>
                        </form>
                    </div>
                </div>
            </aside>
            <!-- sidebar end -->

        </div>
    </div>
</section>
<!-- main-container end -->

Отображать по:
<?//= Html::a('10', Url::current(['per-page' => '10']));?><!--,-->
<?//= Html::a('25', Url::current(['per-page' => '25']));?><!--,-->
<?//= Html::a('50', Url::current(['per-page' => '50']));?>
<?//= Html::a('100', Url::current(['per-page' => '100']));?>
