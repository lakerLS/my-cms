<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\modules\user\models\User;
use dektrium\user\widgets\Connect;
use app\classes\myForms;
use app\models\CompanyDetails;
use app\widgets\megaMenu\breadcrumbs;
use app\widgets\modalBody;
use dektrium\user\models\LoginForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use app\assets\Site;
use app\widgets\megaMenu\megaMenu;

Site::register($this);

$info = CompanyDetails::find()->one();
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<!--[if IE 9]> <html lang="zxx" class="ie9"> <![endif]-->
<!--[if gt IE 9]> <html lang="zxx" class="ie"> <![endif]-->
<!--[if !IE]><!-->
<html dir="ltr" lang="zxx">
<!--<![endif]-->
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta charset="utf-8">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
    <meta name="author" content="htmlcoder.me">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/favicon.ico">

    <!-- Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>


</head>

<!-- body classes:  -->
<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
<!-- "gradient-background-header": applies gradient background to header -->
<!-- "page-loader-1 ... page-loader-6": add a page loader to the page (more info @components-page-loaders.html) -->
<body class="transparent-header front-page ">

    <?php $this->beginBody() ?>

    <!-- scrollToTop -->
    <!-- ================ -->
    <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>

    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">
        <!-- header-container start -->
        <div class="header-container">
            <!-- header-top start -->
            <!-- classes:  -->
            <!-- "dark": dark version of header top e.g. class="header-top dark" -->
            <!-- "colored": colored version of header top e.g. class="header-top colored" -->
            <!-- ================ -->
            <div class="header-top dark">
                <div class="container">
                    <div class="row">
                        <div class="col-3 col-sm-6 col-lg-9">
                            <!-- header-top-first start -->
                            <!-- ================ -->
                            <div class="header-top-first clearfix">
                                <ul class="social-links circle small clearfix hidden-sm-down">
                                    <li class="twitter"><a target="_blank" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
                                    <li class="skype"><a target="_blank" href="http://www.skype.com"><i class="fa fa-skype"></i></a></li>
                                    <li class="linkedin"><a target="_blank" href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
                                    <li class="googleplus"><a target="_blank" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a></li>
                                    <li class="youtube"><a target="_blank" href="http://www.youtube.com"><i class="fa fa-youtube-play"></i></a></li>
                                    <li class="flickr"><a target="_blank" href="http://www.flickr.com"><i class="fa fa-flickr"></i></a></li>
                                    <li class="facebook"><a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                                    <li class="pinterest"><a target="_blank" href="http://www.pinterest.com"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                                <div class="social-links hidden-md-up circle small">
                                    <div class="btn-group dropdown">
                                        <button id="header-top-drop-1" type="button" class="btn dropdown-toggle dropdown-toggle--no-caret" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-share-alt"></i></button>
                                        <ul class="dropdown-menu dropdown-animation" aria-labelledby="header-top-drop-1">
                                            <li class="twitter"><a target="_blank" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
                                            <li class="skype"><a target="_blank" href="http://www.skype.com"><i class="fa fa-skype"></i></a></li>
                                            <li class="linkedin"><a target="_blank" href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
                                            <li class="googleplus"><a target="_blank" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a></li>
                                            <li class="youtube"><a target="_blank" href="http://www.youtube.com"><i class="fa fa-youtube-play"></i></a></li>
                                            <li class="flickr"><a target="_blank" href="http://www.flickr.com"><i class="fa fa-flickr"></i></a></li>
                                            <li class="facebook"><a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                                            <li class="pinterest"><a target="_blank" href="http://www.pinterest.com"><i class="fa fa-pinterest"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <ul class="list-inline hidden-md-down">
                                    <li class="list-inline-item"><i class="fa fa-map-marker pr-1 pl-2"></i><?= $info->address; ?></li>
                                    <li class="list-inline-item"><i class="fa fa-phone pr-1 pl-2"></i><?= $info->phone_one ?></li>
                                    <li class="list-inline-item"><i class="fa fa-envelope-o pr-1 pl-2"></i> <?= $info->email_one ?></li>
                                </ul>
                            </div>
                            <!-- header-top-first end -->
                        </div>
                        <div class="col-9 col-sm-6 col-lg-3">

                            <!-- header-top-second start -->
                            <!-- ================ -->
                            <div id="header-top-second"  class="clearfix">

                                <!-- header top dropdowns start -->
                                <!-- ================ -->
                                <div class="header-top-dropdown text-right">


                                        <? if (Yii::$app->user->isGuest) { ?>
                                        <!--Для неавторизованных -->
                                    <div class="btn-group">
                                        <a href="/registration" class="btn btn-dark btn-sm"><i class="fa fa-lock pr-2"></i> Регистрация</a>
                                    </div>
                                    <div class="btn-group">
                                        <button id="header-top-drop-2" type="button" class="btn dropdown-toggle btn-dark btn-sm dropdown-toggle--no-caret" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user pr-2"></i> Вход</button>
                                        <ul class="dropdown-menu dropdown-menu-right dropdown-animation" aria-labelledby="header-top-drop-2">
                                            <li>
                                                <? $form = ActiveForm::begin(['action' => '/user/login']); ?>

                                                <? $login = \Yii::createObject(LoginForm::className()); ?>

                                                <?= $form->field($login, 'login')->textInput(['autofocus' => 'autofocus']) ?>

                                                <?= $form->field($login, 'password')->passwordInput()->label('Пароль (<a href="/user/forgot" tabindex="5">Забыли пароль?</a>)') ?>

                                                <?= Html::hiddenInput('redirect', Yii::$app->request->pathInfo) ?>

                                                <?= Html::submitButton('Авторизоваться', ['class' => 'btn btn-primary btn-block']) ?>

                                                <? ActiveForm::end() ?>

                                                <?= Connect::widget(['baseAuthUrl' => ['/user/security/auth']]) ?>


                                            </li>
                                        </ul>
                                    </div>

                                    <? }else { ?>

                                        <!--Для авторизованных -->
                                        <?php  ActiveForm::begin(['options' => ['class' => 'btn btn-dark btn-sm']]); ?>

                                        <?= Html::a('<i class="fa fa-lock pr-2"></i> Выход', ['/user/security/logout'], ['data-method' => 'post','style'=>'color: #ffffff;text-decoration: none;']) ?>

                                        <?= Html::hiddenInput('redirect', Yii::$app->request->pathInfo) ?>

                                        <?php ActiveForm::end(); ?>

                                        <?
                                        if (Yii::$app->user->can('admin'))
                                            echo Html::a('<i class="fa fa-user pr-2"></i>'.Yii::$app->user->identity->username, '/category?active=3', ['class' => 'btn btn-dark btn-sm']);

                                        else
                                            echo Html::a('<i class="fa fa-user pr-2"></i>'.Yii::$app->user->identity->username, '/user/settings/profile', ['class' => 'btn btn-dark btn-sm']);
                                    }?>

                                </div>
                                <!--  header top dropdowns end -->
                            </div>
                            <!-- header-top-second end -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- header-top end -->

            <!-- header start -->
            <!-- classes:  -->
            <!-- "fixed": enables fixed navigation mode (sticky menu) e.g. class="header fixed clearfix" -->
            <!-- "fixed-desktop": enables fixed navigation only for desktop devices e.g. class="header fixed fixed-desktop clearfix" -->
            <!-- "fixed-all": enables fixed navigation only for all devices desktop and mobile e.g. class="header fixed fixed-desktop clearfix" -->
            <!-- "dark": dark version of header e.g. class="header dark clearfix" -->
            <!-- "centered": mandatory class for the centered logo layout -->
            <!-- ================ -->
            <header class="header fixed fixed-desktop clearfix">
                <div class="container">
                    <div class="row">
                        <div class="col-md-auto hidden-md-down">
                            <!-- header-first start -->
                            <!-- ================ -->
                            <div class="header-first clearfix">

                                <!-- logo -->
                                <div id="logo" class="logo">
                                    <a href="index.html"><img id="logo_img" src="/template/images/logo_light_blue.png" alt="The Project"></a>
                                </div>

                                <!-- name-and-slogan -->
                                <div class="site-slogan">
                                    <? modalBody::pencil('layout-test') ?>
                                </div>

                            </div>
                            <!-- header-first end -->

                        </div>
                        <div class="col-lg-10 ml-lg-auto">

                            <!-- header-second start -->
                            <!-- ================ -->
                            <div class="header-second clearfix">

                                <!-- main-navigation start -->
                                <!-- classes: -->
                                <!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
                                <!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
                                <!-- ================ -->
                                <div class="main-navigation main-navigation--mega-menu  animated">
                                    <nav class="navbar navbar-expand-lg navbar-light p-0">
                                        <div class="navbar-brand clearfix hidden-lg-up">

                                            <!-- logo -->
                                            <div id="logo-mobile" class="logo">
                                                <a href="index.html"><img id="logo-img-mobile" src="/template/images/logo_light_blue.png" alt="The Project"></a>
                                            </div>

                                            <!-- name-and-slogan -->
                                            <div class="site-slogan">
                                                Multipurpose HTML5 Template
                                            </div>

                                        </div>

                                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-1" aria-controls="navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
                                            <span class="navbar-toggler-icon"></span>
                                        </button>

                                        <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                            <!-- main-menu -->
                                            <? megaMenu::body() ?>
                                            <!-- main-menu end -->
                                        </div>
                                    </nav>
                                </div>
                                <!-- main-navigation end -->
                            </div>
                            <!-- header-second end -->

                        </div>
                    </div>
                </div>
            </header>
            <!-- header end -->
        </div>
        <!-- header-container end -->

        <div style="margin-bottom: 81px"></div>

        <!-- breadcrumb start -->
        <!-- ================ -->

        <? if (!empty(Yii::$app->view->params['meta'])) { ?>
            <div class="breadcrumb-container">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a href="/">Главная</a></li>
                        <?= breadcrumbs::body(Yii::$app->view->params['meta'],
                            !empty(Yii::$app->view->params['modelOne']) ? Yii::$app->view->params['modelOne'] : null) ?>
                    </ol>
                </div>
            </div>
        <? } ?>

        <!-- breadcrumb end -->


        <?= $content ?>


        <? // Модальные окна (Many - создание записи, One - редактирование записи)
        if (!empty(Yii::$app->view->params['modelMany']) && !empty(Yii::$app->view->params['meta'])) {

            // необходимо для вывода модального окна создания записи, а так же кнопки "добавить запись"
            modalBody::create(Yii::$app->view->params['modelMany'], Yii::$app->view->params['meta']);
        }
        elseif (!empty(Yii::$app->view->params['modelOne']) && !empty(Yii::$app->view->params['meta'])) {

            // необходимо для вывода модального окна, дополнительной кнопки "редактировать" не появляется
            modalBody::update(Yii::$app->view->params['modelOne'], Yii::$app->view->params['meta']);
        }
        ?>

        <!-- footer top start -->
        <!-- ================ -->
        <div class="dark-translucent-bg footer-top animated-text default-hovered" style="background-color:rgba(0,0,0,0.6);">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="call-to-action text-center">
                            <div class="row">
                                <div class="col-md-8">
                                    <h2 class="mt-4">Powerful Bootstrap Template</h2>
                                    <h2 class="mt-4">Waste no more time</h2>
                                </div>
                                <div class="col-md-4">
                                    <p class="mt-3"><a href="#" class="btn btn-animated btn-lg btn-gray-transparent">Purchase<i class="fa fa-cart-arrow-down pl-20"></i></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer top end -->
        <!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
        <!-- ================ -->
        <footer id="footer" class="clearfix dark">

            <!-- .footer start -->
            <!-- ================ -->
            <div class="footer">
                <div class="container">
                    <div class="footer-inner">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="footer-content">
                                    <div class="logo-footer"><img id="logo-footer" src="/template/images/logo_light_blue.png" alt=""></div>
                                    <p><? modalBody::pencil('layout-about_us') ?></p>
                                    <ul class="list-inline mb-20">
                                        <li class="list-inline-item"><i class="text-default fa fa-map-marker pr-1"></i> <?= $info->address ?></li>
                                        <li class="list-inline-item"><i class="text-default fa fa-phone pl-10 pr-1"></i> <?= $info->phone_one ?></li>
                                        <li class="list-inline-item"><a href="mailto:info@theproject.com" class="link-dark"><i class="text-default fa fa-envelope-o pl-10 pr-1"></i> <?= $info->email_one ?></a></li>
                                    </ul>
                                    <div class="separator-2"></div>
                                    <ul class="social-links circle margin-clear animated-effect-1">
                                        <li class="facebook"><a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                                        <li class="twitter"><a target="_blank" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
                                        <li class="googleplus"><a target="_blank" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a></li>
                                        <li class="linkedin"><a target="_blank" href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
                                        <li class="xing"><a target="_blank" href="http://www.xing.com"><i class="fa fa-xing"></i></a></li>
                                        <li class="skype"><a target="_blank" href="http://www.skype.com"><i class="fa fa-skype"></i></a></li>
                                        <li class="youtube"><a target="_blank" href="https://www.youtube.com"><i class="fa fa-youtube"></i></a></li>
                                        <li class="dribbble"><a target="_blank" href="https://dribbble.com/"><i class="fa fa-dribbble"></i></a></li>
                                        <li class="pinterest"><a target="_blank" href="http://www.pinterest.com"><i class="fa fa-pinterest"></i></a></li>
                                        <li class="flickr"><a target="_blank" href="http://www.flickr.com"><i class="fa fa-flickr"></i></a></li>
                                        <li class="instagram"><a target="_blank" href="http://www.instagram.com"><i class="fa fa-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <h2 class="title">Свяжитесь с нами</h2>
                                <? myForms::FooterContacts() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .footer end -->

            <!-- .subfooter start -->
            <!-- ================ -->
            <div class="subfooter">
                <div class="container">
                    <div class="subfooter-inner">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-center">

                                    <? myForms::adminka(); ?>

                                    <? modalBody::login(); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .subfooter end -->

        </footer>
        <!-- footer end -->
    </div>
    <!-- page-wrapper end -->

    <!-- JavaScript files placed at the end of the document so the pages load faster -->
    <!-- ================================================== -->
    <?php $this->endBody() ?>

</body>
</html>

<?php $this->endPage() ?>