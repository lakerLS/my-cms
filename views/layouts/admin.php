<?php


use app\assets\Admin;
use app\assets\AdminStart;
use app\widgets\body;
use app\widgets\myMenu;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

Admin::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta charset="utf-8">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>


    <!-- mobile settings -->
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <!-- WEB FONTS : use %7C instead of | (pipe) -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />


</head>

<body>
<?php $this->beginBody() ?>

<!-- WRAPPER -->
<div id="wrapper">

    <!--
        ASIDE
        Keep it outside of #wrapper (responsive purpose)
    -->
    <aside id="aside">
        <!--
            Always open:
            <li class="active alays-open">

            LABELS:
                <span class="label label-danger pull-right">1</span>
                <span class="label label-default pull-right">1</span>
                <span class="label label-warning pull-right">1</span>
                <span class="label label-success pull-right">1</span>
                <span class="label label-info pull-right">1</span>
        -->
        <nav id="sideNav"><!-- MAIN MENU -->
            <ul class="nav nav-list">

                <li class="<?= Yii::$app->request->get('active') == 1 ? 'active' : null ?>">
                    <a href="#">
                        <i class="fa fa-menu-arrow pull-right"></i>
                        <i class="main-icon fa fa-book"></i> <span>Профиль</span>
                    </a>

                    <?= myMenu::sidebar([
                        ['label' => 'Ваш аккаунт', 'url' => '/account', 'active' => 'user/settings/account?active=1'],
                        ['label' => 'Ваш профиль', 'url' => '/user/settings/profile?active=1'],
                    ]) ?>
                </li>
                <li class="<?= Yii::$app->request->get('active') == 2 ? 'active' : null ?>">
                    <a href="#">
                        <i class="fa fa-menu-arrow pull-right"></i>
                        <i class="main-icon fa fa-link"></i> <span>Управление доступом</span>
                    </a>

                    <?= myMenu::sidebar([
                        ['label' => 'Пользователи', 'url' => '/user/admin/index?active=2'],
                        ['label' => 'Роли', 'url' => '/permit/access/role?active=2'],
                        ['label' => 'Правила доступа', 'url' => '/permit/access/permission?active=2'],
                    ]) ?>
                </li>
                <li class="<?= Yii::$app->request->get('active') == 3 ? 'active' : null ?>">
                    <a href="#">
                        <i class="fa fa-menu-arrow pull-right"></i>
                        <i class="main-icon fa fa-pencil-square-o"></i> <span>Наполнение сайта</span>
                    </a>

                    <?= myMenu::sidebar([
                        ['label' => 'Меню и содержимое', 'url' => '/category?active=3'],
                        ['label' => 'Информация о компании', 'url' => '/company-details/update?id=5&active=3'],
                        ['label' => 'Ответы на вопросы', 'url' => '/faq?active=3', 'notice' => ['Faq', ['text' => ' '], true, 'Новый вопрос']],
                        ['label' => 'Обработка заказов', 'url' => '/order?active=3', 'notice' => ['Order', ['closed' => '0'], true, 'Необработанная услуга']],
                        ['label' => 'Разделы', 'url' => '/sections?active=3'],
                    ]) ?>
                </li>
                <li class="<?= Yii::$app->request->get('active') == 4 ? 'active' : null ?>">
                    <a href="#">
                        <i class="fa fa-menu-arrow pull-right"></i>
                        <i class="main-icon fa fa-gears"></i> <span>Разработчику</span>
                    </a>

                    <?= myMenu::sidebar([
                        ['label' => 'Типы содержимого', 'url' => '/type?active=4'],
                    ]) ?>
                </li>

            </ul>
            <ul class="nav nav-list">
                <li class="active">
                    <a href="/">
                        <i class="main-icon fa fa-reply"></i> <span>На главную</span>
                    </a>
                </li>
            </ul>

        </nav>

        <span id="asidebg"><!-- aside fixed background --></span>
    </aside>
    <!-- /ASIDE -->


    <!-- HEADER -->
    <header id="header">

        <!-- Mobile Button -->
        <button id="mobileMenuBtn"></button>

        <!-- Logo -->
        <span class="logo pull-left">
					<a href="/"><img src="/admin/assets/images/logo_light.png" alt="admin panel" height="35" /></a>
				</span>

        <form method="get" action="page-search.html" class="search pull-left hidden-xs">
            <input type="text" class="form-control" name="k" placeholder="Search for something..." />
        </form>

        <nav>

            <!-- OPTIONS LIST -->
            <ul class="nav pull-right">

                <!-- USER OPTIONS -->
                <li class="dropdown pull-left">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img class="user-avatar" alt="" src="/admin/assets/images/noavatar.jpg" height="34" />
                        <span class="user-name">
									<span class="hidden-xs">
										admin <i class="fa fa-angle-down"></i>
									</span>
								</span>
                    </a>
                    <ul class="dropdown-menu hold-on-click">
                        <li><!-- my calendar -->
                            <a href="calendar.html"><i class="fa fa-calendar"></i> Calendar</a>
                        </li>
                        <li><!-- my inbox -->
                            <a href="#"><i class="fa fa-envelope"></i> Inbox
                                <span class="pull-right label label-default">0</span>
                            </a>
                        </li>
                        <li><!-- settings -->
                            <a href="page-user-profile.html"><i class="fa fa-cogs"></i> Settings</a>
                        </li>

                        <li class="divider"></li>

                        <li><!-- lockscreen -->
                            <a href="page-lock.html"><i class="fa fa-lock"></i> Lock Screen</a>
                        </li>
                        <li><!-- logout -->
                            <a href="page-login.html"><i class="fa fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
                <!-- /USER OPTIONS -->

            </ul>
            <!-- /OPTIONS LIST -->

        </nav>

    </header>
    <!-- /HEADER -->


    <!--
        MIDDLE
    -->
    <section id="middle">

        <div id="content" class="padding-20">

    <?= $content ?>

        </div>
    </section>
    <!-- /MIDDLE -->

</div>

<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>