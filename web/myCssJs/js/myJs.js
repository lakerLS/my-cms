// Автозаполнение транслитом поля "url" из поля "name", при создании записи
function countCharContent(id) {
    $.ajax({
        type: "POST",
        url: "/translate.php",
        data: "category-name=" + $("#content-name-"+id).val(),
        success: function (html) {
            $("#content-url-"+id).val(html);
        }
    });
    return false;
}

// Автозаполнение транслитом поля "url" из поля "name", при создании категории
function countCharCategory() {
    $.ajax({
        type: "POST",
        url: "/translate.php",
        data: "category-name=" + $("#category-name").val(),
        success: function (html) {
            $("#category-url").val(html);
        }
    });
    return false;
}

// Вставляет в кнопку "фильтр по категориям" id выбранной категории
function category_filter()
{
    var margin = document.getElementById('category-id');
    var button = document.getElementById('button_filter');

    button.href = '?active=3&ContentSearch%5Bname%5D=&ContentSearch%5Bcategory_id%5D='+margin.value;
}

// Решение проблемы невозможности использовать input в CKEditor (который выводится в модальном окне)
$(document).on('shown.bs.modal', function() {
    $(document).off('focusin.modal');
});
$(document).on('focusin.modal', function (e) {
    $(document).off('focusin.modal');
});