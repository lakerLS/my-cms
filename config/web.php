<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'thumbnail'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],


    'layout' => 'site',
    'language' => 'ru-RU',

    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableConfirmation' => false,
            'enableUnconfirmedLogin' => true,
            'adminPermission' => 'admin',
            'controllerMap' => [
                'admin' => 'app\modules\user\controllers\AdminController',
                'security' => 'app\modules\user\controllers\SecurityController',
            ],
            'modelMap' => [
                'User' => 'app\modules\user\models\User',
            ],
        ],

        'permit' => [
            'class' => 'developeruz\db_rbac\Yii2DbRbac',
            'params' => [
                'userClass' => 'app\modules\rbac\models\User',
                'accessRoles' => ['admin']
            ],
            'controllerMap' => [
                'access' => 'app\modules\rbac\controllers\AccessController',
                'user' => 'app\modules\rbac\controllers\UserController',
            ],
        ],
        'treemanager' => [
            'class' => '\kartik\tree\Module',
        ],
        'comment' => [
            'class' => 'yii2mod\comments\Module',
            'controllerMap' => [
                'default' => 'app\modules\comments\controllers\DefaultController',
            ],
        ],
    ],
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\PathController',
            'access' => ['@'],
            'root' => [
                'path' => '/upload/global',
                'name' => 'global'
            ],
        ],
    ],

    'components' => [
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'vkontakte' => [
                    'class'        => 'dektrium\user\clients\VKontakte',
                    'clientId'     => '6767902',
                    'clientSecret' => '2eYMNAv8S1U1sshLFiQT',
                ],
                'yandex' => [
                    'class'        => 'dektrium\user\clients\Yandex',
                    'clientId'     => '1d32a10b96d449fd85f57b7824af9f2e',
                    'clientSecret' => '71a27e719f604dd29a076cc90231f15c'
                ],
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '744948389389-09hd3linfmsbj9ujhanhbbv15me5toa4.apps.googleusercontent.com',
                    'clientSecret' => 'S6E_pkpcoxiFl3BilVqSdYj0',
                ],
                // etc.
            ],
        ],

        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => ['/myCssJs/js/jquery/jquery.js']
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => []
                ],
            ],
        ],

        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views'       => '@app/modules/user/views',
                    '@developeruz/db_rbac/views' => '@app/modules/rbac/views',
                    //'@yii2mod/comments/views'    => '@app/modules/comments/views/index'
                ],
            ],
        ],

        'formatter' => [
            'currencyCode' => 'RUR',
            'numberFormatterOptions' => [
                NumberFormatter::MIN_FRACTION_DIGITS => 2,
            ],
        ],

        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'f15sdf6sdfSDfsdfjghjsdQQ6d46',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'thumbnail' => [
            'class' => 'himiklab\thumbnail\EasyThumbnail',
            'cacheAlias' => 'assets/gallery_thumbnails',
        ],
//        'user' => [
//            'identityClass' => 'app\models\User',
//            'enableAutoLogin' => true,
//        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
//        'mailer' => [
//            'class' => 'yii\swiftmailer\Mailer',
//            'useFileTransport' => true, //true - сообщения не отправляются на почту
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'litehost24.ru',
//                'username' => 'mebelnye-shity@mebelnye-shity.ru',
//                'password' => 'wpQSO1JiQV',
//                'port' => '587',
//                'encryption' => 'tls',
//                'streamOptions' => ['ssl' => ['allow_self_signed'=>true, 'verify_peer' => false]],
//            ],
//        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true, //  запрещаем index.php
            'showScriptName' => false, // запрещаем r= routes
            'rules' => [
                '/category' => '/category/index',
                '/registration' => '/user/registration/register',
                '/login' => '/user/security/login',
                '/account' => '/user/settings/account',
                '' => 'site/index',

                [
                    'class' => 'app\components\DynamicPage',

                ], // проверяем, если страница динамическая
            ],
        ],

        'i18n' => [
            'translations' => [
                'sourceLanguage' => 'ru-RU',
                'yii2mod.comments' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@yii2mod/comments/messages',
                ],
            ],
        ],

        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LeMMFEUAAAAANJu0uXNvG1KARMyOx50OJCg-cZC',
            'secret' => '6LeMMFEUAAAAAPxsf4GpueKbNLmNMa4LGdaTNPIn',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
     //configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
         //uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
         //uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
