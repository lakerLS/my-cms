<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CompanyDetails;

/**
 * CompanyDetailsSearch represents the model behind the search form of `app\models\CompanyDetails`.
 */
class CompanyDetailsSearch extends CompanyDetails
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['image', 'indification', 'fio', 'phone_one', 'phone_two', 'phone_three', 'email_one', 'email_two', 'coordinate', 'address'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'indification', $this->indification])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'phone_one', $this->phone_one])
            ->andFilterWhere(['like', 'phone_two', $this->phone_two])
            ->andFilterWhere(['like', 'phone_three', $this->phone_three])
            ->andFilterWhere(['like', 'email_one', $this->email_one])
            ->andFilterWhere(['like', 'email_two', $this->email_two])
            ->andFilterWhere(['like', 'coordinate', $this->coordinate])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
