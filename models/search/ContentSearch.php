<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Content;
use yii\data\Pagination;

/**
 * ContentSearch represents the model behind the search form of `app\models\Content`.
 */
class ContentSearch extends Content
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'author_id', 'hits', 'position'], 'integer'],
            [['name', 'image', 'text', 'description', 'keywords', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($category)
    {
        if (Yii::$app->request->get('type'))
            $query = Content::find()
                ->with('category_id')
                ->where(['category_id' => $category->id])
                ->andWhere(['section' => Yii::$app->request->get('type')])
                ->andWhere(['<>', 'type', 'pencil'])
                ->orderBy(['date' => SORT_DESC]);

        elseif (Yii::$app->request->post('search'))
            $query = Content::find()
                ->where(['category_id' => $category->id])
                ->andWhere(['like', 'name', Yii::$app->request->post('search')])
                ->andWhere(['<>', 'type', 'pencil'])
                ->orderBy(['date' => SORT_DESC]);

        elseif (Yii::$app->request->post('globalSearch'))
            $query = Content::find()
                ->where(['like', 'name', Yii::$app->request->post('globalSearch')])
                ->andWhere(['<>', 'type', 'pencil'])
                ->orderBy(['date' => SORT_DESC]);

        else
            $query = Content::find()
                ->where(['category_id' => $category->id])
                ->andWhere(['<>', 'type', 'pencil'])
                ->orderBy(['position' => SORT_DESC])
                ->with(['user'])
                ->with(['comment']);

        $count = $query->count();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->pagination->defaultPageSize = 21;

        return $dataProvider;
    }

    public function modify($params)
    {
        $query = Content::find()
            ->where(['<>', 'type', 'pencil'])
            ->orderBy(['date' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'author_id' => $this->author_id,
            'hits' => $this->hits,
            'date' => $this->date,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'keywords', $this->keywords]);

        $dataProvider->pagination->defaultPageSize = 21;

        return $dataProvider;
    }
}
