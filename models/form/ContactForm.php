<?php

namespace app\models\form;

use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $message;
    public $verifyCode;

    public $reCaptcha;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['reCaptcha'],
                ReCaptchaValidator::className(),
                'secret' => '6LeMMFEUAAAAAPxsf4GpueKbNLmNMa4LGdaTNPIn',
                'uncheckedMessage' => 'Пожалуйста, подтвердите, что вы не робот.',
                'skipOnEmpty' => 'true',
            ],

            // name, email, subject and body are required
            [['name', 'email', 'subject', 'message'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Как к вам обращаться',
            'email' => 'Ваш email',
            'subject' => 'Тема',
            'message' => 'Сообщение',
            'verifyCode' => 'Проверочный код',
        ];
    }



    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
       // echo "'{$email}'";
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
              //  ->setFrom(['litehost24@litehost24.ru' => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->message)
                ->send();

            return true;
        }
        return false;
    }
}
