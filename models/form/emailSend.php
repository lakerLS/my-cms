<?php

//$company_details = \app\models\CompanyDetails::find()->all();
use app\classes\WidHelper;
use app\models\CompanyDetails;

ob_start();
?>
<?php
$company_details = CompanyDetails::find()->one();
?>

    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:mc="http://www.w3.org/1999/xhtml"><head>


        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1"><meta name="x-apple-disable-message-reformatting"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="format-detection" content="telephone=no"><meta name="format-detection" content="date=no"><meta name="format-detection" content="address=no"><meta name="format-detection" content="email=no"><title>Full Screen</title><style>
            @import url('https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:300,400,600,700,800');
            /*Reset Css*/
            body {margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;}
            table {border-spacing: 0;-mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
            table td {border-collapse: collapse;}
            img {-ms-interpolation-mode: bicubic; display: block;}
            .ExternalClass {width: 100%;}
            .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: inherit;}
            .ReadMsgBody {width: 100%;background-color: #ffffff;}
            a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important; }
            td a {color: inherit; text-decoration: none;}
            a {color: inherit; text-decoration: none;}
            .undoreset a, .undoreset a:hover {text-decoration: none !important;}
            .yshortcuts a {border-bottom: none !important;}
            .ios-footer a {color: #aaaaaa !important;text-decoration: none;}
            table p, table h1, table h2, table h3, table h4{ margin-top:0!important;margin-right:0!important;margin-bottom:0!important;margin-left:0!important;padding-top:0!important;padding-right:0!important;padding-bottom:0!important;padding-left:0!important;display: inline-block!important;font-family: inherit!important;}
            /*Reset Css*/
            @media screen and (max-width: 599px)
            {
                body{width:100% !important;}
                .container           {width:100% !important; min-width:100% !important;}
                .container-wrap      {display:inline-block !important; width:100% !important; height:auto !important; border-radius:0 !important;}
                .image-container     {width: 100% !important; height: auto !important; }
                .disable             {display: none !important;}
                .enable              {display: inline !important;}
                .padding-top-10      {padding-top: 10px !important;}
                .padding-top-20      {padding-top: 20px !important;}
                .padding-top-30      {padding-top: 30px !important;}
                .padding-top-40      {padding-top: 40px !important;}
                .text                {width:90% !important; text-align:center !important;}
                .text-on-bg          {width:90% !important; text-align:center !important;}
                .text-center         {text-align: center !important;}
                .text-left           {text-align: left !important;}
                .background-cover    {background-size: cover !important;}
                .background-cover-percent    {background-size: 100% 79% !important;}
                .border-none         {border:0 !important;}
            }
            @media screen and (max-width: 480px) {}
            @media screen and (max-width: 320px) {}
        </style>


    </head><body marginwidth="0" marginheight="0" style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" offset="0" topmargin="0" leftmargin="0"><table mc:repeatable="layout" mc:hideable="" mc:variant="navigation-margin" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 0px; border-collapse: collapse; margin: 0 auto; max-width: 800px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" data-module="navigation-margin" data-thumb="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/thumbnails/navigation-margin.jpg" class="">
        <!--[if (gte mso 9)|(IE)]></table><table width="800" align="center"><![endif]-->
        <tbody><tr>
            <td align="center" style="width:100%; height:100%; background-color:#ffffff;" bgcollor="#ffffff" data-bgcolor="navigation-margin-bg">
                <table width="600" align="center" class="container" border="0" cellpadding="0" cellspacing="0" style="width:600px;">
                    <!-- mrgn -->
                    <tbody><tr>
                        <td height="20"><img mc:edit="" editable="" style="display:block;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/img/spacer.png" width="1" data-crop="false" alt="#"></td>
                    </tr>
                    <!-- mrgn -->
                    </tbody></table>
            </td>
        </tr>
        </tbody></table><table mc:repeatable="layout" mc:hideable="" mc:variant="title-variant-margin" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 0px; border-collapse: collapse; margin: 0 auto; max-width: 800px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" data-module="title-variant-margin" data-thumb="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/thumbnails/title-variant-margin.jpg" class="">
        <!--[if (gte mso 9)|(IE)]></table><table width="800" align="center"><![endif]-->
        <tbody><tr>
            <td align="center" style="width:100%; height:100%; background-color:#ffffff;" bgcollor="#ffffff" data-bgcolor="title-variant-margin-bg">
                <table width="600" align="center" class="container" border="0" cellpadding="0" cellspacing="0" style="width:600px;">
                    <!-- mrgn -->
                    <tbody><tr>
                        <td height="20"><img mc:edit="" editable="" style="display:block;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/img/spacer.png" width="1" data-crop="false" alt="#"></td>
                    </tr>
                    <!-- mrgn -->
                    </tbody></table>
            </td>
        </tr>
        </tbody></table><table mc:repeatable="layout" mc:hideable="" mc:variant="title-variant-5" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 0px; border-collapse: collapse; margin: 0 auto; max-width: 800px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" data-module="title-variant-5" data-thumb="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/thumbnails/var-5.jpg" class="">
        <!--[if (gte mso 9)|(IE)]></table><table width="800" align="center"><![endif]-->
        <tbody><tr>
            <td align="center" style="width:100%; height:100%; background-color:#ffffff;" bgcollor="#ffffff" data-bgcolor="var-5-bg">
                <table width="600" align="center" class="container" border="0" cellpadding="0" cellspacing="0" style="width:600px;">
                    <tbody><tr>
                        <td align="center">
                            <table width="410" align="center" class="text" border="0" cellpadding="0" cellspacing="0" style="width:410px;">
                                <!-- mrgn -->
                                <tbody><tr>
                                    <td height="16"></td>
                                </tr>
                                <!-- mrgn -->
                                <!-- ttl -->
                                <tr>
                                    <td style="text-align:center; font-family: 'Montserrat', Arial, Helvetica, sans-serif; font-size:20px; line-height: 25px; text-decoration: none; color: #000000; font-weight:600; text-transform: uppercase; letter-spacing: 0.05em" data-size="var-5-title-size" data-color="var-5-title-color" data-link-color="var-5-link-color" data-link-style="color: blue;" mc:edit="">
                                        <multiline><?= substr(Yii::$app->request->serverName, '0', '-3') ?></multiline>
                                    </td>
                                </tr>
                                <!-- ttl -->
                                <!-- mrgn -->
                                <tr>
                                    <td height="10"></td>
                                </tr>
                                <!-- mrgn -->
                                <!-- line -->
                                <tr>
                                    <td align="center">
                                        <table width="120" height="2" class="var-5-line-color" align="center" border="0" cellpadding="0" cellspacing="0" style="width:120px;  background-color: #3498db" data-bgcolor="var-5-line-color">

                                            <tbody><tr>
                                                <td align="center" height="2" width="120" style="line-height: 2px; width:120px;"><img src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/img/spacer.png" alt="spacer" width="120" height="2"></td>
                                            </tr>

                                            </tbody></table>
                                    </td>
                                </tr>
                                <!-- line -->
                                <!-- mrgn -->
                                <tr>
                                    <td height="11"></td>
                                </tr>
                                <!-- mrgn -->
                                <!-- text -->
                                <tr>
                                    <td style="text-align:center; font-family: 'Open Sans', sans-serif; font-size:12px; line-height: 19px; text-decoration: none; color: #444444; font-weight:400;" data-size="var-5-text-size" data-color="var-5-text-color" data-link-color="var-5-link-color" data-link-style="color: blue;" mc:edit="">
                                        <multiline>
                                            Вы получили данное письмо, потому что Ваши данные были указаны при заказе с нашего сайта.
                                        </multiline>
                                    </td>
                                </tr>
                                <!-- text -->
                                <!-- mrgn -->
                                <tr>
                                    <td height="17"></td>
                                </tr>
                                <!-- mrgn -->
                                </tbody></table>
                        </td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        </tbody></table><table mc:repeatable="layout" mc:hideable="" mc:variant="title-variant-7" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 0px; border-collapse: collapse; margin: 0 auto; max-width: 800px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" data-module="title-variant-7" data-thumb="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/thumbnails/var-7.jpg" class="">
        <!--[if (gte mso 9)|(IE)]></table><table width="800" align="center"><![endif]-->
        <tbody><tr>
            <td align="center" style="width:100%; height:100%; background-color:#ffffff;" bgcollor="#ffffff" data-bgcolor="var-7-bg">
                <table width="600" align="center" class="text" border="0" cellpadding="0" cellspacing="0" style="width:600px;">
                    <!-- mrgn -->
                    <tbody><tr>
                        <td height="20"></td>
                    </tr>
                    <!-- mrgn -->
                    <tr>
                        <td align="left">
                            <table width="300" align="left" class="container" cellpadding="0" cellspacing="0" style="width:300px;  mso-table-lspace:0pt; mso-table-rspace:0pt; border-left: 4px solid #3498db;" data-border-left-color="var-7-line-color">
                                <tbody><tr>
                                    <td align="center" style="padding-left: 20px;">
                                        <table width="300" align="center" class="text" border="0" cellpadding="0" cellspacing="0" style="width:300px;  padding-left: 20px;">
                                            <!-- mrgn -->
                                            <tbody><tr>
                                                <td height="4"></td>
                                            </tr>
                                            <!-- mrgn -->
                                            <!-- ttl -->
                                            <tr>
                                                <td style="text-align:left; font-family: 'Montserrat', Arial, Helvetica, sans-serif; font-size:20px; line-height: 25px; text-decoration: none; color: #000000; font-weight:600; text-transform: uppercase; letter-spacing: 0.05em" data-size="var-7-title-size" data-color="var-7-title-color" data-link-color="var-7-link-color" data-link-style="color: blue;" mc:edit="">
                                                    <multiline>Поставщик:</multiline>
                                                </td>
                                            </tr>
                                            <!-- ttl -->
                                            <!-- mrgn -->
                                            <tr>
                                                <td height="10"></td>
                                            </tr>
                                            <!-- mrgn -->
                                            <!-- sub-title -->
                                            <tr>
                                                <td style="text-align:left; font-family: 'Montserrat', Arial, Helvetica, sans-serif; font-size:13px; line-height: 18px; text-decoration: none; color: #757575; font-weight:600; text-transform: uppercase; letter-spacing: 0.05em" data-size="var-7-sub-title-size" data-color="var-7-sub-title-color" data-link-color="var-7-link-color" data-link-style="color: blue;" mc:edit="">

                                                    <multiline><b>Ф.И.О.:</b> <?= $company_details->fio ?></multiline> <br />

                                                    <? if (!empty($company_details->phone_one)) { ?>
                                                        <multiline><b>Телефон:</b> <?= $company_details->phone_one ?></multiline> <br />
                                                    <? } ?>
                                                    <? if (!empty($company_details->phone_two)) { ?>
                                                        <multiline><b>Телефон:</b> <?= $company_details->phone_two ?></multiline> <br />
                                                    <? } ?>
                                                    <? if (!empty($company_details->phone_three)) { ?>
                                                        <multiline><b>Телефон:</b> <?= $company_details->phone_three ?></multiline> <br />
                                                    <? } ?>

                                                    <? if (!empty($company_details->email_one)) { ?>
                                                        <multiline><b>email:</b> <?= $company_details->email_one ?></multiline> <br />
                                                    <? } ?>
                                                    <? if (!empty($company_details->email_two)) { ?>
                                                        <multiline><b>email:</b> <?= $company_details->email_two ?></multiline> <br />
                                                    <? } ?>
                                                </td>
                                            </tr>
                                            <!-- sub-title -->
                                            <!-- mrgn -->
                                            <tr>
                                                <td height="7"></td>
                                            </tr>
                                            <!-- mrgn -->
                                            </tbody></table>
                                    </td>
                                </tr>
                                </tbody></table>
                        </td>
                    </tr>
                    <!-- mrgn -->
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <!-- mrgn -->
                    </tbody></table>
            </td>
        </tr>
        </tbody></table><table mc:repeatable="layout" mc:hideable="" mc:variant="title-variant-8" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 0px; border-collapse: collapse; margin: 0 auto; max-width: 800px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" data-module="title-variant-8" data-thumb="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/thumbnails/var-8.jpg" class="">

        <tbody><tr>
            <td align="center" style="width:100%; height:100%; background-color:#ffffff;" bgcollor="#ffffff" data-bgcolor="var-7-bg">
                <table width="600" align="center" class="text" border="0" cellpadding="0" cellspacing="0" style="width:600px;">
                    <!-- mrgn -->
                    <tbody><tr>
                        <td height="20"></td>
                    </tr>
                    <!-- mrgn -->
                    <tr>
                        <td align="left">
                            <table width="300" align="left" class="container" cellpadding="0" cellspacing="0" style="width:300px;  mso-table-lspace:0pt; mso-table-rspace:0pt; border-left: 4px solid #3498db;" data-border-left-color="var-7-line-color">
                                <tbody><tr>
                                    <td align="center" style="padding-left: 20px;">
                                        <table width="300" align="center" class="text" border="0" cellpadding="0" cellspacing="0" style="width:300px;  padding-left: 20px;">
                                            <!-- mrgn -->
                                            <tbody><tr>
                                                <td height="4"></td>
                                            </tr>
                                            <!-- mrgn -->
                                            <!-- ttl -->
                                            <tr>
                                                <td style="text-align:left; font-family: 'Montserrat', Arial, Helvetica, sans-serif; font-size:20px; line-height: 25px; text-decoration: none; color: #000000; font-weight:600; text-transform: uppercase; letter-spacing: 0.05em" data-size="var-7-title-size" data-color="var-7-title-color" data-link-color="var-7-link-color" data-link-style="color: blue;" mc:edit="">
                                                    <multiline>Заказчик: </multiline>
                                                </td>
                                            </tr>
                                            <!-- ttl -->
                                            <!-- mrgn -->
                                            <tr>
                                                <td height="10"></td>
                                            </tr>
                                            <!-- mrgn -->
                                            <!-- sub-title -->
                                            <tr>
                                                <td style="text-align:left; font-family: 'Montserrat', Arial, Helvetica, sans-serif; font-size:13px; line-height: 18px; text-decoration: none; color: #757575; font-weight:600; text-transform: uppercase; letter-spacing: 0.05em" data-size="var-7-sub-title-size" data-color="var-7-sub-title-color" data-link-color="var-7-link-color" data-link-style="color: blue;" mc:edit="">
                                                    <multiline><b>Ф.И.О.:</b> <?= $model->name ?></multiline> <br />
                                                    <multiline><b>Телефон:</b> <?= $model->phone ?></multiline> <br />
                                                    <multiline><b>Email:</b> <?= $model->email ?></multiline> <br />
                                                </td>
                                            </tr>
                                            <!-- sub-title -->
                                            <!-- mrgn -->
                                            <tr>
                                                <td height="7"></td>
                                            </tr>
                                            <!-- mrgn -->
                                            </tbody></table>
                                    </td>
                                </tr>
                                </tbody></table>
                        </td>
                    </tr>
                    <!-- mrgn -->
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <!-- mrgn -->
                    </tbody></table>
            </td>
        </tr>
        </tbody></table><table mc:repeatable="layout" mc:hideable="" mc:variant="title-variant-8" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 0px; border-collapse: collapse; margin: 0 auto; max-width: 800px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" data-module="title-variant-8" data-thumb="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/thumbnails/var-8.jpg" class="">


    <!--[if (gte mso 9)|(IE)]></table><table width="800" align="center"><![endif]-->

        <!--[if (gte mso 9)|(IE)]></table><table width="800" align="center"><![endif]-->
        <tbody><tr>
            <td align="center" style="width:100%; height:100%; background-color:#ffffff;" bgcollor="#ffffff" data-bgcolor="title-variant-margin-2-bg">
                <table width="600" align="center" class="container" border="0" cellpadding="0" cellspacing="0" style="width:600px;">
                    <!-- mrgn -->
                    <tbody><tr>
                        <td height="20">
                            <a href="#"><img mc:edit="" editable="" style="display:block;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/img/spacer.png" width="1" height="1" data-crop="false" alt="#"></a>
                        </td>
                    </tr>
                    <!-- mrgn -->
                    </tbody></table>
            </td>
        </tr>
        </tbody></table><table mc:repeatable="layout" mc:hideable="" mc:variant="team-5" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 0px; border-collapse: collapse; margin: 0 auto; max-width: 800px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" data-module="team-5" data-thumb="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/thumbnails/team-5.jpg" class="">
        <!--[if (gte mso 9)|(IE)]></table><table width="800" align="center"><![endif]-->
        <tbody><tr>
            <td align="center" style="width:100%; height:100%; background-color:#f8f8f8;" bgcollor="#f8f8f8" data-bgcolor="team-5-bg">
                <table width="600" align="center" class="container" border="0" cellpadding="0" cellspacing="0" style="width:600px;">
                    <!-- mrgn -->
                    <tbody><tr>
                        <td height="40"></td>
                    </tr>
                    <!-- mrgn -->
                    <tr>
                        <td align="center">
                            <table width="600" align="center" class="container" border="0" cellpadding="0" cellspacing="0" style="width:600px;">
                                <tbody><tr>
                                    <!-- sec -->
                                    <th width="395" align="right" class="container-wrap border-none" valign="top" style="vertical-align: top; border-left: 4px solid #3498db;" data-border-left-color="team-5-line-color">
                                        <table width="395" align="right" class="container" border="0" cellpadding="0" cellspacing="0" style="width:395px; padding-left: 20px;">
                                            <tbody><tr>
                                                <td align="left">
                                                    <table width="365" align="left" class="container" border="0" cellpadding="0" cellspacing="0" style="">
                                                        <tbody><tr>
                                                            <td align="center">
                                                                <table width="365" align="center" class="text" border="0" cellpadding="0" cellspacing="0" style="width:365px;">
                                                                    <!-- mrgn -->
                                                                    <tbody><tr>
                                                                        <td height="18"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    <!-- ttl -->
                                                                    <tr>
                                                                        <td style="text-align:left; font-family: 'Montserrat', Arial, Helvetica, sans-serif; font-size:14px; line-height: 14px; text-decoration: none; color: #000000; font-weight:600; text-transform: uppercase; letter-spacing: 0.05em" data-size="team-5-title-size" data-color="team-5-title-color" data-link-color="team-5-link-color" data-link-style="color: blue;" mc:edit="">
                                                                            <multiline><?= $model->price->name ?></multiline>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- ttl -->
                                                                    <!-- mrgn -->
                                                                    <tr>
                                                                        <td height="5"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    <!-- job -->
                                                                    <tr>
                                                                        <td style="text-align:left; font-family: 'Open Sans' , sans-serif; font-size:14px; line-height: 20px; text-decoration: none; color: #bdbdbd; font-weight:400;" data-size="team-5-job-size" data-color="team-5-job-color" data-link-color="team-5-link-color" data-link-style="color: blue;" mc:edit="">
                                                                            <multiline><?= $model->price->section ? $model->price->section : '' ?></multiline>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- job -->
                                                                    <!-- mrgn -->
                                                                    <tr>
                                                                        <td height="7"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    <!-- text -->
                                                                    <tr>
                                                                        <td style="text-align:left; font-family: 'Open Sans' , sans-serif; font-size:13px; line-height: 20px; text-decoration: none; color: #444444; font-weight:400;" data-size="team-5-text-size" data-color="team-5-text-color" data-link-color="team-5-link-color" data-link-style="color: blue;" mc:edit="">
                                                                            <multiline>
                                                                                <?= WidHelper::text($model->price, 40) ?>...
                                                                            </multiline>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- text -->
                                                                    <!-- mrgn -->

                                                                    <!-- icons -->
                                                                    <!-- mrgn -->
                                                                    <tr>
                                                                        <td height="20"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    </tbody></table>
                                                            </td>
                                                        </tr>
                                                        </tbody></table>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                    </th>
                                    <!-- sec -->
                                    <!-- sec -->
                                    <th width="180" align="right" class="container-wrap padding-top-20" valign="top" style="vertical-align: top;">
                                        <table width="180" align="right" class="container" border="0" cellpadding="0" cellspacing="0" style="width:180px;">
                                            <tbody><tr>
                                                <td align="center">
                                                    <table width="180" align="center" class="text" border="0" cellpadding="0" cellspacing="0" style="">
                                                        <!-- image -->
                                                        <tbody><tr>
                                                            <td align="center">
                                                                <img mc:edit="" editable="" label="team-5-image-1" class="image-container" src="<?= Yii::$app->request->hostInfo ?><?= WidHelper::image($model->price, 210, 180) ?>" width="210" height="180" alt="#" style="display:block; vertical-align:top;">
                                                            </td>
                                                        </tr>
                                                        <!-- image -->
                                                        </tbody></table>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                    </th>
                                    <!-- sec -->
                                </tr>
                                </tbody></table>
                        </td>
                    </tr>
                    <!-- mrgn -->
                    <tr>
                        <td height="40"></td>
                    </tr>
                    <!-- mrgn -->
                    </tbody></table>
            </td>
        </tr>
        </tbody></table><table mc:repeatable="layout" mc:hideable="" mc:variant="contacts-1" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 0px; border-collapse: collapse; margin: 0 auto; max-width: 800px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" data-module="contacts-1" data-thumb="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/thumbnails/cont-1.jpg" class="">
        <!--[if (gte mso 9)|(IE)]></table><table width="800" align="center"><![endif]-->
        <tbody><tr>
            <td align="center" style="width:100%; height:100%; background-color:#ffffff;" bgcollor="#ffffff" data-bgcolor="cont-1-bg">
                <table width="600" align="center" class="container" border="0" cellpadding="0" cellspacing="0" style="width:600px;">
                    <!-- mrgn -->
                    <tbody><tr>
                        <td height="38"></td>
                    </tr>
                    <!-- mrgn -->
                    <tr>
                        <td align="center">
                            <table width="600" align="center" class="container" border="0" cellpadding="0" cellspacing="0" style="width:600px;">
                                <tbody><tr>
                                    <!-- sec -->
                                    <th width="180" align="left" class="container-wrap" valign="top" style="vertical-align: top;">
                                        <table width="180" align="left" class="container" border="0" cellpadding="0" cellspacing="0" style="width:180px;">
                                            <tbody><tr>
                                                <td align="center">
                                                    <table width="180" align="center" class="text" border="0" cellpadding="0" cellspacing="0" style="">
                                                        <tbody><tr>
                                                            <!-- sec -->
                                                            <th width="40" align="left" class="container-wrap" valign="top" style="vertical-align: top;">
                                                                <table width="40" align="left" class="container" border="0" cellpadding="0" cellspacing="0" style="width:40px;">
                                                                    <!-- mrgn -->
                                                                    <tbody><tr>
                                                                        <td height="2"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    <!-- icn -->
                                                                    <tr>
                                                                        <td align="left">
                                                                            <img mc:edit="" editable="" label="contacts-1-icon-1" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/img/contacts-1-icon-1.png" width="28" height="40" alt="#" style="display:block; vertical-align:top;">
                                                                        </td>
                                                                    </tr>
                                                                    <!-- icn -->
                                                                    <!-- mrgn -->
                                                                    <tr>
                                                                        <td height="20"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    </tbody></table>
                                                            </th>
                                                            <!-- sec -->
                                                            <!-- sec -->
                                                            <th width="130" align="right" class="container-wrap" valign="top" style="vertical-align: top;">
                                                                <table width="130" align="right" class="container" border="0" cellpadding="0" cellspacing="0" style="width:130px;">
                                                                    <!-- ttl -->
                                                                    <tbody><tr>
                                                                        <td style="vertical-align:top; text-align:left; font-family: 'Montserrat', Arial, Helvetica, sans-serif; font-size:16px; line-height: 16px; text-decoration: none; color: #000000; font-weight:400; text-transform: uppercase; letter-spacing: 0.05em" data-size="cont-1-title-size" data-color="cont-1-title-color" data-link-color="cont-1-link-color" data-link-style="color: blue;" mc:edit="">
                                                                            <multiline>Наш тел.</multiline>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- ttl -->
                                                                    <!-- mrgn -->
                                                                    <tr>
                                                                        <td height="8"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    <!-- sub-title -->
                                                                    <tr>
                                                                        <td style="vertical-align:top; text-align:left; font-family: 'Open Sans', sans-serif; font-size:13px; line-height: 13px; text-decoration: none; color: #444444; font-weight: 500; text-transform: uppercase; letter-spacing: 0.05em" data-size="cont-1-sub-title-size" data-color="cont-1-sub-title-color" data-link-color="cont-1-link-color" data-link-style="color: blue;" mc:edit="">
                                                                            <multiline><?= $company_details->phone_one ?></multiline>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- sub-title -->
                                                                    <!-- mrgn -->
                                                                    <tr>
                                                                        <td height="45"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    </tbody></table>
                                                            </th>
                                                            <!-- sec -->
                                                        </tr>
                                                        </tbody></table>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                    </th>
                                    <!-- sec -->
                                    <!-- sec -->
                                    <th width="180" align="center" class="container-wrap padding-top-10" valign="top" style="vertical-align: top;">
                                        <table width="180" align="center" class="container" border="0" cellpadding="0" cellspacing="0" style="width:180px;">
                                            <tbody><tr>
                                                <td align="center">
                                                    <table width="180" align="center" class="text" border="0" cellpadding="0" cellspacing="0" style="">
                                                        <tbody><tr>
                                                            <!-- sec -->
                                                            <th width="40" align="left" class="container-wrap" valign="top" style="vertical-align: top;">
                                                                <table width="40" align="left" class="container" border="0" cellpadding="0" cellspacing="0" style="width:40px;">
                                                                    <!-- mrgn -->
                                                                    <tbody><tr>
                                                                        <td height="2"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    <!-- icn -->
                                                                    <tr>
                                                                        <td align="left">
                                                                            <img mc:edit="" editable="" label="contacts-1-icon-2" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/img/contacts-1-icon-2.png" width="40" height="40" alt="#" style="display:block; vertical-align:top;">
                                                                        </td>
                                                                    </tr>
                                                                    <!-- icn -->
                                                                    <!-- mrgn -->
                                                                    <tr>
                                                                        <td height="20"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    </tbody></table>
                                                            </th>
                                                            <!-- sec -->
                                                            <!-- sec -->
                                                            <th width="130" align="right" class="container-wrap" valign="top" style="vertical-align: top;">
                                                                <table width="130" align="right" class="container" border="0" cellpadding="0" cellspacing="0" style="width:130px;">
                                                                    <!-- ttl -->
                                                                    <tbody><tr>
                                                                        <td style="vertical-align:top; text-align:left; font-family: 'Montserrat', Arial, Helvetica, sans-serif; font-size:16px; line-height: 16px; text-decoration: none; color: #000000; font-weight:400; text-transform: uppercase; letter-spacing: 0.05em" data-size="cont-1-title-size" data-color="cont-1-title-color" data-link-color="cont-1-link-color" data-link-style="color: blue;" mc:edit="">
                                                                            <multiline>Наш сайт</multiline>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- ttl -->
                                                                    <!-- mrgn -->
                                                                    <tr>
                                                                        <td height="8"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    <!-- sub-title -->
                                                                    <tr>
                                                                        <td style="vertical-align:top; text-align:left; font-family: 'Open Sans', sans-serif; font-size:13px; line-height: 13px; text-decoration: none; color: #444444; font-weight: 500; text-transform: uppercase; letter-spacing: 0.05em" data-size="cont-1-sub-title-size" data-color="cont-1-sub-title-color" data-link-color="cont-1-link-color" data-link-style="color: blue;" mc:edit="">
                                                                            <multiline><?= substr(strstr(Yii::$app->request->hostInfo, '//'), 2) ?></multiline>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- sub-title -->
                                                                    <!-- mrgn -->
                                                                    <tr>
                                                                        <td height="45"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    </tbody></table>
                                                            </th>
                                                            <!-- sec -->
                                                        </tr>
                                                        </tbody></table>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                    </th>
                                    <!-- sec -->
                                    <!-- sec -->
                                    <th width="180" align="right" class="container-wrap padding-top-10" valign="top" style="vertical-align: top;">
                                        <table width="180" align="right" class="container" border="0" cellpadding="0" cellspacing="0" style="width:180px;">
                                            <tbody><tr>
                                                <td align="center">
                                                    <table width="180" align="center" class="text" border="0" cellpadding="0" cellspacing="0" style="">
                                                        <tbody><tr>
                                                            <!-- sec -->
                                                            <th width="40" align="left" class="container-wrap" valign="top" style="vertical-align: top;">
                                                                <table width="40" align="left" class="container" border="0" cellpadding="0" cellspacing="0" style="width:40px;">
                                                                    <!-- mrgn -->
                                                                    <tbody><tr>
                                                                        <td height="2"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    <!-- icn -->
                                                                    <tr>
                                                                        <td align="left">
                                                                            <img mc:edit="" editable="" label="contacts-1-icon-3" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/img/contacts-1-icon-3.png" width="40" height="40" alt="#" style="display:block; vertical-align:top;">
                                                                        </td>
                                                                    </tr>
                                                                    <!-- icn -->
                                                                    <!-- mrgn -->
                                                                    <tr>
                                                                        <td height="20"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    </tbody></table>
                                                            </th>
                                                            <!-- sec -->
                                                            <!-- sec -->
                                                            <th width="130" align="right" class="container-wrap" valign="top" style="vertical-align: top;">
                                                                <table width="130" align="right" class="container" border="0" cellpadding="0" cellspacing="0" style="width:130px;">
                                                                    <!-- ttl -->
                                                                    <tbody><tr>
                                                                        <td style="vertical-align:top; text-align:left; font-family: 'Montserrat', Arial, Helvetica, sans-serif; font-size:16px; line-height: 16px; text-decoration: none; color: #000000; font-weight:400; text-transform: uppercase; letter-spacing: 0.05em" data-size="cont-1-title-size" data-color="cont-1-title-color" data-link-color="cont-1-link-color" data-link-style="color: blue;" mc:edit="">
                                                                            <multiline> Наш email</multiline>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- ttl -->
                                                                    <!-- mrgn -->
                                                                    <tr>
                                                                        <td height="8"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    <!-- sub-title -->
                                                                    <tr>
                                                                        <td style="vertical-align:top; text-align:left; font-family: 'Open Sans', sans-serif; font-size:13px; line-height: 13px; text-decoration: none; color: #444444; font-weight: 500; text-transform: uppercase; letter-spacing: 0.05em" data-size="cont-1-sub-title-size" data-color="cont-1-sub-title-color" data-link-color="cont-1-link-color" data-link-style="color: blue;" mc:edit="">
                                                                            <multiline> <?= $company_details->email_one ?></multiline>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- sub-title -->
                                                                    <!-- mrgn -->
                                                                    <tr>
                                                                        <td height="45"></td>
                                                                    </tr>
                                                                    <!-- mrgn -->
                                                                    </tbody></table>
                                                            </th>
                                                            <!-- sec -->
                                                        </tr>
                                                        </tbody></table>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                    </th>
                                    <!-- sec -->
                                </tr>
                                </tbody></table>
                        </td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        </tbody></table><div class="parentOfBg"></div><table mc:repeatable="layout" mc:hideable="" mc:variant="contacts-2" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 0px; border-collapse: collapse; margin: 0px auto; max-width: 800px; position: relative; opacity: 1; top: 0px; left: 0px;" data-module="contacts-2" data-thumb="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2017/12/15/8CvVaGoq2p0O4kYuRzhj3y5U/Preview/thumbnails/cont-2.jpg" class="">
        <!--[if (gte mso 9)|(IE)]></table><table width="800" align="center"><![endif]-->

        <!--[if (gte mso 9)|(IE)]></table><table width="800" align="center"><![endif]-->

<?php

$email_html =  ob_get_clean();

Yii::$app->mailer->compose()
    ->setFrom('ecostroy50@ecostroy50.ru')
    ->setTo($model->email)
    ->setSubject('Заказ с сайта ' . $_SERVER['HTTP_HOST'] . ' №'.$model->id) // тема письма
    // ->setTextBody('Текстовая версия письма (без HTML)')
    ->setHtmlBody($email_html)
    ->send();

Yii::$app->mailer->compose()
    ->setFrom($model->email)
    ->setTo('ecostroy50@ecostroy50.ru')
    ->setSubject('Заказ с сайта ' . $_SERVER['HTTP_HOST'] . ' №'.$model->id) // тема письма
    // ->setTextBody('Текстовая версия письма (без HTML)')
    ->setHtmlBody($email_html)
    ->send();