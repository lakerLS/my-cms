<?php

namespace app\models;

use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 */
class Faq extends \yii\db\ActiveRecord
{
    public $reCaptcha;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reCaptcha'],
                ReCaptchaValidator::className(),
                'secret' => '6LeMMFEUAAAAAPxsf4GpueKbNLmNMa4LGdaTNPIn',
                'uncheckedMessage' => 'Пожалуйста, подтвердите, что вы не робот.',
                'skipOnEmpty' => 'true',
            ],

            [['name'], 'string', 'max' => 255],
            [['text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Вопрос',
            'text' => 'Ответ',
        ];
    }
}
