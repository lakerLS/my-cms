<?php

namespace app\models;

use app\components\storageImage\eventOfImage;
use app\modules\position\PositionBehavior;
use dektrium\user\models\User;
use yii\db\ActiveRecord;
use yii2mod\comments\models\CommentModel;

/**
 * This is the model class for table "content".
 *
 * @property int $id
 * @property int $category_id
 * @property int $author_id
 * @property string $name
 * @property string $image
 * @property string $text
 * @property string $description
 * @property string $keywords
 * @property int $hits
 * @property string $data
 * @property int $position
 */
class Content extends ActiveRecord
{

    public function behaviors()
    {
        return [
            'positionBehavior' => [
                'class' => PositionBehavior::class,
                'groupAttributes' => [
                    'category_id' // multiple lists varying by 'categoryId'
                ],
            ],
        ];
    }

    public function beforeSave($insert)// перед записью
    {
        if (parent::beforeSave($insert))
        {
            eventOfImage::beforeSave($insert, $this); // сохранение и редактирование изображений.

            return true;
        }

        else
            return false;

    }

    public function afterDelete()// после удалением quote
    {
        parent::afterDelete();

        eventOfImage::afterDelete($this); // удаление изображений.
    }


    public static function tableName()
    {
        return 'content';
    }

    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['category_id', 'author_id', 'hits', 'position', 'price', 'price_old'], 'integer'],
            [['image', 'text'], 'string'],
            [['date'], 'safe'],
            [['url'], 'string', 'max' => 60],
            [['type', 'name', 'title', 'description', 'keywords', 'section', 'source'], 'string', 'max' => 200],
            [['category_id', 'type', 'name', 'url'], 'required'],

            [['url'], 'uniqueInCategory'],
        ];
    }

    // Проверка на уникальность в текущей категории. В других категориях совпадения не критичны.
    public function uniqueInCategory ($attribute) {

        $leaves = Content::find()->where(['category_id' => $this->category_id])->all();

        foreach ($leaves as $value)
        {
            //Если найдены совпадения в текущей категории и адрес записи изменился.
            if ($this->url == $value->url && $this->oldAttributes['url'] != $this->url)
            {
                $this->addError($attribute, 'Запись с указанным адресом в данной категории уже существует.
                            Измените адрес.');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'author_id' => 'Автор',
            'title' => 'Наименование для поисковика',
            'type' => 'Дизайн страницы',
            'url' => 'Адрес',
            'name' => 'Заголовок',
            'image' => 'Изображение',
            'text' => 'Текст',
            'price' => 'Цена',
            'price_old' => 'Старая цена',
            'description' => 'Краткое описание',
            'keywords' => 'Ключевые слова',
            'section' => 'Раздел',
            'source' => 'Источник',
            'hits' => 'Просмотры',
            'date' => 'Дата',
            'position' => 'Позиция',
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'author_id']);
    }

    public function getOneType()
    {
        return $this->hasOne(Type::class, ['type' => 'type']);
    }

    public function getComment()
    {
        return $this->hasMany(CommentModel::class, ['entityId' => 'id'])->select(['entityId', 'status']); // использую для счетчика комментариев
    }
}
