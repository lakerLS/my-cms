<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "type".
 *
 * @property int $id
 * @property string $type
 * @property string $name
 * @property string $description
 * @property string $select
 * @property string $route
 * @property string $tpl_v1
 * @property string $tpl_v2
 */
class Type extends \yii\db\ActiveRecord
{

    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        $this->select = implode(',', $this->select);

        return true;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['many', 'one'], 'integer'],
            [['select'], 'safe'],
            [['name', 'description', 'type', 'tpl_v1', 'tpl_v2'], 'string'],
            [['name', 'select', 'type'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип содержимого',
            'name' => 'Наименование',
            'description' => 'Описание',
            'select' => 'Активные поля в содержимом',
            'many' => 'Для списка',
            'one' => 'Для записи',
            'tpl_v1' => 'Tpl V1',
            'tpl_v2' => 'Tpl V2',
        ];
    }
}
