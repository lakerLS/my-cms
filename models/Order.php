<?php

namespace app\models;

use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $adress
 * @property string $price_name
 * @property string $price_id
 */
class Order extends \yii\db\ActiveRecord
{
    public $reCaptcha;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reCaptcha'],
                ReCaptchaValidator::className(),
                'secret' => '6LeMMFEUAAAAAPxsf4GpueKbNLmNMa4LGdaTNPIn',
                'uncheckedMessage' => 'Пожалуйста, подтвердите, что вы не робот.',
                'skipOnEmpty' => 'true',
            ],

            [['name', 'email', 'phone', 'adress', 'price_name', 'price_id', 'closed', 'date'], 'string', 'max' => 255],
            [['phone', 'email'], 'required'],
            //[['phone'], 'match', 'pattern' => '/^\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}$/', 'message' => 'Пожалуйста, укажите номер телефона в следующем формате: +7 (999) 333-22-11'],
            [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'email' => 'Email',
            'phone' => 'Телефон',
            'adress' => 'Адрес',
            'price_name' => 'Заказанная услуга',
            'price_id' => 'ID услуги',
            'date' => 'Дата',
        ];
    }

    public function getPrice()
    {
        return $this->hasOne(Content::className(), ['id' => 'price_id']);
    }
}
