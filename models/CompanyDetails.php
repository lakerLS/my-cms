<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_details".
 *
 * @property int $id
 * @property string $image
 * @property string $indification
 * @property string $fio
 * @property string $phone_one
 * @property string $phone_two
 * @property string $phone_three
 * @property string $email
 * @property string $coordinate
 * @property string $address
 */
class CompanyDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'indification', 'fio', 'phone_one','phone_two','phone_three', 'email_one', 'email_two', 'coordinate'], 'string', 'max' => 200],
            [['address'], 'string', 'max' => 400],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение',
            'indification' => 'Индификатор',
            'fio' => 'Ф.И.О.',
            'phone_one' => 'Телефон',
            'phone_two' => 'Второй телефон',
            'phone_three' => 'Третий телефон',
            'email_one' => 'Email',
            'email_two' => 'Второй Email',
            'coordinate' => 'Координаты',
            'address' => 'Адрес',
        ];
    }
}
