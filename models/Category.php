<?php

namespace app\models;

use app\components\myRedirect;
use app\components\storageImage\eventOfImage;
use kartik\tree\models\Tree;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int $root
 * @property int $lft
 * @property int $rgt
 * @property int $lvl
 * @property string $type
 * @property string $name
 * @property string $icon
 * @property int $icon_type
 * @property int $active
 * @property int $selected
 * @property int $disabled
 * @property int $readonly
 * @property int $visible
 * @property int $collapsed
 * @property int $movable_u
 * @property int $movable_d
 * @property int $movable_l
 * @property int $movable_r
 * @property int $removable
 * @property int $removable_all
 */
class Category extends Tree
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    public function beforeSave($insert)// перед записью
    {
        if (parent::beforeSave($insert))
        {
            eventOfImage::beforeSave($insert, $this); // сохранение и редактирование изображений.

            return true;
        }

        else
            return false;
    }

    public function afterDelete()
    {
        parent::afterDelete();

        eventOfImage::afterDelete($this); // удаление изображений.
    }

    /**
     * Делаем запись в таблицу "redirect", где хранятся новые и старые адреса, для переадресации.
     *
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        myRedirect::urlMain($this);
        myRedirect::urlChildren();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['type', 'url', 'name'], 'required'];
        $rules[] = [['name', 'url'], 'string', 'max' => 60];
        $rules[] = [['type', 'image', 'title', 'description', 'tags'], 'string', 'max' => 255];

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $attr = parent::attributeLabels();
        $attr['type'] = 'Тип';
        $attr['url'] = 'Адрес';
        $attr['description'] = 'Краткое описание';
        $attr['tags'] = 'Теги';
        $attr['image'] = 'Изображение (Пропорции 356 х 246)';
        return $attr;
    }


    public function getContent()
    {
        return $this->hasOne(Content::class, ['category_id' => 'id']);
    }
}
